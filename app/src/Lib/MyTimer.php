<?php

class MyTimer
{

    private $arr = array();
    private static $instance;

    private function __construct()
    {

    }

    public static function instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function start($id = 0)
    {
        self::instance()->arr[$id] = array();
        self::instance()->set('start', $id);
    }

    public static function set($str, $id = 0)
    {
        if (isset(self::instance()->arr[$id])) {
            self::instance()->arr[$id][] = array(
              'time' => microtime(true),
              'str' => $str,
              'memory' => memory_get_usage(true),
            );
        }
    }

    public static function fin($id = 0)
    {
        if (isset(self::instance()->arr[$id])) {
            self::instance()->set('stop', $id);
            self::instance()->makeDiffs($id);
            return self::instance()->arr[$id];
        }
        return array();
    }

    private function makeDiffs($id = 0)
    {
        if (isset($this->arr[$id])) {
            $start = $this->arr[$id][0]['time'];
            foreach ($this->arr[$id] as $key => $Val) {
                if ($key) {
                    $this->arr[$id][$key]['fromstart'] = ($Val['time'] - $start);
                    $this->arr[$id][$key]['fromprev'] = ($Val['time'] - $this->arr[$id][($key - 1)]['time']);
                }
            }
        }
    }

    public static function getFin($id = 0)
    {
        if (isset(self::instance()->arr[$id])) {
            $arr = static::fin($id);
            $arr = array_pop($arr);
            return $arr['fromstart'];
        }
    }

    function __destruct()
    {

    }

}
