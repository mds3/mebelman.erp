<?php

namespace Mebius\Service\SmsGate;

use \Config;
use \Illuminate\Support\ServiceProvider;

class SmsGateServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            '\\Mebius\\Service\\SmsGate\\SmsGateInterface',
            function() {
            return new SmscGate(Config::get('services.smsc'));
        });
    }

}
