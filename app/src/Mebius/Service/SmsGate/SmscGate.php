<?php

namespace Mebius\Service\SmsGate;

/**
 * SMSC Gate
 *
 * @author topot
 */
class SmscGate implements SmsGateInterface
{

    /**
     * Sms-gate configuration data
     * @var array
     */
    private $conf = [];

    public function __construct(array $conf)
    {
        $this->conf = $conf;
    }

    public function send($to, $msg, $from = null)
    {
        if (!isset($from)) {
            $from = $this->conf['from'];
        }

        $url = 'http://www.smsc.ru/sys/send.php?'
            . 'login=' . $this->conf['login']
            . '&psw=' . $this->conf['pass']
            . '&phones=' . rawurlencode($to)
            . '&mes=' . rawurlencode($msg)
            . '&translit=' . $this->conf['translit']
            . '&time=0'
            . '&id=0'
            . '&flash=0'
            . '&charset=' . $this->conf['charset']
        //. '&sender=' . rawurlencode($from)
        ;
        //die($url);

        $result = file_get_contents($url);

        if (preg_match('/^OK - (\d+) SMS, ID - (\d+)/', $result, $m)) {
            return true;
        }
        return false;
    }

}
