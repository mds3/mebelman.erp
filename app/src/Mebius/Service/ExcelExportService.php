<?php

namespace Mebius\Service;

use \Mebius\Domain\ExportData;
use \PHPExcel;
use \PHPExcel_IOFactory;

/**
 * Description of ExcelExport
 *
 * @author 1
 */
class ExcelExportService
{

    private $data;

    public function __construct(ExportData $data = null)
    {
        if ($data) {
            $this->setData($data);
        }
    }

    function getData()
    {
        return $this->data;
    }

    function setData(ExportData $data)
    {
        $this->data = $data;
    }

    public function send(ExportData $data = null)
    {
        if ($data) {
            $this->setData($data);
        }

        if (!$this->data) {
            die('NO DATA FOR EXPORT');
        }

        $objPHPExcel = new PHPExcel();
        //echo '<pre>', print_r($objPHPExcel), '</pre>';

        $objPHPExcel->getProperties()->setCreator($this->data->getAuthor())
            ->setLastModifiedBy($this->data->getAuthor())
            ->setTitle($this->data->getTitle())
            ->setSubject($this->data->getTitle())
            ->setDescription($this->data->getTitle())
            ->setKeywords($this->data->getTitle())
            ->setCategory($this->data->getCategory());

        $objPHPExcel->getActiveSheet()->fromArray($this->data->getHead());

        $objPHPExcel->getActiveSheet()->fromArray(array_map(function($item) {
                return array_map(function($item) {
                    return (string) $item;
                }, $item);
            }, $this->data->getEntries()), '', 'A2');

        //var_dump($objPHPExcel);
        //die();
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->data->getTitle() . ' от ' . date("d-m-Y") . '.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

}
