<?php

namespace Mebius\Manager\Contracts;

/**
 * EntryManagerInterface
 * @author topot
 */
interface EntryManagerInterface
{

    public function getCreateData();

    public function getSearchData();

    public function getShowData($id);

    public function getEditData($id);

    public function getShowlogsData($id);

    public function getStatusData($id);

    public function getExportData($id);

    public function getStoreData(array $input);

    public function getIndexData(array $input);

    public function getUpdateData($id, array $input);
}
