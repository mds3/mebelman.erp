<?php

namespace Mebius\Manager;

use \Event;
use \Illuminate\Support\Facades\Auth;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Manager\Contracts\EntryManagerInterface;
use \Mebius\Repository\EntryRepository;
use \Mebius\Repository\Exceptions\NotFoundException;
use \URL;

/**
 * EntryManager
 *
 * @author topot
 */
class EntryManager extends Manager implements EntryManagerInterface
{

    public function __construct(EntryRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Массив данных для отображения списка заявок
     * @param array $input
     * @return array - data-array
     */
    public function getIndexData(array $input)
    {
        $input = $this->getAllowedOptionsInput(self::ENTITY_INDEX);

        $options = $this->getOptions(self::ENTITY_INDEX);

        $entries = $this->all($input);

        foreach ($entries as $entry) {
            $entry->url = URL::route(self::ENTITY_SHOW, $entry->id);
        }

        $data = [
          'entries' => $entries,
          'input' => $input,
          'options' => $options,
          'page_title' => trans('entry.entries'),
          'cells' => $this->getTypeCells($input),
          'icons' => $this->getIconsDict(),
        ];

        if (route_allowed(self::ENTITY_CREATE)) {
            $data['routes'][self::ENTITY_CREATE] = [
              'route' => URL::route(self::ENTITY_CREATE),
              'name' => trans('entry.new'),
            ];
        }

        if (route_allowed(self::ENTITY_SEARCH)) {
            $data['routes']['search'] = [
              'route' => URL::route(self::ENTITY_SEARCH),
              'name' => trans('entry.search'),
            ];
        }

        if (route_allowed(self::ENTITY_EXPORT)) {
            $data['routes']['export'] = [
              'route' => URL::route(self::ENTITY_EXPORT, $input),
              'name' => trans('entry.export'),
            ];
        }

        return $data;
    }

    /**
     * Массив данных для карточки заявки
     * @param int $id
     * @return array
     * @throws NotFoundException
     * @throws ActionNotAllowedException
     */
    public function getShowData($id)
    {
        try {
            $entry = $this->find($id);
            $this->checkIsAllowedAction(self::ENTITY_SHOW, $entry);
        } catch (NotFoundException $e) {
            throw $e;
        } catch (ActionNotAllowedException $e) {
            $e->setMessage(trans('entry.not_found'));
            throw $e;
        }

        Event::fire(self::ENTITY_SHOW, [$entry, $this]);

        $owner = (!is_null($entry->master) && Auth::id() == $entry->master->id);
        $canedit = route_allowed('order.edit');

        $data = [
          'route' => URL::route('entry.status', $entry->getKey()),
          'method' => 'PUT',
          'page_title' => trans('entry.page_title') . $entry->order_key,
          'routes' => [],
        ];

        if (action_allowed(self::ENTITY_SHOWLOGS, $entry)) {
            $data['routes']['showlogs'] = [
              'route' => URL::route(self::ENTITY_SHOWLOGS, $entry->getKey()),
              'name' => trans('msg.showlogs'),
              'options' => $entry->getKey(),
            ];
        }

        if (!$entry->is_archived) {

            if (action_allowed('entry.status', $entry) && $entry->is_refused) {
                $data['routes']['reanimate'] = [
                  'route' => URL::route('entry.status', $entry->getKey()),
                  'method' => 'PUT',
                  'name' => trans('entry.reanimate'),
                  'hidden' => [
                    '_status' => 'reanimate',
                  ],
                ];
            }

            if (!$entry->is_refused) {

                if (action_allowed(self::ENTITY_EDIT, $entry)) {
                    $data['routes']['edit'] = [
                      'route' => URL::route(self::ENTITY_EDIT, $entry->getKey()),
                      'method' => 'GET',
                      'name' => trans('entry.edit'),
                    ];
                }

                if ($entry->is_called && action_allowed('order.create', $entry)) {
                    $data['routes']['create'] = [
                      'route' => URL::route('order.create',
                          ['id' => $entry->getKey()]),
                      'method' => 'GET',
                      'name' => trans('order.create'),
                    ];
                }



                if (!$entry->visit_time_isset || $entry->is_deferred || $canedit) {
                    $data['routes']['visit'] = [
                      'name' => trans('entry.visit_time'),
                    ];
                }

                if (!$entry->is_refused && ($owner || $canedit)) {
                    $data['routes']['defer'] = [
                      'name' => trans('entry.do_defer'),
                    ];
                    $data['routes']['refuse'] = [
                      'name' => trans('entry.do_refuse'),
                    ];
                }
            } else {

                if (action_allowed('entry.archiving', $entry)) {
                    $data['routes']['archiving'] = [
                      'route' => URL::route('entry.status', $entry->getKey()),
                      'method' => 'PUT',
                      'name' => trans('entry.do_archived'),
                      'hidden' => [
                        '_status' => 'archiving',
                      ],
                    ];
                }
            }
        }

        //echo '<pre>', print_r($data['routes']), '</pre>';

        $data['entry'] = $entry;

        return $data;
    }

    /**
     *
     * @return array
     */
    public function getCreateData()
    {
        $masters = $this->repo->getMastersSelectableList($this->repo->getMasterRoleId());
        $suppliers = $this->repo->getSuppliersSelectableList();
        $refers = $this->getRefersList();
        $entry = $this->repo->getEmptyEntry();
        $routes = [
          'return' => [
            'route' => route(self::ENTITY_SHOW, $entry->getKey()),
            'name' => trans('msg.cancel'),
          ]
        ];
        return [
          'entry' => $entry,
          'masters' => $masters,
          'suppliers' => $suppliers,
          'refers' => $refers,
          'route' => route(self::ENTITY_STORE),
          'method' => 'POST',
          'routes' => $routes,
          'page_title' => trans('entry.adding'),
        ];
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getStoreData(array $input)
    {
        return [];
    }

    /**
     *
     * @param int $id
     * @return array
     */
    public function getEditData($id)
    {
        return [];
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getUpdateData($id, array $input)
    {
        return [];
    }

    public function getExportData($id)
    {
        return [];
    }

    public function getSearchData()
    {
        return [];
    }

    public function getShowlogsData($id)
    {
        return [];
    }

    public function getStatusData($id)
    {
        return [];
    }

}
