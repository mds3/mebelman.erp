<?php

namespace Mebius\Repository;

use \Event;
use \EventsLog;
use \Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;
use \Mebius\Repository\Exceptions\NotFoundException;
use \Order;

class OrderEloquentRepository extends AbstractTypedEloquentRepository implements OrderRepository
{

    use Traits\IconedTrait;

    public function __construct(Order $order)
    {
        /* @var $model Order */
        $this->model = $order;
    }

    public function cancelOrder(Order $order)
    {
        $order->canceled_at = date($order->getDateTimeFormat());
        $order->archived_at = date($order->getDateTimeFormat());
        $order->status = Order::STATUS_ARCHIVED;
        $order->cost = 0;
        $order->prepay = 0;
        $order->result_amount = 0;
        $order->cashbox = 0;
        $order->stuff()->delete();
        $order->payments()->delete();
        return $order->save();
    }

    public function archivate(Order $order)
    {
        $order->archived_at = date($order->getDateTimeFormat());
        $order->status = Order::STATUS_ARCHIVED;

        //echo '<pre>', print_r($order->toArray()), '</pre>';
        //die();

        return $order->save();
    }

    public function getPeriodOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getPeriodOptions());
    }

    public function getTypeOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getTypeOptions());
    }

    public function getOrderByOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getOrderByOptions());
    }

    public function getSearchOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getSearchOptions());
    }

    public function getSearchOptionsListWithTypes()
    {
        return array_map(function($item) {
            return [
              'name' => $item['name'],
              'type' => $item['type'],
            ];
        }, $this->model->getSearchOptions());
    }

    public function getInputTypes()
    {
        return [
          'type' => 'all',
          'period' => 'all',
          'created_by' => 'all',
          'order_by' => 'desc',
        ];
    }

    public function getDefaultValueByType($type)
    {
        if (array_key_exists($type, $this->getInputTypes())) {
            return $this->getInputTypes()[$type];
        }
        return 'all';
    }

    /**
     * Возвращает массив доступных для пользователя и роута опций для отображения
     * @param string $route_name имя роута
     * @return array
     */
    public function getOptions($route_name = 'order.index', $input = [])
    {
        $options['type'] = $this->getTypeOptionsList();
        $options['period'] = $this->getPeriodOptionsList();
        $options['order_by'] = $this->getOrderByOptionsList();

        if (!in_array('created_by', $this->getRouteAllowedOptions($route_name))) {
            $options['created_by'] = $this->getMastersSelectableList(
                $this->getMasterRoleId(), ['all' => 'Все мастера']
            );
        }

        return $options;
    }

    public function getTypeCells($input)
    {
        $type = isset($input['type']) ? $input['type'] : 'all';
        $arr = $this->model->getTypeCells($type);
        if (!isset($input['created_by']) || $input['created_by'] == 'all') {
            $arr += ['master_name' => ''];
        }
        return $arr;
    }

    private function getFilteredInputOptions($input = [])
    {

        $options['type'] = $this->model->getTypeOptions();
        $input['type'] = isset($input['type']) && isset($options['type'][$input['type']])
                ? $input['type'] : $this->getDefaultValueByType('type');



        $options['order_by'] = $this->model->getOrderByOptions();
        $input['order_by'] = isset($input['order_by']) && isset($options['order_by'][$input['order_by']])
                ? $input['order_by'] : $this->getDefaultValueByType('order_by');



        $options['period'] = $this->model->getPeriodOptions($input['type']);
        $input['period'] = isset($input['period']) && isset($options['period'][$input['period']])
                ? $input['period'] : $this->getDefaultValueByType('period');

        return [$input, $options];
    }

    public function getModelType()
    {
        return '\\Order';
    }

    public function getEmptyOrder()
    {
        return $this->createObject();
    }

    public function getOrder(array $input)
    {
        return $this->createObject($input);
    }

    /**
     *
     * @param array $input
     * @return boolean
     */
    public function create(array $input)
    {

        /* @var $created \Order */
        $created = parent::create($input);
        if (!$created) {
            return false;
        }

        $order = $this->findFirstBy('id', $created->getKey());

        //echo 'created<pre>', print_r($created->toArray()), '</pre>';
        //echo 'order<pre>', print_r($order->toArray()), '</pre>';
        //echo 'getDirty<pre>', print_r($created->getDirty()), '</pre>';

        $input['stuff'] = $this->getStuffArrFromInput($input, $created->getKey());

        //echo '<pre>', print_r($input['stuff']), '</pre>';
        //die();

        $order->stuff()->createMany($input['stuff']);

        $order->load(['stuff']);

        return $order;
    }

    public function update(Model $order, array $input = [])
    {
        /* @var $order \Order */

        $input['stuff'] = $this->getStuffArrFromInput($input, $order->id);

        foreach (['inplace', 'cashless'] as $type) {
            if (!isset($input[$type])) {
                $input[$type] = 0;
            }
            if ($input[$type] == $order->{$type}) {
                unset($input[$type]);
            }
        }

        $updated = parent::update($order, $input);

        if ($upd = $this->saveStuff($order, $input)) {
            $updated['stuff'] = '';
        }

        return $updated;
    }

    public function saveStuff(Order $order, array $input)
    {
        $input['stuff'] = $this->getStuffArrFromInput($input, $order->id);

        $stuff = $this->getStuffArrFromOrder($order);

        if ($stuff != $input['stuff']) {
            //echo '<pre>stuff ', print_r($stuff), '</pre>';
            //echo '<pre>input[stuff] ', print_r($input['stuff']), '</pre>';
            //die('NOT EQUALS');
            $order->stuff()->delete();
            $order->stuff = $order->stuff()->createMany($input['stuff']);
            $order->load(['stuff']);
            //echo '<pre>order: ', print_r($order->toArray()), '</pre>';
            //die();
            Event::fire('order.stuff_changed', $order);
            return true;
        }
        return false;
    }

    protected function getStuffArrFromOrder(Order $order)
    {
        $ret = [];
        foreach ($order->stuff as $i => $stuff) {
            $ret[$i]['id'] = $stuff->id;
            $ret[$i]['order_id'] = $stuff->order_id;
            $ret[$i]['name'] = $stuff->name;
            $ret[$i]['quantity'] = $stuff->quantity;
            $ret[$i]['cost'] = $stuff->cost;
        }
        return $ret;
    }

    protected function getStuffArrFromInput(array $input, $order_id)
    {
        $ret = [];

        if (!isset($input['stuff_name']) || empty($input['stuff_name'])) {
            return $ret;
        }

        foreach ($input['stuff_name'] as $i => $name) {
            if (!empty($name)) {
                $ret[$i] = [
                  'id' => isset($input['id'][$i]) ? $input['id'][$i] : null,
                  'order_id' => $order_id,
                  'name' => $name,
                  'quantity' => trim($input['stuff_quantity'][$i]),
                ];
                if (isset($input['stuff_cost'][$i])) {
                    $ret[$i]['cost'] = trim($input['stuff_cost'][$i]);
                }
            }
        }
        return $ret;
    }

    public function getEventsLog(Order $order)
    {
        return EventsLog::with([
                  'creator' => function($query) {
                      $query->select('id', 'name');
                  }
                ])
                ->where('model', '=', 'Order')
                ->where('model_id', '=', $order->id)
                ->get();
    }

    public function all(array $input = [], array $columns = ['*'])
    {
        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
          'operator' => function($query) {
              $query->select('id', 'name');
          },
            /* 'updater' => function($query) {
              $query->select('id', 'name');
              }, */
        ]);

        list($input, $options) = $this->getFilteredInputOptions($input);
        //echo 'options:<pre>', print_r($options), '</pre>';

        $where = $this->getWhereByOptions($input, $options);
        //echo '<pre>', print_r($where), '</pre>';
        //die();

        $query = $this->getQueryByWhere($where);

        return $query->paginate(static::LIMIT_PER_PAGE, $columns);
        //return $query->get($columns);
    }

    public function allForExport(array $input = [])
    {
        $columns = $this->model->getShortStatement();

        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
          'supplier' => function($query) {
              $query->select('id', 'name');
          },
          'refer' => function($query) {
              $query->select('id', 'name');
          },
          'operator' => function($query) {
              $query->select('id', 'name');
          },
        ]);

        list($input, $options) = $this->getFilteredInputOptions($input);

        $where = $this->getWhereByOptions($input, $options);

        $query = $this->getQueryByWhere($where);

        $entries = $query->get($columns);

        return $this->getNormalizedCollectionAsArray($entries);
    }

    /**
     *
     * @param int $id
     * @return Collection|Order
     */
    public function find($id)
    {
        return $this->model->with([
              'master' => function($query) {
                  $query->select('id', 'name', 'phone', 'email');
              },
              'refer' => function($query) {
                  $query->select('id', 'name');
              },
              'stuff' => function($query) {
                  $query->select('id', 'order_id', 'name', 'quantity', 'cost');
              },
              'supplier' => function($query) {
                  $query->select('id', 'name', 'phone', 'email');
              },
              'operator' => function($query) {
                  $query->select('id', 'name', 'phone');
              },
              'payments' => function($query) {
                  $query->select('id', 'order_id', 'summ', 'created_at');
              }
            ])->find($id);
    }

    /**
     * Возвращает заказ с минимумом данных и всеми платежами
     * @param int $id
     * @return Collection|Order
     * @throws NotFoundException
     */
    public function findWithPayments($id)
    {
        $order = $this->model->with([
              'master' => function($query) {
                  $query->select('id', 'name');
              },
              'payments' => function($query) {
                  $query->select('id', 'order_id', 'summ', 'created_at');
              }
            ])->find($id);
        if (!$order) {
            throw new NotFoundException(trans('order.not_found'));
        }

        return $order;
    }

    public function getNormalizedCollectionAsArray(Collection $Entries)
    {
        $entries = [];

        foreach ($Entries as $i => $Entry) {
            $entries[$i] = $entry = $Entry->toArray();
            $entries[$i]['master'] = $entries[$i]['master']['name'];
            $entries[$i]['supplier'] = $entries[$i]['supplier']['name'];
            foreach ($entry as $k => $ent) {
                $entries[$i][$k] = is_array($ent) ? $ent['name'] : $ent;
            }

            $entries[$i]['inplace'] = $entries[$i]['inplace'] ? 'на месте' : 'в цеху';
            $entries[$i]['docs_signed'] = $entries[$i]['cashless'] ? ($entries[$i]['docs_signed']
                        ? 'да' : 'нет') : '-';
            $entries[$i]['cashless'] = $entries[$i]['cashless'] ? 'безнал.' : 'нал.';
            $entries[$i]['status'] = trans('status.' . strtolower($Entry->status));
        }
        return $entries;
    }

    public function getCountByOptions($input)
    {

        list($input, $options) = $this->getFilteredInputOptions($input);

        $where = $this->getWhereByOptions($input, $options);

        $query = $this->getQueryByWhere($where);

        return $query->count();
    }

    public function finishOrder(Order $order, array $input)
    {
        /* @var $order Order */
        //echo '<pre>', print_r($order->toArray()), '</pre>';

        $order->fill($input);

        if (!$order->cashless && $order->has_client_debt) {
            $this->addError('Заказ не может быть завершен, т.к. имеется долг клиента.');
            return false;
        }

        $order->{Order::FINISHED_TIME} = date($order->getDateTimeFormat());

        $order->status = Order::STATUS_FINISHED;

        //echo '<pre>', print_r($order->toarray()), '</pre>';
        //die();
        $res = $order->save();

        if (!$res) {
            //echo '<pre>', print_r($order->getErrors()), '</pre>';
            //die();
        }

        return $res;
    }

    public function closeOrder(Order $order)
    {
        //$order->status = Order::STATUS_CLOSED;
        if ($order->has_cashbox_debt) {
            $this->addError('Заказ не может быть закрыт, т.к. имеется долг в кассу.');
            return false;
        }
        $order->status = Order::STATUS_ARCHIVED;
        $order->setArchivedAt();
        return $order->save();
    }

    private function getSearchWhere(array $input = [])
    {
        $options = $this->model->getSearchOptions($input);
        $where = [];
        foreach ($input as $key => $val) {
            if (isset($options[$key])) {
                foreach ($options[$key]['conditions'] as $i => $cond) {
                    $where[] = [$i => $cond];
                }
            }
        }
        return $where;
    }

    public function searchByInputOptions(array $input)
    {

        $arr[$input['search_by']] = $input['search_val'];

        $where = $this->getSearchWhere($arr);

        //echo '<pre>', print_r($where), '</pre>';
        //die();

        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
            /* 'creator' => function($query) {
              $query->select('id', 'name');
              },
              'updater' => function($query) {
              $query->select('id', 'name');
              }, */
        ]);

        //list($input, $options) = $this->getFilteredInputOptions($input);
        //$where = $this->getWhereByOptions($arr, $options);
        //echo '<pre>', print_r($where), '</pre>';

        $query = $this->getQueryByWhere($where);

        return $query->get();
    }

}
