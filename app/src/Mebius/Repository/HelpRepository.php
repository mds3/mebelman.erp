<?php

namespace Mebius\Repository;

use \Mebius\Model\Contracts\HelpInterface;
use \Mebius\Repository\Contracts\HelpRepositoryInterface;

class HelpRepository implements HelpRepositoryInterface
{

    public function __construct(HelpInterface $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        $sections = $this->model->get();
        foreach ($sections as $key => $section) {
            if ($key != 'main' && !route_allowed($key . '.index')) {
                unset($sections[$key]);
            }
        }
        return $sections;
    }

}
