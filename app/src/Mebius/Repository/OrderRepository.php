<?php

namespace Mebius\Repository;

use \Mebius\Repository\LaravelRepositories\Contracts\RepositoryInterface;
use \Order;

interface OrderRepository extends RepositoryInterface
{

    /**
     * Возвращает список типов отображения списка заявок
     */
    public function getTypeOptionsList();

    /**
     * Возвращает список периодов отображения списка заявок
     */
    public function getPeriodOptionsList();

    /**
     * Возвращает список сортировок отображения списка заявок
     */
    public function getOrderByOptionsList();

    /**
     * Возвращает список типов поиска
     */
    public function getSearchOptionsList();

    /**
     * Возвращает список вариантов поиска с типами значений
     */
    public function getSearchOptionsListWithTypes();

    /**
     * Возвращает массив доступных опций для выпадающих списков
     * @return array key = default_value
     */
    public function getInputTypes();

    /**
     * Возвращает полный список опций
     */
    public function getOptions();

    /**
     *
     * @param array $options
     */
    public function allForExport(array $options);

    /**
     * Возвращает пустой объект заказа
     * @return Order
     */
    public function getEmptyOrder();

    /**
     * Возвращает объект заказа
     * @return Order
     */
    public function getOrder(array $input);

    /**
     *
     * @param Order $order
     * @param array $input
     */
    public function finishOrder(Order $order, array $input);

    /**
     *
     * @param Order $order
     * @param array $input
     */
    public function closeOrder(Order $order);

    /**
     *
     * @param Order $order
     */
    public function cancelOrder(Order $order);

    /**
     *
     * @param array $input
     */
    public function searchByInputOptions(array $input);

    /**
     * @param Order $order
     */
    public function archivate(Order $order);
}
