<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Repository;

use \App;
use \Auth;
use \Illuminate\Database\Eloquent\Builder;
use \Illuminate\Database\Eloquent\Model;
use \Input;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Repository\LaravelRepositories\EloquentRepository;
use \Role;
use \Session;

/**
 * Description of AbstractTypedEloquentRepository
 *
 * @author 1
 */
abstract class AbstractTypedEloquentRepository extends EloquentRepository
{

    /**
     *
     */
    abstract function getInputTypes();

    /**
     * Возвращает массив доступных для пользователя и роута опций для отображения
     * @param string $route_name имя роута
     * @return array
     */
    abstract public function getOptions($route_name, $input);

    /**
     * Возвращает массив разрешенных опций
     * из полученного базового массива опций
     * @param array $options
     * @return array
     */
    protected function getPermittedOptions(array $options)
    {
        return array_filter($options,
            function($item) {
            foreach ($item['permissions'] as $route) {
                //echo "check route: $route - ";
                if (route_allowed($route)) {
                    //echo "allowed - return true<br/>";
                    return true;
                } else {
                    //echo "NOT allowed<br/>";
                }
            }
            return false;
        });
    }

    /**
     *
     * @param string $type
     * @return string
     */
    public function getDefaultValueByType($type)
    {
        if (array_key_exists($type, $this->getInputTypes())) {
            return $this->getInputTypes()[
                $type];
        }
        return 'all';
    }

    /**
     *
     * @param array $input
     * @param array $options
     * @return array
     */
    protected function getWhereByOptions(array $input, array $options)
    {

//echo 'input<pre>', print_r($input), '</pre>';
//echo 'options<pre>', print_r($o ptions),  '</pre>';

        $where = [];

        foreach ($input as $key => $val) {
            if (isset($options[$key][$val])) {
                foreach ($options[$key][$val]['conditions'] as $i => $cond) {
                    $where[] = [$i => $cond];
                }
            } elseif (array_key_exists($key, $this->getInputTypes()) && $val !== $this->getDefaultValueByType($key)) {
//echo 'here <br/>';
                $where[] = ['where' => [
                    [$key, '=', (int) $val],
                  ],
                ];
            }
        }

        return $where;
    }

    /**
     *
     * @param array $whereArr
     * @return Builder
     */
    protected function getQueryByWhere(array $whereArr = [])
    {
        $query = $this->make();

        if (!empty($whereArr)) {
            foreach ($whereArr as $where) {
                $this->setWhereToQuery($query, $where);
            }
        }
        return $query;
    }

    protected function setWhereToQuery($query, $where)
    {
        if (empty($where)) {
            return;
        }

        foreach ($where as $func => $_where) {
            foreach ($_where as $_w) {
                switch (count($_w)) {
                    case 2:
                        $query->$func($_w[0],
                            $_w [
                            1]);
                        break;
                    default:
                        $query->$func($_w[0], $_w[1], $_w[2]);
                        break;
                }
            }
        }
    }

    public function getRouteAllowedOptions($route_name)
    {
        return get_allowed_options($route_name);
    }

    /**
     * Проверяет на существование id пользователя в массиве, если существует
     * @param array $input
     * @param string $key ключ массива для id пользователя
     * @return array
     */
    public function checkUserIdInput(array $input, $key = 'user_id')
    {
        if (isset($input[$key])) {
            $input[$key] = (int) $input[$key];
            if (!$input[$key] || !array_key_exists($input[
                    $key],
                    $this
                        ->getMastersList($this
                            ->getMasterRoleId()))) {
                unset($input[$key]);
            }
        }
        return $input;
    }

    /**
     * Возвращает список мастеров
     * @staticvar array $list
     * @param int $role_id
     * @return array
     */
    public function getMastersList(array $role_id)
    {
        static $list = null;

        if ($list) {
            return $list;
        } else {
            /* @var $repo UserRepository */
            $repo = App::make('\Mebius\Repository\UserRepository');
            $list = $repo->getRoleUsersList($role_id);

            return $list;
        }
    }

    public function getMastersSelectableList(array $role_id, array $def = [])
    {
        $list = $this->getMastersList($role_id);
//array_unshift($list, '-выбрать-');
//$list[0] = 'не назначен';
        ksort($list);
        if (!empty($def)) {
            //$list[key($def)] = $def[key($def)];
            $list = $def + $list;
        }
//echo '<pre>', print_r($list), '</pre>';
        return $list;
    }

    public function getMasterRoleId()
    {
        //echo '<pre>', print_r(\Group::with()->find(1)->users->toArray()), '</pre>';
        //die();
        return [3, 5];
    }

    public function getAllowedOptionsInput($route_name)
    {
        $key = $this->getStoredOptionsKey($route_name);

        $input = Session::get($key, []);

        $types = $this->getInputTypes();

        foreach ($types as $type => $def) {
            $input[$type] = Input::has($type) ? Input::get($type) : Session::get("$key.$type",
                    $def);
        }

        if (in_array('user_id', $this->getRouteAllowedOptions($route_name))) {
            $input['user_id'] = Auth::id();
        }

        if (in_array('created_by', $this->getRouteAllowedOptions($route_name))) {
            $input['created_by'] = Auth::id();
        }

        Session::set($key, $input);

        //echo '<pre>Session: ', print_r(Session::get($key, [])), '</pre>';

        return $input;
    }

    public function flushInputOptions($route_name)
    {
        $key = $this->getStoredOptionsKey($route_name);
        Session::forget($key);
    }

    public function getStoredOptionsKey($route_name)
    {
        return $route_name . '.lists';
    }

    public function getDayOptionsList()
    {
        return [$this
              ->getDefaultValueByType('day') => 'Все дни'] + array_combine(range(1,
                    31), range(1, 31));
        //return array_combine(range(1, 31), range(1, 31));
    }

    /**
     *
     * @param bool $withAll добавлять в список параметр Все, или нет
     * @return array
     */
    public function getMonthOptionsList($withAll = true)
    {
        return $withAll ? ['all' => 'Все месяца'] + $this
                ->getMonth() : $this
                ->getMonth();
    }

    public function getMonth($num = false)
    {
        $arr = array_combine(range(1, 12),
            [
          'январь',
          'февраль',
          'март',
          'апрель',
          'май',
          'июнь',
          'июль',
          'август',
          'сентябрь',
          'октябрь',
          'ноябрь',
          'декабрь',
        ]);
        if ($num === false) {
            return $arr;
        } else {
            $num = (int) $num;
            if (isset($arr[$num])) {
                return $arr[$num];
            } else {
                return ' - ';
            }
        }
    }

    public function getYearOptionsList()
    {
        return array_combine(range(2014, date("Y"
            )), range(2014, date("Y")));
    }

    public function getOrderByOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model
                ->getOrderByOptions());
    }

    /**
     *
     * @param string $route_name
     * @param Model $model
     * @return boolean
     * @throws ActionNotAllowedException
     */
    protected function checkIsAllowedAction($route_name, Model $model)
    {
        if (!action_allowed($route_name, $model)) {
            throw new ActionNotAllowedException(trans('msg.action_not_allowed'));
        }
        return true;
    }

}
