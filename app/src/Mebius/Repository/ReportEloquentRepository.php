<?php

namespace Mebius\Repository;

use \DB;
use \Illuminate\Database\Eloquent\Collection;
use \Payment;
use \Report;
use \stdClass;
use \URL;

class ReportEloquentRepository extends AbstractTypedEloquentRepository implements ReportRepository
{

    private $filtered_input = [];

    public function __construct(Report $model)
    {
        $this->model = $model;
    }

    public function all(array $input = [], array $columns = ['*'])
    {
        return $this->getReports($input);
    }

    public function allForExport(array $input)
    {
        return $this->getNormalizedCollectionAsArray($this->all($input));
    }

    public function getNormalizedCollectionAsArray(Collection $Entries)
    {
        $entries = [];

        $fields = $this->getReportFields();

        foreach ($Entries as $i => $Entry) {
            foreach ($fields as $field) {
                $entries[$i][$field] = $Entry->{$field};
            }
        }
        return $entries;
    }

    private function getReports(array $input)
    {
        $input = $this->checkUserIdInput($input);

        if (isset($input['user_id'])) {
            return $this->getReportsByMaster($input);
        } else {
            return $this->getReportsByPeriod($input);
        }
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function filterInput(array $input)
    {
        $input = $this->checkUserIdInput($input);

        if (isset($input['user_id'])) {
            return $this->filterMasterInput($input);
        } else {
            return $this->filterPeriodInput($input);
        }
    }

    /**
     *
     * @param array $input
     * @return Collection
     */
    private function getReportsByPeriod($input)
    {

        $input = $this->filterPeriodInput($input);

        //echo '<pre>', print_r($input), '</pre>';

        if (!$masters = $this->getMastersList($this->getMasterRoleId())) {
            return $this->getCollection();
        }

        //$this->orderRepo->
        $sub = '';

        if (isset($input['year']) && $input['year']) {
            $sub .= ' AND YEAR(created_at) = ' . $input['year'];
        }

        if (isset($input['month']) && $input['month'] && $input['month'] != 'all') {
            $sub .= ' AND MONTH(created_at) = ' . $input['month'];
        }

        if (isset($input['day']) && $input['day']) {
            $sub .= ' AND DAY(created_at) = ' . $input['day'];
        }

        $sql = '
              select created_by as user_id,
              count(*) as orderCnt,
              SUM(cost) as orderSumm,
              SUM(prepay) as prepaySumm,
              SUM(result_amount) as amountSumm,
              SUM(cashbox) as cashboxSumm,
              SUM(prepay) + SUM(result_amount) - SUM(cashbox) as masterDebt,
              SUM(cost) - SUM(prepay) - SUM(result_amount) as clientDebt
              from orders
              WHERE created_by IN (' . implode(',', array_keys($masters)) . ')';
        $sql .= $sub . ' GROUP BY created_by';

        //echo $sql . '<br/>';

        $orders = DB::select(DB::raw($sql));

        //echo '<pre>', print_r($orders), '</pre>';

        $sql = 'SELECT user_id,COUNT(DISTINCT id) as entryCnt FROM entries
                WHERE user_id IN (' . implode(',', array_keys($masters)) . ')';
        $sql .= $sub . ' GROUP BY user_id';

        $entries = DB::select(DB::raw($sql));

        $reports = [];

        foreach ($masters as $id => $name) {

            $reports[$id] = new stdClass();

            foreach ($this->getReportFieldsDictonary() as $fld => $val) {
                $reports[$id]->{$fld} = $val;
            }

            $reports[$id]->user_id = $id;
            $reports[$id]->master = $name;
            $reports[$id]->link_url = URL::route('report.index',
                    ['user_id' => $id]);
            $reports[$id]->name = $name;

            foreach ($orders as $report) {
                if ($report->user_id == $id) {
                    foreach ($report as $key => $val) {
                        $reports[$id]->{$key} = $val;
                    }
                }
            }

            foreach ($entries as $report) {
                if ($report->user_id == $id) {
                    foreach ($report as $key => $val) {
                        $reports[$id]->{$key} = $val;
                    }
                }
            }
        }

        $all = new stdClass();
        foreach ($reports as $id => $report) {
            foreach ($report as $key => $val) {
                if (!isset($all->{$key})) {
                    $all->{$key} = 0;
                }
                $all->{$key} += $val;
            }
        }
        $all->name = 'Итого:';
        $all->user_id = 0;
        $reports['all'] = $all;

        //echo '<pre>', print_r($orders), '</pre>';
        //echo '<pre>arr: ', print_r($reports), '</pre>';
        //die();
        return $this->getCollection($reports);
    }

    /**
     *
     * @param array $input
     * @return Collection
     */
    private function getReportsByMaster($input)
    {
        $input = $this->filterMasterInput($input);

        //echo '<pre>input2: ', print_r($input), '</pre>';

        $sql = "SELECT created_by as user_id,
                YEAR(created_at) as oyear,
                MONTH(created_at) as omon,
                CONCAT(YEAR(created_at),'/',MONTH(created_at)) as period,
                SUM(cost) as orderSumm,
                SUM(prepay) as prepaySumm,
                SUM(result_amount) as amountSumm,
                SUM(cashbox) as cashboxSumm,
                SUM(prepay) + SUM(result_amount) - SUM(cashbox) as masterDebt,
                SUM(cost) - SUM(prepay) - SUM(result_amount) as clientDebt,
                COUNT(DISTINCT id) as orderCnt
                FROM orders
                WHERE created_by = " . $input['user_id'] . "
                AND created_at BETWEEN STR_TO_DATE('" . $input['date1'] . "','%Y-%m-%d')
                AND STR_TO_DATE('" . $input['date2'] . "','%Y-%m-%d')
                GROUP BY oyear,omon
                ";

        $orders = DB::select(DB::raw($sql));
        //echo '<pre>orders: ', print_r($orders), '</pre>';
        //die();

        $sql = "SELECT user_id,COUNT(DISTINCT id) as entryCnt,
                YEAR(created_at) as oyear,
                MONTH(created_at) as omon,
                CONCAT(YEAR(created_at),'/',MONTH(created_at)) as period
                FROM entries
                WHERE user_id = " . $input['user_id'] . "
                AND created_at BETWEEN STR_TO_DATE('" . $input['date1'] . "','%Y-%m-%d')
                AND STR_TO_DATE('" . $input['date2'] . "','%Y-%m-%d')
                GROUP BY oyear,omon";

        $entries = DB::select(DB::raw($sql));

        //echo '<pre>entries: ', print_r($entries), '</pre>';

        $reports = [];

        $periods = $this
            ->getPeriodsArrFromInput($input);
        //echo '<pre>periods: ', print_r($periods), '</pre>';

        foreach ($periods as $id => $period) {

            list($year, $month) = explode('/', $period);

            $reports[$id] = new stdClass();

            foreach ($this->getReportFieldsDictonary() as $fld => $val) {
                $reports[$id]->{$fld} = $val;
            }

            $reports[$id]->user_id = $input['user_id']; //$id;
            $reports[$id]->master = 0; //$name;
            $reports[$id]->link_url = URL::route('report.index',
                    [
                  'month' => $month,
                  'year' => $year,
                  'user_id' => $this->getDefaultValueByType('user_id'),
            ]);
            $reports[$id]->name = $year . ' / ' . $this
                    ->getMonth($month);

            foreach ($orders as $report) {
                if ($report->period == $period) {
                    foreach ($report as $key => $val) {
                        $reports[$id]->{$key} = $val;
                    }
                }
            }

            foreach ($entries as $report) {
                if ($report->period == $period) {
                    foreach ($report as $key => $val) {
                        $reports[$id]->{$key} = $val;
                    }
                }
            }
        }

        $all = new stdClass();
        foreach ($reports as $id => $report) {
            foreach ($report as $key => $val) {
                if (!isset($all->{$key})) {
                    $all->{$key} = 0;
                }
                $all->{$key} += $val;
            }
        }
        $all->name = 'Итого:';
        $all->user_id = 0;
        $reports['all'] = $all;

//echo '<pre>', print_r($orders), '</pre>';
//echo '<pre>arr: ', print_r($reports), '</pre>';
//die();
        return $this
                ->getCollection($reports);
    }

    private function getPeriodsArrFromInput(array $input)
    {
        $ret = [];

        if ($input['y1'] == $input['y2']) {
//echo 'Years equals<br/>';
            $ret = array_map(function($item) use ($input) {
                return $input['y1'] . '/' . $item;
            }, range($input['m1'], $input['m2']));
        } else {
//echo 'Years NOT equals<br/>';
            $ret = array_merge(array_map(function($item) use ($input) {
                    return $input['y1'] . '/' . $item;
                }, range($input['m1'], 12
                )),
                array_map(function($item) use ($input) {
                    return $input['y2'] . '/' . $item;
                }, range(1, $input['m2'])));
//$ret += range(1, $input['m2']);
        }

        return $ret;
    }

    /**
     *
     * @param int $id
     * @return Payment
     */
    public function find($id)
    {
        return parent::find($id);
    }

    public function getReportFieldsDictonary()
    {
        return [
          'name' => 'Итого:',
          'entryCnt' => 0,
          'orderCnt' => 0,
          'orderSumm' => 0,
          'prepaySumm' => 0,
          'amountSumm' => 0,
          'cashboxSumm' => 0,
          'masterDebt' => 0,
          'clientDebt' => 0,
        ];
    }

    public function getReportFields()
    {
        return array_keys($this->getReportFieldsDictonary());
    }

    public function getReportFieldsValues()
    {
        return array_map(function ($value) {
            $value = trans('report.' . $value);
            $value = is_array($value) ? $value['name'] : $value;
            return $value;
        }, $this->getReportFields());
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getAllowedInputKeys(array $input)
    {
        if (isset($input['user_id'])) {
            return $this->getMasterAllowedInputKeys();
        } else {
            return $this->getPeriodAllowedInputKeys();
        }
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getAllowedInputDictionary(array $input)
    {
        if (isset($input['user_id'])) {
            return $this->getMasterAllowedInputDictionary();
        } else {
            return $this->getPeriodAllowedInputDictionary();
        }
    }

    /**
     *
     * @return array
     */
    public function getPeriodAllowedInputDictionary()
    {
        return [
          'user_id' => 'user_id',
          'day' => 'day',
          'month' => 'month',
          'year' => 'year',
        ];
    }

    /**
     *
     * @return array
     */
    public function getMasterAllowedInputDictionary()
    {
        return [
          'user_id' => 'user_id',
          'm1' => 'month',
          'y1' => 'year',
          'm2' => 'month',
          'y2' => 'year',
        ];
    }

    /**
     *
     * @return array
     */
    public function getPeriodAllowedInputKeys()
    {
        return array_keys($this->getPeriodAllowedInputDictionary());
    }

    /**
     *
     * @return array
     */
    public function getMasterAllowedInputKeys()
    {
        return array_keys($this->getMasterAllowedInputDictionary());
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function filterPeriodInput(array $input)
    {
        $only = $this->getPeriodAllowedInputKeys();

        //echo '<pre>filterPeriodInput: ', print_r($input), '</pre>';

        $this->sanitizeInput($input, $only);

        //echo '<pre>filterPeriodInput: ', print_r($input), '</pre>';

        if (isset($this->filtered_input['period'])) {
            return $this->filtered_input['period'];
        }

        if (isset($input['day']) && ($input['day'] == $this
                ->getDefaultValueByType('day') || !isset($this
                    ->getDayOptionsList()[$input['day']]))) {
            unset($input['day']);
        }

        if (!isset($input['month']) || !isset($this
                    ->getMonthOptionsList()[$input['month']])) {
            $input['month'] = $this
                ->getDefaultValueByType('month');
        }

        if (!isset($input['year']) || !isset($this
                    ->getYearOptionsList()[$input['year']])) {
            $input['year'] = $this
                ->getDefaultValueByType('year');
        }

        $this->filtered_input['period'] = $input;

        return $input;
    }

    private function sanitizeInput(array &$input, array $only)
    {
        foreach ($input as $key => $val) {
            if (!in_array($key, $only)) {
                unset($input[$key]);
            }
        }
    }

    public function filterMasterInput(array $input)
    {
        if (isset($this->filtered_input['master'])) {
            return $this->filtered_input['master'];
        }

        $only = $this->getMasterAllowedInputKeys();

        $this->sanitizeInput($input, $only);

        if (!isset($input['m1']) || !isset($this
                    ->getMonthOptionsList()[$input['m1']])) {
            $input['m1'] = $this
                ->getDefaultValueByType('month');
        }

        if (!isset($input['m2']) || !isset($this
                    ->getMonthOptionsList()[$input['m2']])) {
            $input['m2'] = $this
                ->getDefaultValueByType('month');
        }

        if (!isset($input['y1']) || !isset($this
                    ->getYearOptionsList()[$input['y1']])) {
            $input['y1'] = $this
                ->getDefaultValueByType('year');
        }

        if (!isset($input['y2']) || !isset($this
                    ->getYearOptionsList()[$input['y2']])) {
            $input['y2'] = $this
                ->getDefaultValueByType('year');
        }

        $input['date1'] = $input['y1'] . '-' . $input['m1'];

        $input['date2'] = $input['y2'] . '-' . $input['m2'];

        $time1 = strtotime($input['date1']);
        $time2 = strtotime($input['date2']);

        if ($time1 > time()) {
            $input['y1'] = $this
                ->getDefaultValueByType('year');
            $input['m1'] = $this
                ->getDefaultValueByType('month');
            $input['date1'] = $input['y1'] . '-' . $input['m1'];
        }

        if ($time2 > time()) {
            $input['y2'] = $this
                ->getDefaultValueByType('year');
            $input['m2'] = $this
                ->getDefaultValueByType('month');
            $input['date2'] = $input['y2'] . '-' . $input['m2'];
        }

        if ($time1 > $time2) {
            $date1 = $input['date1'];
            $m1 = $input['m1'];
            $y1 = $input['y1'];
            $input['date1'] = $input['date2'];
            $input['m1'] = $input['m2'];
            $input['y1'] = $input['y2'];
            $input['date2'] = $date1;
            $input['m2'] = $m1;
            $input['y2'] = $y1;
        }

        $input['date1'] .= '-1';
        $input['date2'] .= '-' . date('t', strtotime($input['date2'] . '-1'));

        $this->filtered_input['master'] = $input;

        return $input;
    }

    public function getModelType()
    {
        return '\\Report';
    }

    public function getInputTypes()
    {
        return [
          'day' => 'all',
          'month' => date('n'),
          'year' => date('Y'),
          'm1' => date('n'),
          'y1' => date('Y'),
          'm2' => date('n'),
          'y2' => date('Y'),
          'user_id' => 'all',
          'order_by' => 'desc',
        ];
    }

    /**
     *
     * @param array $options
     * @return type
     */
    protected function getPermittedOptions(array $options)
    {
        return array_filter($options,
            function($item) {
            foreach ($item['permissions'] as $route) {
                //echo "check route: $route - ";
                if (route_allowed($route)) {
                    //echo "allowed - return true<br/>";
                    return true;
                } else {
                    //echo "NOT allowed<br/>";
                }
            }
            return false;
        });
    }

    /**
     * Возвращает массив доступных для пользователя и роута опций для отображения
     * @param string $route_name имя роута
     * @return array
     */
    public function getOptions($route_name = 'cashbox.index', $input = [])
    {
        if (isset($input['user_id']) && $input['user_id'] != $this->getDefaultValueByType('user_id') && array_key_exists($input[
                'user_id'], $this->getMastersList($this->getMasterRoleId()))) {
            return $this->getMasterOptions($route_name);
        } else {
            return $this->getPeriodOptions($route_name);
        }
    }

    private function getPeriodOptions($route_name)
    {
        $options['day'] = $this->getDayOptionsList();
        $options['month'] = $this->getMonthOptionsList();
        $options['year'] = $this->getYearOptionsList();
        //$options['order_by'] = $this->getOrderByOptionsList();

        if (!in_array('user_id', $this->getRouteAllowedOptions($route_name))) {
            $options['user_id'] = $this->getMastersSelectableList(
                $this->getMasterRoleId(), ['all' => 'Все мастера']
            );
        }
        return $options;
    }

    private function getMasterOptions($route_name)
    {

        $options['m1'] = $this->getMonthOptionsList(false);
        $options['y1'] = $this->getYearOptionsList();

        $options['m2'] = $this->getMonthOptionsList(false);
        $options['y2'] = $this->getYearOptionsList();

        if (!in_array('user_id', $this->getRouteAllowedOptions($route_name))) {
            $options['user_id'] = $this->getMastersSelectableList(
                $this->getMasterRoleId(), ['all' => 'Все мастера']
            );
        }
        return $options;
    }

}
