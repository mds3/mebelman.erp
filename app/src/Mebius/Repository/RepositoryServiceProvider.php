<?php

namespace Mebius\Repository;

use \Entry;
use \Help;
use \Illuminate\Support\ServiceProvider;
use \Order;
use \Payment;
use \Report;
use \Role;
use \User;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        //  binding interface HolidayRepository to the concrete implementation of EloquentHolidayRepository
        $this->app->bind(
            '\\Mebius\\Repository\\EntryRepository',
            function() {
            return new EntryEloquentRepository(new Entry());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\OrderRepository',
            function() {
            return new OrderEloquentRepository(new Order());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\UserRepository',
            function() {
            return new UserEloquentRepository(new User());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\RoleRepository',
            function() {
            return new RoleEloquentRepository(new Role());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\CashboxRepository',
            function() {
            return new CashboxEloquentRepository(new Payment());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\ReportRepository',
            function() {
            return new ReportEloquentRepository(new Report());
        });
        $this->app->bind(
            '\\Mebius\\Repository\\Contracts\\HelpRepositoryInterface',
            function() {
            return new HelpRepository(new Help());
        });
        $this->app->bind(
            '\Mebius\Repository\MainRepositoryInterface',
            function() {
            return new MainRepository();
        });
    }

}
