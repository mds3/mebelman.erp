<?php

namespace Mebius\Repository;

/**
 * MainRepository
 *
 * @author topot
 */
interface MainRepositoryInterface
{

    //////////////////
    // MainManager
    public function getIndexData();

    //////////////////
    // MainRepository
}
