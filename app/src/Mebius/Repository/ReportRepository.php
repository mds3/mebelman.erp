<?php

namespace Mebius\Repository;

use \Mebius\Repository\LaravelRepositories\Contracts\RepositoryInterface;

interface ReportRepository extends RepositoryInterface
{

    public function filterInput(array $input);
}
