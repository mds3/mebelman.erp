<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Repository\Traits;

/**
 * Description of IconedTrait
 *
 * @author 1
 */
trait IconedTrait
{

    public function getIconsDict()
    {
        return $this->model->getIconsDict();
    }

}
