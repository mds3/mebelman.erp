<?php

namespace Mebius\Repository;

use \App;
use \Illuminate\Database\Eloquent\Collection;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Repository\Exceptions\NotFoundException;
use \Payment;
use \URL;

class CashboxEloquentRepository extends AbstractTypedEloquentRepository implements CashboxRepository
{

    public function __construct(Payment $model)
    {
        $this->model = $model;
    }

    public function all(array $input = [], array $columns = ['*'])
    {

        $input = $this->filterInput($input);
        //echo '_input:<pre>', print_r($input), '</pre>';

        $master = function($query) {
            $query->select('id', 'name');
        };

        $order = function($query) use ($master) {
            //$query->select('id', 'order_key', 'status', 'created_by');
            $query->with(['master']);
        };

        $this->with(['order' => $order]);

        $query = $this->make();

        if (isset($input['user_id']) && $input['user_id']) {
            $query->whereHas('order',
                function ($q) use ($input) {
                $q->where('created_by', $input['user_id']);
            });
        }

        if ($input['cash'] != $this->getDefaultValueByType('cash')) {
            $query->where('cash', $input['cash']);
        }

        $this->setDateToQuery($query, $input);

        $payments = $query->get($columns);

        foreach ($payments as $payment) {
            //echo '<pre>', print_r($payment->toArray()), '</pre>';
            $payment->url = URL::route('order.show', $payment->order->getKey());
        }

        return $this->addFinalResult($payments);
    }

    private function setDateToQuery(&$query, array $input)
    {
        if (isset($input['year']) && $input['year']) {
            $query->whereYear('created_at', '=', $input['year']);
        }

        if (isset($input['month']) && $input['month'] && $input['month'] != 'all') {
            $query->whereMonth('created_at', '=', $input['month']);
        }

        if (isset($input['day']) && $input['day']) {
            $query->whereDay('created_at', '=', $input['day']);
        }
    }

    /**
     *
     * @param Collection $colection
     * @return Collection
     */
    private function addFinalResult(Collection $colection)
    {

        if ($colection->count()) {

            $fin = $this->model->newInstance();

            $fin->id = 0;
            $fin->order_id = 0;
            $fin->summ = 0;

            foreach ($colection as $payment) {
                $fin->summ += $payment->summ;
            }
            $colection->add($fin);
        }

        return $colection;
    }

    /**
     *
     * @param int $id
     * @return Payment
     */
    public function find($id)
    {
        return parent::find($id);
    }

    public function findOrderPayments($id)
    {
        return $this->findAllBy('order_id', $id);
    }

    public function filterInput(array $input, $refilter = false)
    {

        if (isset($this->filtered_input['cashbox']) && !$refilter) {
            return $this->filtered_input['cashbox'];
        }

        $input = array_only($input, $this->getAllowedInputKeys());

        $input = $this->checkUserIdInput($input);

        if (isset($input['day']) && ($input['day'] == $this->getDefaultValueByType('day') || !isset($this->getDayOptionsList()[$input['day']]))) {
            unset($input['day']);
        }

        if (!isset($input['month']) || !isset($this->getMonthOptionsList()[$input['month']])) {
            $input['month'] = $this->getDefaultValueByType('month');
        }

        if (!isset($input['year']) || !isset($this->getYearOptionsList()[$input['year']])) {
            $input['year'] = $this->getDefaultValueByType('year');
        }

        if (!isset($input['cash']) || !isset($this->getCashOptions()[$input['cash']])) {
            $input['cash'] = $this->getDefaultValueByType('cash');
        }

        $this->filtered_input['cashbox'] = $input;

        return $input;
    }

    public function getModelType()
    {
        return '\\Payment';
    }

    public function getInputTypes()
    {
        return [
          'cash' => 'all',
          'day' => 'all',
          'month' => date('n'),
          'year' => date('Y'),
          'user_id' => 'all',
            //'order_by' => 'desc',
        ];
    }

    public function getAllowedInputKeys()
    {
        return array_keys($this->getInputTypes());
    }

    /**
     * Возвращает массив доступных для пользователя и роута опций для отображения
     * @param string $route_name имя роута
     * @return array
     */
    public function getOptions($route_name = 'cashbox.index', $input = [])
    {
        $options['cash'] = $this->getCashOptions();
        $options['day'] = $this->getDayOptionsList();
        $options['month'] = $this->getMonthOptionsList();
        $options['year'] = $this->getYearOptionsList();

        if (!in_array('user_id', $this->getRouteAllowedOptions($route_name))) {
            $options['user_id'] = $this->getMastersSelectableList(
                $this->getMasterRoleId(), ['all' => 'Все мастера']
            );
        }
        //$options['order_by'] = $this->getOrderByOptionsList();

        return $options;
    }

    public function getCashOptions()
    {
        return [
          'all' => 'Нал + безнал',
          'Y' => 'Наличные',
          'N' => 'Безнал',
        ];
    }

    public function allForExport(array $input)
    {
        return $this->getNormalizedCollectionAsArray($this->all($input));
    }

    public function getNormalizedCollectionAsArray(Collection $Entries)
    {
        //var_dump($Entries);
        //die();
        $entries = [];

        foreach ($Entries as $i => $Entry) {

            $entries[$i] = $this->getFieldsDictonary();

            if ($Entry->id) {
                $entries[$i]['order_key'] = $Entry->order->getKey();
                $entries[$i]['created_at'] = $Entry->created_at->toDateTimeString();
                $entries[$i]['master'] = $Entry->order->master->name;
            }
            $entries[$i]['summ'] = $Entry->summ;
        }
        return $entries;
    }

    public function getFieldsDictonary()
    {
        return [
          'order_key' => 'Итого:',
          'summ' => 0,
          'created_at' => '-',
          'master' => '-',
        ];
    }

    public function getFields()
    {
        return array_keys($this->getFieldsDictonary());
    }

    public function getFieldsValues()
    {
        return array_map(function ($value) {
            $value = trans('cashbox.' . $value);
            $value = is_array($value) ? $value['name'] : $value;
            return $value;
        }, $this->getFields());
    }

    public function getCountByOptions($input)
    {
        //echo 'input1: <pre>', print_r($input), '</pre>';
        $input = $this->filterInput($input);
        //echo 'input2: <pre>', print_r($input), '</pre>';

        $query = $this->make();

        $this->setDateToQuery($query, $input);

        //return $query->count();
        return $query->sum('summ');
    }

    ///////////////////////////
    // CashboxManager

    /**
     * Возвращает данные по платежам по заказу
     * @param type $order_key
     * @throws NotFoundException
     * @throws ActionNotAllowedException
     */
    public function getShowData($order_key)
    {
        try {
            /* @var $OR OrderEloquentRepository */
            $OR = App::make("\Mebius\Repository\OrderRepository");
            $order = $OR->findWithPayments($order_key);
            $this->checkIsAllowedAction('cashbox.show', $order);
        } catch (NotFoundException $e) {
            throw $e;
        } catch (ActionNotAllowedException $e) {
            $e->setMessage(trans('order.not_found'));
            throw $e;
        }

        $data = [
          'order' => $order,
          'page_title' => trans('msg.cashbox') . ' / ' . trans('order.page_title') . $order->getKey(),
          'routes' => [
            'order' => [
              'route' => route('order.show', $order->getKey()),
              'name' => trans('order.show'),
            ],
          ],
        ];
        //echo '<pre>', print_r($data), '</pre>';

        return $data;
    }

}
