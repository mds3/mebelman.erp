<?php

namespace Mebius\Repository;

use \Entry;
use \Mebius\Model\Contracts\EntryInterface;
use \Mebius\Repository\LaravelRepositories\Contracts\RepositoryInterface;

interface EntryRepository extends RepositoryInterface
{
    //public function __construct(EntryInterface $model);

    /**
     * Устанавливает время просмотра заявки мастером
     * @param Entry $entry
     */
    public function setMasterSawEntry(Entry $entry);

    /**
     * Возвращает массив мастеров-приемщиков
     * @param int $role_id
     * @return array for form select
     */
    public function getMastersList(array $role_id);

    /**
     * Возвращает массив мастеров-приемщиков для формы
     * (с нулевым элементом -выбрать-
     * @param int $role_id
     * @return array for form select
     */
    public function getMastersSelectableList(array $role_id, array $def);

    /**
     * Возвращает список типов отображения списка заявок
     */
    public function getTypeOptionsList();

    /**
     * Возвращает список периодов отображения списка заявок
     */
    public function getPeriodOptionsList();

    /**
     * Возвращает список сортировок отображения списка заявок
     */
    public function getOrderByOptionsList();

    /**
     * Возвращает список типов поиска
     */
    public function getSearchOptionsList();

    /**
     * Возвращает список вариантов поиска с типами значений
     */
    public function getSearchOptionsListWithTypes();

    /**
     * Возвращает массив доступных опций для выпадающих списков
     * @return array key = default_value
     */
    public function getInputTypes();

    /**
     * Возвращает полный список опций
     * @return array
     */
    public function getOptions();

    /**
     * Возвращает пустой объект заявки
     * @return EntryInterface
     */
    public function getEmptyEntry();

    /**
     *
     * @param array $options
     */
    public function allForExport(array $options);

    /**
     *
     * @param array $input
     */
    public function searchByInputOptions(array $input);
}
