<?php

namespace Mebius\Repository;

use \App;
use \Auth;
use \Mebius\Domain\DataObject;
use \Mebius\Domain\DataRoute;

/**
 * Description of MainEloquentRepository
 *
 * @author topot
 */
class MainRepository extends AbstractTypedEloquentRepository implements MainRepositoryInterface
{

    /**
     *
     * @return DataObject
     */
    public function getIndexData()
    {
        $data = new DataObject();
        $data->setTitle(trans('msg.main'));
        $data->setOptions($this->getOptions());

        if (route_allowed('entry.create')) {
            $data->addRoute(new DataRoute(
                'entry.create', 'GET', route('entry.create'),
                trans('entry.create')
            ));
        }

        return $data;
    }

    public function getOptions($route_name = '', $input = [])
    {
        /*
          Просроченные заказы
         */
        $options = [
          'entries_today' => [
            'name' => trans('entry.entries_today'),
            'icon' => 'check',
            'section' => 'entry',
            'permissions' => [
              'user.create',
            ],
            'options' => [
              'type' => 'all',
              'period' => 'today',
            ],
          ],
          'new_entries' => [
            'name' => trans('entry.new_entries'),
            'icon' => 'eye-close',
            'section' => 'entry',
            'permissions' => [
              'entry.index',
            ],
            'options' => [
              'type' => 'new',
              'period' => 'all',
            ],
          ],
          'call_wait' => [
            'name' => trans('entry.call_wait'),
            'icon' => 'phone-alt',
            'section' => 'entry',
            'permissions' => [
              'entry.status',
              'entry.edit'
            ],
            'options' => [
              'type' => 'call_wait',
              'period' => 'all',
            ],
          ],
          'visit_today' => [
            'name' => trans('entry.visit_today'),
            'icon' => 'road',
            'section' => 'entry',
            'permissions' => [
              'entry.status',
              'entry.edit'
            ],
            'options' => [
              'type' => 'visit_wait',
              'period' => 'today',
            ],
          ],
          'overdue' => [
            'name' => trans('entry.overdue'),
            'icon' => 'fire',
            'section' => 'entry',
            'permissions' => [
              'entry.status',
              'entry.edit'
            ],
            'options' => [
              'type' => 'overdue',
              'period' => 'all',
            ],
          ],
          'deferred_for_today' => [
            'name' => trans('entry.deferred_for_today'),
            'icon' => 'time',
            'section' => 'entry',
            'permissions' => [
              'entry.status',
              'entry.edit'
            ],
            'options' => [
              'type' => 'deferred',
              'period' => 'today',
            ],
          ],
          'overdue_deferred' => [
            'name' => trans('entry.overdue_deferred'),
            'icon' => 'minus-sign',
            'section' => 'entry',
            'permissions' => [
              'entry.status',
              'entry.edit'
            ],
            'options' => [
              'type' => 'overdue_deferred',
              'period' => 'all',
            ],
          ],
          'orders_today' => [
            'name' => trans('order.orders_today'),
            'icon' => 'check',
            'section' => 'order',
            'permissions' => [
              'order.close',
            ],
            'options' => [
              'type' => 'all',
              'period' => 'today',
            ],
          ],
          'finish_today' => [
            'name' => trans('order.finish_today'),
            'icon' => 'ok',
            'section' => 'order',
            'permissions' => [
              'order.status',
              'order.edit'
            ],
            'options' => [
              'type' => 'finish',
              'period' => 'today',
            ],
          ],
          'finish_overdue' => [
            'name' => trans('order.finish_overdue'),
            'icon' => 'fire',
            'section' => 'order',
            'permissions' => [
              'order.show',
            ],
            'options' => [
              'type' => 'overdue',
              'period' => 'all',
            ],
          ],
          'cashbox_today' => [
            'name' => trans('cashbox.today'),
            'icon' => 'usd',
            'section' => 'cashbox',
            'permissions' => [
              'order.cashbox',
            ],
            'options' => [
              'day' => date('j'),
              'month' => date('n'),
              'year' => date('Y'),
              'user_id' => 'all',
            ],
            'str' => trans('msg.cur'),
          ],
        ];


        /* @var $model['entry'] EntryEloquentRepository */
        $model['entry'] = App::make("\Mebius\Repository\EntryRepository");

        /* @var $model['order'] OrderEloquentRepository */
        $model['order'] = App::make("\Mebius\Repository\OrderRepository");

        /* @var $model['order'] CashboxEloquentRepository */
        $model['cashbox'] = App::make("\Mebius\Repository\CashboxRepository");

        foreach ($options as $type => $option) {

            $allowed = [];
            foreach ($option['permissions'] as $route) {
                $perms = $model[$option['section']]->getRouteAllowedOptions($route);
                if (is_array($perms)) {
                    $allowed += $perms;
                }
            }

            //$allowed = $model[$option['section']]->getRouteAllowedOptions($route);


            $route = $option['section'] . '.index';

            if (is_array($allowed) && $allowed) {
                if (in_array('user_id', $allowed)) {
                    $option['options']['user_id'] = Auth::id();
                }
                if (in_array('created_by', $allowed)) {
                    $option['options']['created_by'] = Auth::id();
                }

                $options[$type]['count'] = $model[$option['section']]->getCountByOptions($option['options']);
                $options[$type]['route'] = route($route, $option['options']);
            } else {
                unset($options[$type]);
            }
        }

        return $options;
    }

    public function getModelType()
    {
        return null;
    }

    public function getInputTypes()
    {
        return [];
    }

}
