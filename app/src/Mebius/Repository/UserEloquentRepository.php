<?php

namespace Mebius\Repository;

use \Eloquent;
use \Mebius\Repository\LaravelRepositories\EloquentRepository;
use \User;

class UserEloquentRepository extends EloquentRepository implements UserRepository
{

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function all(array $options = [], array $columns = ['*'])
    {
        return $this->model->with([
              'roles' => function($query) {
                  $query->select('id', 'name');
              }
            ])->get();
    }

    /**
     *
     * @param int $id
     * @return User
     */
    public function find($id)
    {
        return $this->model->with([
              'roles' => function($query) {
                  $query->select('id', 'name');
              }
            ])->find($id);
    }

    /* public function create(array $input)
      {

      $model = new User($input);

      if (!empty($input['password'])) {
      $model->password = $input['password'];
      }

      if ($model->create($input)) {
      return $model->id;
      }
      $this->setErrors($model->getErrors());
      echo __METHOD__ . '<pre>', print_r($model->getErrors()), '</pre>';
      return false;
      } */

    public function update(Eloquent $model, array $input)
    {
        //echo '<pre>', print_r($input), '</pre>';
        if (isset($input['password']) && empty($input['password'])) {
            unset($input['password']);
            $model->setAttribute('password', '');
            $model->syncOriginalAttribute('password');
        }
        //echo '<pre>', print_r($input), '</pre>';
        //die();
        $ch = $model->roles()->sync(isset($input['role_id']) ? $input['role_id']
                    : []);
        //echo '<pre>', print_r($ch), '</pre>';
        //die();
        $dirty = parent::update($model, $input);
        if ($dirty === false) {
            return false;
        }
        if (count($ch['attached']) || count($ch['detached'])) {
            $dirty['role_id'] = true;
        }
        return $dirty;
    }

    public function destroy($id)
    {
        /* @var $user User|Builder */
        $user = $this->find($id);
        if ($user) {
            $user->roles()->detach();
            return $user->delete();
        }
        return false;
        //parent::destroy($mixed);
    }

    public function getModelType()
    {
        return '\\User';
    }

    /**
     *
     * @param array $role_ids
     * @return type
     */
    public function getRoleUsersList(array $role_ids)
    {
        $ret = [];
        $Roles = \Role::find($role_ids);
        foreach ($Roles as $role) {
            foreach ($role->users as $user) {
                $ret[$user->id] = $user->name;
                //echo '<pre>', print_r($user->toArray()), '</pre>';
            }
        }
        //echo '<pre>', print_r($ret), '</pre>';
        //die();
        return $ret;
    }

}
