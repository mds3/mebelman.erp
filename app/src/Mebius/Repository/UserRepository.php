<?php

namespace Mebius\Repository;

use \Mebius\Repository\LaravelRepositories\Contracts\RepositoryInterface;

interface UserRepository extends RepositoryInterface
{

    /**
     *
     * @param array $role_ids
     * @return array [][id] => name
     */
    public function getRoleUsersList(array $role_ids);
}
