<?php

namespace Mebius\Repository;

use \App;
use \Auth;
use \Entry;
use \Event;
use \Illuminate\Database\Eloquent\Builder;
use \Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Repository\Exceptions\NotFoundException;
use \Role;
use \URL;

class EntryEloquentRepository extends AbstractTypedEloquentRepository implements EntryRepository
{

    use Traits\IconedTrait;

    /**
     *
     */
    const ENTITY_INDEX = 'entry.index';
    const ENTITY_SHOW = 'entry.show';
    const ENTITY_CREATE = 'entry.create';
    const ENTITY_STORE = 'entry.store';
    const ENTITY_EDIT = 'entry.edit';
    const ENTITY_UPDATE = 'entry.update';
    const ENTITY_EXPORT = 'entry.export';
    const ENTITY_SEARCH = 'entry.search';
    const ENTITY_SHOWLOGS = 'entry.showlogs';

    public function __construct(Entry $entry)
    {
        $this->model = $entry;
    }

    public function getPeriodOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getPeriodOptions());
    }

    public function getTypeOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->getPermittedOptions($this->model->getTypeOptions()));
    }

    public function getOrderByOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getOrderByOptions());
    }

    public function getInputTypes()
    {
        return [
          'type' => 'all',
          'period' => 'all',
          'user_id' => 'all',
          'order_by' => 'desc',
        ];
    }

    /**
     * Возвращает массив доступных для пользователя и роута опций для отображения
     * @param string $route_name имя роута
     * @return array
     */
    public function getOptions($route_name = 'entry.index', $input = [])
    {
        $options['type'] = $this->getTypeOptionsList();
        $options['period'] = $this->getPeriodOptionsList();
        $options['order_by'] = $this->getOrderByOptionsList();

        if (!in_array('user_id', $this->getRouteAllowedOptions($route_name))) {
            $options['user_id'] = $this->getMastersSelectableList(
                $this->getMasterRoleId(), ['all' => trans('msg.masters')]
            );
        }

        return $options;
    }

    public function getRouteAllowedOptions($route_name = 'entry.index')
    {
        return get_allowed_options($route_name);
    }

    public function getSupplierRoleId()
    {
        return Role::SUPPLIER_ROLE_ID;
    }

    public function getTypeCells($input)
    {
        $type = isset($input['type']) ? $input['type'] : 'all';
        $arr = $this->model->getTypeCells($type);
        if (!isset($input['user_id']) || $input['user_id'] == 'all') {
            $arr += ['master.name' => ''];
        }
        return $arr;
    }

    protected function getWhereByOptions(array $input = [], array $options = [])
    {
        $where = [];

        //echo '<pre>', print_r($options), '</pre>';

        if (isset($input['user_id']) && $input['user_id'] !== 'all') {
            $where['users']['where'][] = ['user_id', '=', (int) $input['user_id'], 'or'];
        }

        if (isset($input['created_by']) && $input['created_by'] !== 'all') {
            $where['users']['where'][] = ['created_by', '=', (int) $input['created_by'], 'or'];
        }

        $types = $this->model->getTypeOptions();
        //echo '<pre>', print_r($types), '</pre>';

        /* foreach ($types as $key => $opts) {
          foreach ($opts['permissions'] as $route) {
          if (!route_allowed($route)) {
          unset($types[$key]);
          }
          }
          } */
        //echo '<pre>', print_r(array_keys($types)), '</pre>';

        $input['type'] = isset($input['type']) ? $input['type'] : 'all';

        $periods = $this->model->getPeriodOptions($input['type']);

        $orders = $this->model->getOrderByOptions();

        $input['period'] = isset($input['period']) ? $input['period'] : 'all';

        $input['order_by'] = (isset($input['order_by']) && isset($orders[$input['order_by']]))
                ? $input['order_by'] : 'desc';

        //echo '<pre>', print_r($input), '</pre>';

        if (isset($input['type']) && isset($types[$input['type']])) {
            foreach ($types[$input['type']]['conditions'] as $i => $cond) {
                $where[] = [$i => $cond];
            }
        }

        if (isset($input['period']) && isset($periods[$input['period']])) {
            foreach ($periods[$input['period']]['conditions'] as $i => $cond) {
                $where[] = [$i => $cond];
            }
        }

        if (isset($input['order_by']) && isset($orders[$input['order_by']])) {
            foreach ($orders[$input['order_by']]['conditions'] as $i => $cond) {
                $where[] = [$i => $cond];
            }
        }

        //echo '<pre>', print_r($where), '</pre>';
        //die();

        return $where;
    }

    /**
     *
     * @param array $whereArr
     * @return Builder
     */
    protected function getQueryByWhere(array $whereArr = [])
    {
        $query = $this->make();

        if (!empty($whereArr)) {
            if (isset($whereArr['users'])) {
                $query->where(function ($query) use($whereArr) {
                    $this->setWhereToQuery($query, $whereArr['users']);
                });
                unset($whereArr['users']);
            }
            foreach ($whereArr as $where) {
                $this->setWhereToQuery($query, $where);
            }
        }
        return $query;
    }

    protected function setWhereToQuery($query, $where)
    {
        if (empty($where)) {
            return;
        }

        foreach ($where as $func => $_where) {
            foreach ($_where as $_w) {
                switch (count($_w)) {
                    case 2:
                        $query->$func($_w[0], $_w[1]);
                        break;
                    case 3:
                        $query->$func($_w[0], $_w[1], $_w[2]);
                        break;
                    default:
                        $query->$func($_w[0], $_w[1], $_w[2], $_w[3]);
                        break;
                }
            }
        }
    }

    public function getMastersList(array $role_id)
    {
        /* @var $repo UserRepository */
        $repo = App::make('\Mebius\Repository\UserRepository');
        return $repo->getRoleUsersList($role_id);
    }

    public function getMastersSelectableList(array $role_id, array $def = [])
    {
        return $this->makeList($this->getMastersList($role_id), $def);
    }

    public function getSuppliersSelectableList(array $def = [])
    {
        return $this->makeList($this->getMastersList([$this->getSupplierRoleId()]),
                $def, false);
    }

    public function makeList($list, array $def = [], $with0 = true)
    {
        if ($with0) {
            $list[0] = trans('msg.not_assigned');
        }
        ksort($list);
        if (!empty($def)) {
            $list[key($def)] = $def[key($def)];
        }
        //echo '<pre>', print_r($list), '</pre>';
        return $list;
    }

    public function getModelType()
    {
        return '\\Entry';
    }

    public function setMasterSawEntry(Entry $entry)
    {
        //echo '<pre>', print_r($this->model->update()), '</pre>';
        //die("updating entry id $entry->id");
        $entry->view_time = date($entry->getDateTimeFormat());
        //die('view_time: ' . $entry->view_time);

        $entry->unsetMasterDates();

        //echo '<pre>', print_r($entry->toArray()), '</pre>';

        if (!$entry->save()) {
            //echo '<pre>errors: ', print_r($entry->getErrors()), '</pre>';
            $this->setErrors($entry->getErrors());
            return false;
        }
        //die('view_time: ' . $entry->view_time);
        return true;
    }

    public function getEmptyEntry()
    {
        //$entry = new Entry;
        //$entry->master = App::build('User');

        $entry = $this->model->newInstance([]);
        $entry->master = App::build('User');
        $entry->refer_id = 1;
        $entry->supplier_id = 2;
        //echo '<pre>', print_r($entry->toArray()), '</pre>';
        //die();
        return $entry;
    }

    public function all(array $input = [], array $columns = ['*'])
    {
        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
          'operator' => function($query) {
              $query->select('id', 'name');
          },
            /* 'updater' => function($query) {
              $query->select('id', 'name');
              }, */
        ]);

        $where = $this->getWhereByOptions($input);

        $query = $this->getQueryByWhere($where);

        return $query->paginate(static::LIMIT_PER_PAGE, $columns);
        //return $query->get($columns);
    }

    /**
     *
     * @param int $id
     * @return Entry
     * @throws NotFoundException
     */
    public function find($id)
    {
        $entry = $this->model->with([
              'master' => function($query) {
                  $query->select('id', 'name', 'phone', 'email');
              },
              'supplier' => function($query) {
                  $query->select('id', 'name');
              },
              'operator' => function($query) {
                  $query->select('id', 'name');
              },
              'refer' => function($query) {
                  $query->select('id', 'name');
              }
            ])->find($id);

        if (!$entry) {
            throw new NotFoundException(trans('entry.not_found'));
        }
        return $entry;
    }

    public function create(array $input)
    {
        if (isset($input['user_id']) && ((int) $input['user_id']) == 0) {
            unset($input['user_id']);
        }
        return parent::create($input);
    }

    public function update(Model $model, array $input)
    {
        if (isset($input['user_id']) && ((int) $input['user_id']) == 0) {
            unset($input['user_id']);
            //unset($model->user_id);
        }
        if (isset($input['visit_time']) && strtotime($input['visit_time']) <= 0) {
            unset($input['visit_time']);
        }
        if (isset($input['call_time']) && strtotime($input['call_time']) <= 0) {
            unset($input['call_time']);
        }

        //echo '<pre>', print_r($input), '</pre>';
        //echo '<pre>', print_r($model->toArray()), '</pre>';
        //die();

        return parent::update($model, $input);
    }

    public function allForExport(array $input = [])
    {
        $columns = $this->model->getShortStatement();

        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
          'operator' => function($query) {
              $query->select('id', 'name');
          },
          'refer' => function($query) {
              $query->select('id', 'name');
          },
        ]);

        $where = $this->getWhereByOptions($input);

        $query = $this->getQueryByWhere($where);

        $entries = $query->get($columns);

        return $this->getNormalizedCollectionAsArray($entries);
    }

    public function getNormalizedCollectionAsArray(Collection $Entries)
    {
        $entries = [];

        foreach ($Entries as $i => $Entry) {

            $entry = $Entry->toArray();

            $entries[$i] = $entry;

            $entries[$i]['master'] = $entry['master'] = is_array($entries[$i]['master'])
                    ? $entries[$i]['master']['name'] : trans('msg.not_assigned');
            //echo '<pre>', print_r($entries[$i]['master']), '</pre>';
            foreach ($entry as $k => $ent) {
                $ent = is_array($ent) ? $ent['name'] : $ent;
                $ent = ($ent == $Entry->getDefaultDateTimeString() || $ent == $Entry->getDefaultDateString())
                        ? '-' : $ent;
                $entries[$i][$k] = $ent;
            }
            $entries[$i]['status'] = trans('status.' . strtolower($Entry->condition));
        }
        return $entries;
    }

    public function getCountByOptions($options)
    {
        $where = $this->getWhereByOptions($options);

        $query = $this->getQueryByWhere($where);

        return $query->count();
    }

    public function getSearchOptionsList()
    {
        return array_map(function($item) {
            return $item['name'];
        }, $this->model->getSearchOptions());
    }

    public function getSearchOptionsListWithTypes()
    {
        return array_map(function($item) {
            return [
              'name' => $item['name'],
              'type' => $item['type'],
            ];
        }, $this->model->getSearchOptions());
    }

    private function getSearchWhere(array $input = [])
    {
        $options = $this->model->getSearchOptions($input);
        $where = [];
        foreach ($input as $key => $val) {
            if (isset($options[$key])) {
                foreach ($options[$key]['conditions'] as $i => $cond) {
                    $where[] = [$i => $cond];
                }
            }
        }
        return $where;
    }

    public function searchByInputOptions(array $input)
    {

        $arr[$input['search_by']] = $input['search_val'];

        $where = $this->getSearchWhere($arr);

        //echo '<pre>', print_r($where), '</pre>';
        //die();

        $this->with([
          'master' => function($query) {
              $query->select('id', 'name');
          },
            /* 'creator' => function($query) {
              $query->select('id', 'name');
              },
              'updater' => function($query) {
              $query->select('id', 'name');
              }, */
        ]);

        //list($input, $options) = $this->getFilteredInputOptions($input);
        //$where = $this->getWhereByOptions($arr, $options);
        //echo '<pre>', print_r($where), '</pre>';

        $query = $this->getQueryByWhere($where);

        return $query->get();
    }

    //////////////////////////////////////////
    // далее EntryManager

    /**
     * Массив данных для отображения списка заявок
     * @param array $input
     * @return array - data-array
     */
    public function getIndexData(array $input)
    {
        $input = $this->getAllowedOptionsInput(self::ENTITY_INDEX);
        //echo '<pre>AllowedOptionsInput', print_r($input), '</pre>';

        $options = $this->getOptions(self::ENTITY_INDEX);
        //echo '<pre>getOptions', print_r($options), '</pre>';

        $entries = $this->all($input);

        foreach ($entries as $entry) {
            $entry->url = URL::route(self::ENTITY_SHOW, $entry->id);
        }

        $data = [
          'entries' => $entries,
          'input' => $input,
          'options' => $options,
          'page_title' => trans('entry.entries'),
          'cells' => $this->getTypeCells($input),
          'icons' => $this->getIconsDict(),
        ];

        if (route_allowed(self::ENTITY_CREATE)) {
            $data['routes'][self::ENTITY_CREATE] = [
              'route' => URL::route(self::ENTITY_CREATE),
              'name' => trans('entry.create'),
            ];
        }

        if (route_allowed(self::ENTITY_SEARCH)) {
            $data['routes']['search'] = [
              'route' => URL::route(self::ENTITY_SEARCH),
              'name' => trans('entry.search'),
            ];
        }

        if (route_allowed(self::ENTITY_EXPORT) && $entries->count()) {
            $data['routes']['export'] = [
              'route' => URL::route(self::ENTITY_EXPORT, $input),
              'name' => trans('entry.export'),
            ];
        }

        return $data;
    }

    /**
     * Массив данных для карточки заявки
     * @param int $id
     * @return array
     * @throws NotFoundException
     * @throws ActionNotAllowedException
     */
    public function getShowData($id)
    {
        try {
            $entry = $this->find($id);
            $this->checkIsAllowedAction(self::ENTITY_SHOW, $entry);
        } catch (NotFoundException $e) {
            throw $e;
        } catch (ActionNotAllowedException $e) {
            $e->setMessage(trans('entry.not_found'));
            throw $e;
        }

        Event::fire(self::ENTITY_SHOW, [$entry, $this]);

        $data = [
          'route' => URL::route('entry.status', $entry->getKey()),
          'method' => 'PUT',
          'page_title' => trans('entry.page_title') . $entry->order_key,
          'routes' => [],
        ];

        if (action_allowed(self::ENTITY_SHOWLOGS, $entry)) {
            $data['routes']['showlogs'] = [
              'method' => 'GET',
              'route' => URL::route(self::ENTITY_SHOWLOGS, $entry->getKey()),
              'name' => trans('msg.showlogs'),
              'options' => $entry->getKey(),
            ];
        }

        if (!$entry->is_archived) {

            if (action_allowed('entry.status', $entry) && $entry->is_refused) {
                $data['routes']['reanimate'] = [
                  'route' => URL::route('entry.status', $entry->getKey()),
                  'method' => 'PUT',
                  'name' => trans('entry.reanimate'),
                  'hidden' => [
                    '_status' => 'reanimate',
                  ],
                ];
            }

            if (!$entry->is_refused) {

                if (action_allowed(self::ENTITY_EDIT, $entry)) {
                    $data['routes']['edit'] = [
                      'method' => 'GET',
                      'route' => URL::route(self::ENTITY_EDIT, $entry->getKey()),
                      'name' => trans('entry.edit'),
                    ];
                }

                if ($entry->is_called && action_allowed('order.create', $entry)) {
                    $data['routes']['create'] = [

                      'method' => 'GET',
                      'route' => route('order.create',
                          ['id' => $entry->getKey()]),
                      'name' => trans('order.create'),
                    ];
                }

                //die('action_allowed: ' . (action_allowed('entry.status', $entry)));


                if ((action_allowed('entry.status', $entry) && (!$entry->visit_time_isset || $entry->is_deferred)) || route_allowed('entry.export')) {
                    $data['routes']['visit'] = [
                      'name' => trans('entry.visit_time'),
                    ];
                }

                if (!$entry->is_refused && action_allowed('entry.status', $entry)) {
                    $data['routes']['defer'] = [
                      'name' => trans('entry.do_defer'),
                    ];
                    $data['routes']['refuse'] = [
                      'name' => trans('entry.do_refuse'),
                    ];
                }
            } else {

                if (action_allowed('entry.archiving', $entry)) {
                    $data['routes']['archiving'] = [
                      'method' => 'PUT',
                      'route' => URL::route('entry.status', $entry->getKey()),
                      'name' => trans('entry.do_archived'),
                      'hidden' => [
                        '_status' => 'archiving',
                      ],
                    ];
                }
            }
        } else {

            if ($entry->order_created) {
                $data['routes']['order_created'] = [
                  'method' => 'GET',
                  'route' => route('order.show', $entry->order_key),
                  'name' => trans('entry.order_created') . ' ' . $entry->order_key,
                ];
            }
        }

        //echo '<pre>', print_r($data['routes']), '</pre>';

        $data['entry'] = $entry;

        return $data;
    }

    /**
     *
     * @return array
     */
    public function getCreateData()
    {
        $masters = $this->getMastersSelectableList($this->getMasterRoleId());
        $suppliers = $this->getSuppliersSelectableList();
        $refers = $this->getRefersList();
        $entry = $this->getEmptyEntry();
        $routes = [
          'return' => [
            'route' => route(self::ENTITY_SHOW, $entry->getKey()),
            'name' => trans('msg.cancel'),
          ]
        ];
        return [
          'entry' => $entry,
          'masters' => $masters,
          'suppliers' => $suppliers,
          'refers' => $refers,
          'route' => route(self::ENTITY_STORE),
          'method' => 'POST',
          'routes' => $routes,
          'page_title' => trans('entry.adding'),
        ];
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getStoreData(array $input)
    {
        return [];
    }

    /**
     *
     * @param int $id
     * @return array
     */
    public function getEditData($id)
    {
        return [];
    }

    /**
     *
     * @param array $input
     * @return array
     */
    public function getUpdateData($id, array $input)
    {
        return [];
    }

    public function getExportData($id)
    {
        return [];
    }

    public function getSearchData()
    {
        return [];
    }

    public function getShowlogsData($id)
    {
        return [];
    }

    public function getStatusData($id)
    {
        return [];
    }

}
