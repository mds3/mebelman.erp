<?php

namespace Mebius\Repository\LaravelRepositories;

use \App;
use \Eloquent;
use \Illuminate\Database\Eloquent\Builder;
use \Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;
use \Mebius\Repository\LaravelRepositories\Contracts\PaginatedInterface;
use \Mebius\Repository\LaravelRepositories\Contracts\RepositoryInterface;
use \Mebius\Validator\AbstractValidator;
use \Refer;

abstract class EloquentRepository implements RepositoryInterface
{

    const LIMIT_PER_PAGE = 20;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var AbstractValidator
     */
    protected $validator;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * with eager loadings
     *
     * @var array
     */
    protected $with = array();

    /**
     * returns a collection of all models
     *
     * @return Collection
     */
    public function all(array $options = [], array $columns = ['*'])
    {
        return $this->model->all($columns);
    }

    /**
     * returns the model found
     *
     * @param int $id
     * @return Model
     */
    public function find($id)
    {
        $query = $this->make();

        return $query->find($id);
    }

    /**
     * returns the repository itself, for fluent interface
     *
     * @param array $with
     * @return self
     */
    public function with(array $with)
    {
        $this->with = array_merge($this->with, $with);

        return $this;
    }

    /**
     * returns the first model found by conditions
     *
     * @param string $key
     * @param mixed $value
     * @param string $operator
     * @return Model
     */
    public function findFirstBy($key, $value, $operator = '=')
    {
        $query = $this->make();

        return $query->where($key, $operator, $value)->first();
    }

    /**
     * returns the first model found by conditions
     *
     * @param array $params [][field, operator, value]
     * @return Model
     */
    public function findOneBy($params)
    {
        $query = $this->make();

        foreach ($params as $param) {
            $query->where($param[0], $param[1], $param[2]);
        }

        return $query->first();
    }

    /**
     * returns all models found by conditions
     *
     * @param string $key
     * @param mixed $value
     * @param string $operator
     * @return Collection
     */
    public function findAllBy($key, $value, $operator = '=')
    {
        $query = $this->make();

        return $query->where($key, $operator, $value)->get();
    }

    /**
     * returns all models that have a required relation
     *
     * @param string $relation
     * @return Collection
     */
    public function has($relation)
    {
        $query = $this->make();

        return $query->has($relation)->get();
    }

    /**
     * returns paginated result
     *
     * @param int $page
     * @param int $limit
     * @return PaginatedInterface
     */
    public function getPaginated($page = 1, $limit = 10)
    {
        $query = $this->make();
        $collection = $query->forPage($page, $limit)->get();

        return new PaginatedResult($page, $limit, $collection->count(),
            $collection);
    }

    /**
     * returns the query builder with eager loading, or the model itself
     *
     * @return Builder|Model
     */
    protected function make()
    {
        return $this->model->with($this->with);
    }

    /**
     *
     * @param array $input
     * @return int|boolean created id or false if not created
     */
    public function create(array $input)
    {

        $modelType = $this->getModelType();
        /* @var $model Eloquent */
        $model = new $modelType($input);
        /* @var $saved Eloquent */
        $saved = $model->create($input);

        if ($saved->exists) {
            //$saved->load($saved->getRelations());
            return $saved;
        }
        $this->setErrors($saved->getErrors());
        //echo __METHOD__ . '<pre>', print_r($saved->getErrors()), '</pre>';
        return false;
    }

    /**
     *
     * @param Model $model
     * @param array $input
     * @return boolean false|integer count updated fields
     */
    public function update(Model $model, array $input = [])
    {
        $model->fill($input);
        //return $model->update();
        //echo 'dirty:<pre>', print_r($model->getDirty()), '</pre>';
        //die();
        $dirty = $model->getDirty();
        if ($model->update()) {
            return $dirty;
        }
        $this->setErrors($model->getErrors());
        return false;
    }

    protected function save(Model $model)
    {
        if ($model->save()) {
            return $model->id;
        }
        $this->setErrors($model->getErrors());
        return false;
    }

    public function delete()
    {
        return $this->model->delete();
    }

    public function destroy($mixed = null)
    {
        return $this->model->destroy($mixed);
    }

    protected function addError($error)
    {
        $this->errors[] = $error;
    }

    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     *
     * @param array $input
     * @return Model
     */
    public function createObject(array $input = [])
    {
        $obj = App::build($this->getModelType());
        $obj->fill($input);
        return $obj;
    }

    /**
     *
     * @param array $items
     * @return Collection
     */
    public function getCollection(array $items = [])
    {
        return $this->model->newCollection($items);
    }

    protected function getRefersList()
    {
        $R = new Refer;
        return $R->getList();
    }

}
