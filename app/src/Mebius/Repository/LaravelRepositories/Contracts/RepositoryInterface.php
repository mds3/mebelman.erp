<?php

namespace Mebius\Repository\LaravelRepositories\Contracts;

use \Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{

    /**
     * returns a collection of all models
     *
     * @param array $options
     * @param array $columns
     * @return Collection
     */
    public function all(array $options = null, array $columns = null);

    /**
     * returns the model found
     *
     * @param int $id
     * @return Model
     */
    public function find($id);

    /**
     * returns the repository itself, for fluent interface
     *
     * @param array $with
     * @return self
     */
    public function with(array $with);

    /**
     * returns the first model found by conditions
     *
     * @param string $key
     * @param mixed $value
     * @param string $operator
     * @return Model
     */
    public function findFirstBy($key, $value, $operator = '=');

    /**
     * returns all models found by conditions
     *
     * @param string $key
     * @param mixed $value
     * @param string $operator
     * @return Collection
     */
    public function findAllBy($key, $value, $operator = '=');

    /**
     * returns all models that have a required relation
     *
     * @param string $relation
     * @return Collection
     */
    public function has($relation);

    /**
     * @param int $page
     * @param int $limit
     * @return PaginatedInterface
     */
    public function getPaginated($page = 1, $limit = 10);

    public function create(array $input);

    public function update(Model $model, array $input);

    public function delete();

    public function destroy($mixed);

    public function getModelType();

    public function createObject(array $input);

    public function getErrors();

    public function getCollection(array $items);
}
