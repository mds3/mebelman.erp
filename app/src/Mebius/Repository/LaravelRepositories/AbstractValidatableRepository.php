<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Repository\LaravelRepositories;

/**
 * Description of AbstractValidatableRepository
 *
 * @author 1
 */
abstract class AbstractValidatableRepository
{

    protected $errors = [];

    /**
     *
     */
    abstract static function getRules();

    abstract static function getRulesMessages();

    public function isValid(array $data)
    {
        $validation = \Validator::make($data, static::getRules(),
                static::getRulesMessages()? : []);

        if ($validation->fails()) {
            $this->errors = $validation->errors();
            return false;
        }
        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

}
