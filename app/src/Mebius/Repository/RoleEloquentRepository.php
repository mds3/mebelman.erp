<?php

namespace Mebius\Repository;

use \Mebius\Repository\LaravelRepositories\EloquentRepository;
use \Role;

class RoleEloquentRepository extends EloquentRepository implements RoleRepository
{

    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function all(array $options = [], array $columns = ['*'])
    {
        return $this->model->with('permissions')->get($columns);
    }

    /**
     *
     * @param int $id
     * @return \Role
     */
    public function find($id)
    {
        return parent::find($id);
    }

    /* public function find($id)
      {
      return $this->model->with('permissions')->find($id);
      } */

    public function getModelType()
    {
        return '\\Role';
    }

}
