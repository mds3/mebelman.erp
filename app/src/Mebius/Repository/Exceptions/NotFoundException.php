<?php

/*
 * topot
 */

namespace Mebius\Repository\Exceptions;

use \Exception;

/**
 * NotFoundException
 *
 * @author topot
 */
class NotFoundException extends Exception
{
    //
}
