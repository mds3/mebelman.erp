<?php

namespace Mebius\Controller;

use \Mebius\Repository\Contracts\HelpRepositoryInterface;
use \View;

class HelpController extends BaseController
{

    /**
     *
     * @var HelpRepositoryInterface
     */
    protected $repo;

    public function __construct(HelpRepositoryInterface $repo)
    {
        //parent::__construct($repo);
        $this->repo = $repo;
    }

    public function getIndex()
    {
        $sections = $this->repo->all();
        //echo '<pre>', print_r($help->toArray()), '</pre>';
        //die();

        $data = [
          'sections' => $sections,
          'page_title' => 'Справка',
        ];

        return View::make('help.index', $data);
    }

}
