<?php

namespace Mebius\Controller;

use \Input;
use \Mebius\Repository\RoleRepository;
use \Permission;
use \Redirect;
use \Response;
use \Role;
use \View;

class RoleController extends BaseController
{

    /**
     *
     * @var RoleRepository
     */
    protected $repo;

    public function __construct(RoleRepository $repo)
    {
        //parent::__construct($repo);
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = $this->repo->all();

        //echo '<pre>', print_r($roles->toArray()), '</pre>';

        return View::make('role/index',
                [
              'roles' => $roles,
              'page_title' => trans('role.roles'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $permissions = Permission::all();

        return View::make('role/form',
                [
              'role' => new Role(),
              'permissions' => $permissions,
              'route' => 'role.store',
              'page_title' => trans('role.creation'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $role = $this->repo->create(Input::all());

        if ($role) {

            $role->permissions()->sync(Input::get("permission_id", []));

            return Redirect::route('role.show', $role->getKey())->with('success',
                    trans('role.created'));
        } else {

            return Redirect::route('role.create')
                    ->withErrors($this->repo->getErrors())
                    ->withInput(Input::all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $role = $this->repo->find($id);

        if (!$role) {

            return Redirect::route('role.index')->with('danger',
                    trans('role.not_found'));
        } else {

            return View::make('role/show',
                    [
                  'role' => $role,
                  'page_title' => trans('role.role') . ' ' . $role->name,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->repo->find($id);

        if (!$role) {

            return Redirect::route('role.index')->with('danger',
                    trans('role.not_found'));
        } else {

            $permissions = Permission::all();

            return View::make('role/form',
                    [
                  'role' => $role,
                  'permissions' => $permissions,
                  'route' => ['role.update', $role->id],
                  'method' => 'PUT',
                  'page_title' => trans('role.edition')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        /* @var $role \Role */
        $role = $this->repo->find($id);

        if (!$role) {

            return Redirect::route('role.index')->with('danger',
                    trans('role.not_found'));

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            $dirty = $this->repo->update($role, Input::all());

            //die();

            if ($dirty !== false) {

                $ret = Redirect::route('role.show', array('id' => $id));

                $role->permissions()->sync(Input::get("permission_id", []));

                return $ret->with('success',
                        trans('role.updated') . ': ' . implode(', ',
                            array_keys($dirty)) . '');
            } else {

                return Redirect::route('role.edit', $id)->withErrors($this->repo->getErrors())->withInput(Input::all());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
