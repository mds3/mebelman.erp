<?php

namespace Mebius\Controller;

use \Controller;
use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Http\RedirectResponse;
use \Redirect;
use \Refer;
use \View;

class BaseController extends Controller
{

    protected $layout = 'layouts.master';

    /**
     * Код ошибки
     */
    const ERRROR = 'danger';

    /**
     * Код успеха
     */
    const OK = 'success';

    /**
     * Код инфо
     */
    const INFO = 'info';

    /**
     * @var array
     */
    protected $data = [];

    public function addData($data)
    {
        if (is_array($data)) {
            $this->data = array_merge($this->data, $data);
        } else {
            $this->data[] = $data;
        }
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    protected function addTitle($title)
    {
        $this->layout->title[] = $title;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function setContent($content)
    {
        $this->layout->content = $content;
    }

    /**
     *
     * @param string $route_name проверяемый роут
     * @param Model $model проверяемая модель
     * @param string $return_route куда направлять в случае запрета
     * @return RedirectResponse|boolean
     */
    protected function checkIsAllowedAction($route_name, Model $model,
        $return_route = 'main')
    {
        if (!action_allowed($route_name, $model)) {
            //die('У вас нет прав на выполнение этого действия..');
            Redirect::route($return_route)->with('danger',
                'У вас нет прав на выполнение этого действия..')->send();
            return;
        }
        return true;
    }

    protected function getRefersList()
    {
        $R = new Refer;
        return $R->getList();
    }

}
