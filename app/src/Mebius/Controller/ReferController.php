<?php

namespace Mebius\Controller;

use \Input;
use \Redirect;
use \Refer;
use \Response;
use \View;

class ReferController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $refers = Refer::all();

        //echo '<pre>', print_r($refers->toArray()), '</pre>';

        return View::make('refer/index',
                [
              'refers' => $refers,
              'page_title' => 'Источники'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('refer/form',
                [
              'refer' => new Refer(),
              'route' => 'refer.store',
              'page_title' => 'Добавление нового источника'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $refer = Refer::create(Input::all());

        //die();

        if ($refer->exists) {

            return Redirect::route('refer.show', array('id' => $refer->id))->with('success',
                    'Новый источник создан.');

            return Response::json(array(
                  'error' => false,
                  'refer' => $refer->toArray()), 201
            );
            //$entry = Entry::with('metrostation')->find($id);
        } else {

            //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
            //return;

            return Redirect::route('refer.create')->withErrors($this->repo->getErrors())->withInput(Input::all());

            return Response::json(array(
                  'error' => true,
                  'messages' => $this->repo->getErrors()), 400
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $refer = Refer::with(['entries' => function($query) {
                  $query->select('id', 'order_key', 'refer_id');
              }])->find($id);

        if (!$refer) {

            return Redirect::route('refer.index')->with('danger',
                    'Такой источник не найден.');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            //echo '<pre>', print_r($refer->toArray()), '</pre>';

            return View::make('refer/show',
                    [
                  'refer' => $refer,
                  'page_title' => 'Источник: ' . $refer->name,
            ]);

            return Response::json(array(
                  'error' => false,
                  'user' => $refer->toArray()), 200
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $refer = Refer::find($id);

        if (!$refer) {

            return Redirect::route('refer.index')->with('danger',
                    'Такой источник не найден.');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            return View::make('refer/form',
                    [
                  'refer' => $refer,
                  'route' => ['refer.update', $refer->id],
                  'method' => 'PUT',
                  'page_title' => 'Изменение источника'
            ]);

            return Response::json(
                    array(
                  'error' => false,
                  'user' => $refer->toArray(),
                    ), 200
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $refer = Refer::find($id);

        if (!$refer) {

            return Redirect::route('refer.index')->with('danger',
                    'Такой источник не найден.');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            $refer->fill(Input::all());

            $dirty = $refer->getDirty();

            $ok = $refer->update();

            if ($ok) {

                $ret = Redirect::route('refer.show', array('id' => $id));

                //if ($dirty !== false) {

                return $ret->with('success',
                        'Данные источника обновлены: ' . implode(', ',
                            array_keys($dirty)) . '');

                return Response::json(array(
                      'error' => false,
                      'user' => $refer->toArray()), 201
                );
                //$entry = Entry::with('metrostation')->find($id);
            } else {

                //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
                //return;

                return Redirect::route('refer.edit', $id)->withErrors($this->repo->getErrors())->withInput(Input::all());

                return Response::json(array(
                      'error' => true,
                      'messages' => $this->repo->getErrors()), 400
                );
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $ret = Redirect::route('refer.index');

        $refer = Refer::find($id);

        if (!$refer) {

            $ret->with('danger', 'Такой источник не найден.');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } elseif ($refer->id == 1) {

            $ret->with('danger',
                'Этот источник нельзя удалять, можно только изменять, '
                . 'т.к. он является корневым.');
        } else {

            if ($refer->delete()) {
                $ret->with('success', 'Источник безвозвратно удалён.');
            } else {
                $ret->with('danger', 'Не удалось удалить источник.');
            }
        }
        return $ret;
    }

}
