<?php

namespace Mebius\Controller;

use \Auth;
use \Input;
use \Mebius\Repository\RoleRepository;
use \Mebius\Repository\UserRepository;
use \Redirect;
use \Response;
use \Role;
use \User;
use \View;

class UserController extends BaseController
{

    /**
     *
     * @var UserRepository
     */
    protected $repo;

    public function __construct(UserRepository $repo)
    {
        //parent::__construct($repo);
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        /* @var $repo RoleRepository */
        //$repo = App::make('Mebius\Repository\RoleRepository');
        //echo '<pre>', print_r($repo->all()->toArray()), '</pre>';

        $users = $this->repo->all();
        //echo '<pre>', print_r($users->toArray()), '</pre>';
        //die();

        return View::make('user/index',
                [
              'users' => $users,
              'page_title' => 'Пользователи'
        ]);

        return Response::json(array(
              'error' => false,
              'users' => $users->toArray()), 200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $roles = $this->getRolesList();

        //$user = new User(['role' => new Role()]);
        //echo '<pre>', print_r($user->attributesToArray()), '</pre>';

        return View::make('user/form',
                [
              'user' => new User(['role' => new Role()]),
              'roles' => $roles,
              'route' => 'user.store',
              'page_title' => 'Добавление нового пользователя'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        //echo '<pre>', print_r(Input::all()), '</pre>';
        //return;

        /** @var User $user */
        $user = $this->repo->create(Input::all());

        if ($user) {

            //$user = $this->repo->find($id);
            $user->roles()->sync(Input::get("role_id", []));

            return Redirect::route('user.show', $user->getKey())->with('success',
                    'Пользователь создан');

            return Response::json(array(
                  'error' => false,
                  'user' => $user), 201
            );
            //$entry = Entry::with('metrostation')->find($id);
        } else {

            //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
            //return;

            return Redirect::route('user.create')->withErrors($this->repo->getErrors())->withInput(Input::except('password'));

            return Response::json(array(
                  'error' => true,
                  'messages' => $this->repo->getErrors()), 400
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->repo->find($id);

        if (!$user) {

            return Redirect::route('user.index')->with('danger',
                    'Такой пользователь не найден');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            return View::make('user/show',
                    [
                  'user' => $user,
                  'page_title' => 'Пользователь ' . $user->name,
            ]);

            return Response::json(array(
                  'error' => false,
                  'user' => $user->toArray()), 200
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $check = $this->checkForAdminChange($id);
        if ($check !== true) {
            return $check;
        }

        $user = $this->repo->find($id);

        //echo '<pre>', print_r($user->toArray()), '</pre>';
        //die();

        if (!$user) {

            return Redirect::route('user.index')->with('danger',
                    'Такой пользователь не найден');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            $roles = $this->getRolesList();

            return View::make('user/form',
                    [
                  'user' => $user,
                  'roles' => $roles,
                  'route' => ['user.update', $user->id],
                  'method' => 'PUT',
                  'page_title' => 'Правка пользователя'
            ]);

            return Response::json(
                    array(
                  'error' => false,
                  'user' => $user->toArray(),
                    ), 200
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $check = $this->checkForAdminChange($id);
        if ($check !== true) {
            return $check;
        }

        $user = $this->repo->find($id);

        if (!$user) {

            return Redirect::route('user.index')->with('danger',
                    'Такой пользователь не найден');

            return Response::json(array(
                  'error' => true,
                  'message' => 'Not found'), 404
            );
        } else {

            $dirty = $this->repo->update($user, Input::all());

            //die();

            if ($dirty !== false) {

                $ret = Redirect::route('user.show', $id);

                if (!count($dirty)) {

                    return $ret->with('info',
                            'Данные пользователя не изменены - изменять нечего');
                } else {

                    return $ret->with('success',
                            'Данные пользователя обновлены: ' . implode(', ',
                                array_keys($dirty)) . '');
                }

                return Response::json(array(
                      'error' => false,
                      'user' => $this->repo->find($id)), 201
                );
                //$entry = Entry::with('metrostation')->find($id);
            } else {

                //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
                //return;

                return Redirect::route('user.edit', $id)->withErrors($this->repo->getErrors())->withInput(Input::except('password'));

                return Response::json(array(
                      'error' => true,
                      'messages' => $this->repo->getErrors()), 400
                );
            }
        }
    }

    private function checkForAdminChange($id)
    {
        if ($id == 1 && Auth::id() != 1) {
            return Redirect::route('user.index')->with('danger',
                    'Админа может править только сам admin');
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ret = Redirect::route('user.index');

        if ($id == Auth::user()->id) {
            return $ret->with('danger', 'Себя удалить нельзя');
        }

        if ($this->repo->destroy($id)) {
            $ret->with('success', 'Пользователь удалён');
        } else {
            $ret->with('danger', 'Не удалось удалить пользователя');
        }
        return $ret;
    }

    /////////////////////

    private function getRolesList()
    {
        $R = \App::make('\Mebius\Model\Contracts\RoleInterface');
        return $R->getList();
    }

}
