<?php

namespace Mebius\Controller;

use \Auth;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Domain\ExportData;
use \Mebius\Repository\CashboxRepository;
use \Mebius\Repository\Exceptions\NotFoundException;
use \Mebius\Service\ExcelExportService;
use \Redirect;
use \URL;
use \View;

class CashboxController extends BaseController
{

    /**
     *
     * @var CashboxRepository
     */
    protected $repo;

    public function __construct(CashboxRepository $repo)
    {
        //parent::__construct($repo);
        $this->repo = $repo;
    }

    public function getIndex()
    {
        $input = $this->repo->getAllowedOptionsInput('cashbox.index');
        //echo '<pre>', print_r($input), '</pre>';

        $options = $this->repo->getOptions('cashbox.index');
        //echo '<pre>', print_r($options), '</pre>';
        //die();

        $payments = $this->repo->all($input);
        //echo '<pre>', print_r($payments->toArray()), '</pre>';
        //die();

        $data = [
          'input' => $input,
          'options' => $options,
          'payments' => $payments,
          'page_title' => 'Касса',
        ];

        if (route_allowed('cashbox.export') && $payments->count()) {
            $data['routes']['export'] = [
              'route' => URL::route('cashbox.export', $input),
              'name' => 'Экспорт списка',
            ];
        }

        return View::make('cashbox.index', $data);
    }

    public function show($order_key)
    {
        try {
            $data = $this->repo->getShowData($order_key);
            return View::make('cashbox.show', $data);
        } catch (NotFoundException $e) {
            return Redirect::route('cashbox.index')
                    ->with(static::ERRROR, $e->getMessage());
        } catch (ActionNotAllowedException $e) {
            return Redirect::route('cashbox.index')
                    ->with(static::ERRROR, $e->getMessage());
        }
    }

    public function export()
    {

        //$this->repo->flushInputOptions('report.index');

        $input = $this->repo->getAllowedOptionsInput('cashbox.index');
        //echo '<pre>input1: ', print_r($input), '</pre>';

        $input = $this->repo->filterInput($input);
        //echo '<pre>input2: ', print_r($input), '</pre>';
        //$only = $this->repo->getAllowedInputDictionary($input);
        //echo '<pre>only: ', print_r($only), '</pre>';

        $options = $this->repo->getOptions('cashbox.index');
        //echo '<pre>options: ', print_r($options), '</pre>';

        $category = "Касса";
        $title = $category;

        foreach ($input as $key => $val) {
            $title .= ' - ' . $options[$key][$val];
        }

        //echo $title . '<br/>';
        //die();

        $entries = $this->repo->allForExport($input);
        //echo '<pre>entries: ', print_r($entries), '</pre>';
        //var_dump($entries);
        //die();

        if (!$entries) {
            return Redirect::route('report.index', $input)->with('danger',
                    'Нет ни одной записи для экспорта')->send();
        }
        //echo '<pre>', print_r($entries), '</pre>';

        $head = $this->repo->getFieldsValues();

        //echo '<pre>head: ', print_r($head), '</pre>';
        //echo '<pre>entries: ', print_r($entries), '</pre>';
        //die();

        $Data = new ExportData();
        $Data->setCategory($category);
        $Data->setAuthor(Auth::user()->name);
        $Data->setTitle($title);
        $Data->setHead($head);
        $Data->setEntries($entries);

        $export = new ExcelExportService();
        $export->send($Data);
    }

}
