<?php

namespace Mebius\Controller;

use \Mebius\Repository\MainRepositoryInterface;
use \View;

class MainController extends BaseController
{

    /**
     *
     * @var MainRepositoryInterface
     */
    protected $repo;

    public function __construct(MainRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    public function indexAction()
    {
        $data = $this->repo->getIndexData();

        return View::make('main/index',
                [
              'data' => $data,
              'page_title' => $data->getTitle(),
        ]);
    }

}
