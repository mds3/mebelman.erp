<?php

/*
 * topot
 */

namespace Mebius\Controller\Exceptions;

use \Exception;

/**
 * ActionNotAllowedException
 *
 * @author 1
 */
class ActionNotAllowedException extends Exception
{

    private $fillable = ['message'];

    public function __set($name, $value)
    {
        if (isset($this->{$name}) && in_array($name, $this->fillable)) {
            $this->{$name} = $value;
        }
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

}
