<?php

namespace Mebius\Controller;

use \App;
use \Auth;
use \Entry;
use \Event;
use \Illuminate\Http\RedirectResponse;
use \Input;
use \Mebius\Domain\DataObject;
use \Mebius\Domain\DataRoute;
use \Mebius\Domain\ExportData;
use \Mebius\Repository\OrderEloquentRepository;
use \Mebius\Repository\OrderRepository;
use \Mebius\Service\ExcelExportService;
use \Order;
use \Redirect;
use \Response;
use \URL;
use \View;

class OrderController extends BaseController
{

    /**
     * Хранилище
     * @var OrderEloquentRepository
     */
    protected $repo;

    /**
     * Используемая сущность
     */
    const ENTITY = 'order';

    /**
     * Используемая сущность префиксом
     */
    const ENTITY_PREF = 'order.';

    /**
     *
     */
    const ENTITY_CANCEL = 'order.cancel';
    const ENTITY_CASHBOX = 'order.cashbox';
    const ENTITY_EDIT = 'order.edit';
    const ENTITY_EXPORT = 'order.export';
    const ENTITY_FINISH = 'order.finish';
    const ENTITY_INDEX = 'order.index';
    const ENTITY_SEARCH = 'order.search';
    const ENTITY_SHOW = 'order.show';
    const ENTITY_SHOWLOGS = 'order.showlogs';
    const ENTITY_STUFF = 'order.stuff';

    public function __construct(OrderRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $input = $this->repo->getAllowedOptionsInput(self::ENTITY_INDEX);

        $options = $this->repo->getOptions(self::ENTITY_INDEX);

        $orders = $this->repo->all($input);

        //echo '<pre>', print_r($orders->toArray()), '</pre>';
        //die();

        foreach ($orders as $order) {
            $order->url = URL::route(self::ENTITY_SHOW, $order->getKey());
        }

        $data = [
          'orders' => $orders,
          'input' => $input,
          'options' => $options,
          'page_title' => trans('msg.orders'),
          'cells' => $this->repo->getTypeCells($input),
          'icons' => $this->repo->getIconsDict(),
        ];

        if (route_allowed(self::ENTITY_SEARCH)) {
            $data['routes']['search'] = [
              'method' => 'GET',
              'route' => URL::route(self::ENTITY_SEARCH),
              'name' => trans(self::ENTITY_SEARCH),
            ];
        }

        if (route_allowed(self::ENTITY_EXPORT) && $orders->count()) {
            $data['routes']['export'] = [
              'method' => 'GET',
              'route' => URL::route(self::ENTITY_EXPORT, $input),
              'name' => trans('msg.export'),
            ];
        }

        return View::make('order/index', $data);

        /* return Response::json(array(
          'error' => false,
          'data' => $entries->toArray()), 200
          ); */
    }

    /**
     * @return mixed \Entry|Redirect
     */
    private function getOrderEntry()
    {
        $opt = get_allowed_options(self::ENTITY_PREF . 'create');

        $prm = isset($opt[0]) ? $opt[0] : false;

        $entry_id = (int) Input::get('id', 0);

        if (!$entry_id) {
            return Redirect::route('entry.index')->with(static::ERRROR,
                    trans(self::ENTITY_PREF . 'entry_not_found'));
        }

        $params[] = ['id', '=', $entry_id];
        if ($prm) {
            $params[] = [$prm, '=', Auth::id()];
        }

        $EntryRepository = App::make("\Mebius\Repository\EntryRepository");

        $entry = $EntryRepository->findOneBy($params);

        if (!$entry) {
            return Redirect::route('entry.index')->with(static::ERRROR,
                    trans(self::ENTITY_PREF . 'entry_not_found'));
        }

        if (!$entry->is_allowed_for_order) {
            return Redirect::route('entry.show', [$entry->id])->with(
                    static::ERRROR,
                    trans(self::ENTITY_PREF . 'entry_not_complete_for_order'));
        }

        $order = $this->repo->find($entry->order_key);

        if ($order) {
            Event::fire('order.created_ok', [$entry, $order]);
            return Redirect::route(self::ENTITY_SHOW, $entry->order_key)->with(static::ERRROR,
                    trans(self::ENTITY_PREF . 'order_isset'));
        }

        return $entry;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $entry = $this->getOrderEntry();

        if ($entry instanceof RedirectResponse) {
            return $entry;
        }

        $data = [
          'entry' => $entry,
          'route' => self::ENTITY_PREF . 'store',
          'routes' => [],
          'page_title' => trans(self::ENTITY_PREF . 'page_title') . $entry->order_key,
        ];

        return View::make(self::ENTITY_PREF . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @return Response
     */
    public function store()
    {

        $entry = $this->getOrderEntry();

        if ($entry instanceof RedirectResponse) {
            return $entry;
        }

        /* @var $entry Entry */
        $input = $entry->getOriginal();

        $input += Input::all();

        //echo '<pre>entry: ', print_r($entry->toArray()), '</pre>';
        //echo '<pre>input: ', print_r($input), '</pre>';
        //die();

        $order = $this->repo->create($input);

        if ($order) {

            $order->created_by = $entry->user_id;
            $order->save();

            Event::fire('order.created_ok', [$entry, $order]);

            return Redirect::route(self::ENTITY_SHOW, $order->getKey())->with(static::OK,
                    trans(self::ENTITY_PREF . 'created_ok'));

            /* return Response::json(
              [
              'error' => false,
              self::ENTITY => $order,
              ], 201
              ); */
        } else {

            return Redirect::route(self::ENTITY_PREF . 'create',
                    ['id' => $entry->id])->withErrors($this->repo->getErrors())->withInput(Input::all());

            /* return Response::json(array(
              'error' => true,
              'messages' => $this->repo->getErrors()), 400
              ); */
        }
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $order = $this->findOrderOrRedirect($id);

        if (!$order) {
            return;
        }

        $this->checkIsAllowedAction(self::ENTITY_SHOW, $order);

        Event::fire(self::ENTITY_SHOW, [$order, $this->repo]);

        $data = [
          'page_title' => trans(self::ENTITY_PREF . 'page_title') . $order->order_key,
        ];

        $data['routes']['index'] = [
          'method' => 'GET',
          'route' => route('order.index'),
          'name' => trans('order.index'),
        ];

        $DATA = new DataObject();

        $data['routes']['order.index'] = new DataRoute(
            'order.index', 'GET', route('order.index'), trans('order.index')
        );
        $DATA->addRoute(new DataRoute(
            'order.index', 'GET', route('order.index'), trans('order.index')
        ));

        if (!$order->is_archived) {

            if ($order->is_active && action_allowed(self::ENTITY_FINISH, $order)) {
                $data['routes']['finish'] = [
                  'method' => 'POST',
                  'route' => URL::route(self::ENTITY_FINISH, $order->getKey()),
                  'name' => trans(self::ENTITY_FINISH),
                ];
            }

            if ($order->is_finished && !$order->has_debt && action_allowed(self::ENTITY_PREF . 'archivate',
                    $order)) {
                $data['routes']['archivate'] = [
                  'method' => 'POST',
                  'route' => URL::route(self::ENTITY_PREF . 'archivate',
                      $order->getKey()),
                  'name' => trans(self::ENTITY_PREF . 'do_archived'),
                ];
            }

            if (action_allowed(self::ENTITY_EDIT, $order)) {
                $data['routes']['edit'] = [
                  'method' => 'GET',
                  'route' => URL::route(self::ENTITY_EDIT, $order->getKey()),
                  'name' => trans(self::ENTITY_EDIT),
                ];
            }

            if (action_allowed(self::ENTITY_CANCEL, $order)) {
                $data['routes']['cancel'] = [
                  'method' => 'POST',
                  'route' => URL::route(self::ENTITY_CANCEL, $order->getKey()),
                  'name' => trans(self::ENTITY_CANCEL),
                ];
            }

            if (action_allowed(self::ENTITY_STUFF, $order)) {
                $data['routes']['stuff'] = [
                  'method' => 'POST',
                  'route' => URL::route(self::ENTITY_STUFF, $order->getKey()),
                  'name' => trans(self::ENTITY_STUFF),
                ];
            }

            if ($order->has_cashbox_debt && action_allowed(self::ENTITY_CASHBOX,
                    $order)) {
                $data['routes']['cashbox'] = [
                  'method' => 'POST',
                  'route' => route(self::ENTITY_CASHBOX, $order->getKey()),
                  'name' => trans('order.cashbox_add'),
                ];
            }
        }

        if (action_allowed(self::ENTITY_SHOWLOGS, $order)) {
            $data['routes']['showlogs'] = [
              'method' => 'GET',
              'route' => URL::route(self::ENTITY_SHOWLOGS, $order->getKey()),
              'name' => trans(self::ENTITY_SHOWLOGS),
            ];
        }

        if (action_allowed('cashbox.show', $order)) {
            $data['routes']['payments'] = [
              'route' => route('cashbox.show', $order->getKey()),
              'name' => trans('order.cashbox_show'),
            ];
        }

        //echo '<pre>', print_r($data), '</pre>';
        //die();

        $data['order'] = $order;

        return View::make(self::ENTITY_SHOW, $data);

        /* return Response::json(array(
          'error' => false,
          'data' => $data), 200
          ); */
    }

    public function cancel($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        $canceled = $this->repo->cancelOrder($order);

        $ret = Redirect::route(self::ENTITY_SHOW,
                array($order->getKeyName() => $order->getKey()));

        if (!$canceled) {
            return $ret->with(static::ERRROR,
                    trans(self::ENTITY_PREF . 'cant_cancel'));
        } else {
            return $ret->with(static::OK,
                    trans(self::ENTITY_PREF . 'cancelled_ok'));
        }
    }

    public function stuff($id)
    {
        $order = $this->repo->find($id);

        $saved = $this->repo->saveStuff($order, Input::all());

        $ret = Redirect::route(self::ENTITY_SHOW,
                array($order->getKeyName() => $order->getKey()));

        if (!$saved) {
            return $ret->with(static::INFO,
                    trans(self::ENTITY_PREF . 'stuff_not_changed'));
        } else {
            return $ret->with(static::OK,
                    trans(self::ENTITY_PREF . 'stuff_saved'));
        }
    }

    public function archivate($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        if (!$order->has_debt) {
            $this->repo->archivate($order);
        }

        return Redirect::route(self::ENTITY_SHOW, $id)->with(static::OK,
                trans(self::ENTITY_PREF . 'archived'));
    }

    /**
     * Возвращает логи по заявке
     * @param integer $id
     * @return Response
     */
    public function showlogs($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        $this->checkIsAllowedAction(self::ENTITY_SHOWLOGS, $order);

        $EventsLog = $this->repo->getEventsLog($order);

        return Response::json(array(
              'error' => false,
              'logs' => $EventsLog->toArray()), 200
        );
    }

    /**
     * Добавление суммы в кассу
     * @param integer $id
     * @return Response
     */
    public function cashbox($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        $this->checkIsAllowedAction(self::ENTITY_CASHBOX, $order);

        $cash = (int) Input::get('cash', 0);

        if (!$cash) {
            $ret = [
              'error' => true,
              'msg' => trans(self::ENTITY_PREF . 'null_summ'),
            ];
        } elseif ($cash > $order->cashbox_debt) {
            $ret = [
              'error' => true,
              'msg' => trans(self::ENTITY_PREF . 'big_summ'),
            ];
        } else {

            $order->addToCashBox($cash);

            if (!$order->update()) {
                $ret = [
                  'error' => true,
                  'msg' => trans(self::ENTITY_PREF . 'error_add_cashbox')
                  . $order->getErrors()->first()
                ];
            } else {

                $order->payments()->create([
                  'order_id' => $order->id,
                  'summ' => $cash,
                  'cash' => $order->cashless ? 'N' : 'Y'
                ]);

                $ret = [
                  'error' => false,
                  'data' => [
                    'added' => $cash,
                    'cashbox' => $order->cashbox,
                    'cashbox_debt' => $order->cashbox_debt,
                  ]
                ];
            }
        }
        return Response::json($ret, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        $this->checkIsAllowedAction(self::ENTITY_SHOW, $order);

        $func = is_null($order->master) ? 'getMastersSelectableList' : 'getMastersList';

        $masters = $this->repo->{$func}($this->repo->getMasterRoleId());

        $refers = $this->getRefersList();

        $routes = [
          'return' => [
            'method' => 'GET',
            'route' => route(self::ENTITY_SHOW, $order->getKey()),
            'name' => trans('msg.cancel'),
          ]
        ];

        return View::make('order/form',
                [
              'order' => $order,
              'masters' => $masters,
              'refers' => $refers,
              'route' => [self::ENTITY_PREF . 'update', $order->getKey()],
              'method' => 'PUT',
              'routes' => $routes,
              'page_title' => trans(self::ENTITY_PREF . 'page_title') . $order->order_key . ' / ' . trans(self::ENTITY_PREF . 'edition'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $order = $this->findOrderOrRedirect($id);

        $dirty = $this->repo->update($order, Input::all());

        if ($dirty !== false) {

            $ret = Redirect::route(self::ENTITY_SHOW,
                    array($order->getKeyName() => $order->getKey()));

            if (!count($dirty)) {

                return $ret->with('info',
                        trans(self::ENTITY_PREF . 'nothing_to_change'));
            } else {

                return $ret->with(static::OK,
                        trans(self::ENTITY_PREF . 'changed_ok') . ': ' . implode(', ',
                            array_map(function($item) {
                                return trans(self::ENTITY_PREF . $item);
                            }, array_keys($dirty))) . '');
            }

            return Response::json(array(
                  'error' => false,
                  self::ENTITY => $this->repo->find($order->getKey())), 201
            );
        } else {

            return Redirect::route(self::ENTITY_EDIT, $order->getKey())
                    ->withErrors($this->repo->getErrors())->withInput(Input::all());

            /* return Response::json(array(
              'error' => true,
              'messages' => $this->repo->getErrors()), 400
              ); */
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Удалять нельзя - только архив
        return 'destroy (DELETE) id:' . $id;
    }

    public function finish($id)
    {
        $order = $this->findOrderOrRedirect($id);

        $this->checkIsAllowedAction(self::ENTITY_FINISH, $order);

        if ($order->is_finished) {
            return Redirect::route(self::ENTITY_INDEX)->with(static::OK,
                    trans(self::ENTITY_PREF . 'page_title') . $order->order_key . ' ' . trans(self::ENTITY_PREF . 'finished_early'));
        }

        $input['result_amount'] = Input::get('result_amount', 0);
        $input['docs_signed'] = Input::get('docs_signed', 0);

        if (!$this->repo->finishOrder($order, $input)) {
            return Redirect::route(self::ENTITY_SHOW, $order->getKey())->withErrors($this->repo->getErrors())->withInput($input);
        }

        return Redirect::route(self::ENTITY_SHOW, $order->getKey())->with(static::OK,
                trans(self::ENTITY_PREF . 'page_title') . $order->getKey() . ' ' . trans(self::ENTITY_PREF . 'finished_ok'));
    }

    public function status($id, $status)
    {
        $statuses = ['close'];

        if (in_array($status, $statuses)) {
            return $this->{$status}($id);
        }
        return Redirect::route(self::ENTITY_SHOW, $id)->with(static::ERRROR,
                trans(self::ENTITY_PREF . 'status_invalid'));
    }

    public function close($id)
    {
        $order = $this->findOrderOrRedirect($id);
        if (!$order) {
            return;
        }

        $this->checkIsAllowedAction(self::ENTITY_PREF . 'close', $order);

        if (!$this->repo->closeOrder($order)) {
            $ret = [
              'error' => true,
              'msg' => $this->repo->getErrors()[0],
            ];
        } else {

            $ret = [
              'error' => false,
                /* 'data' => [] */
            ];
        }
        return Response::json($ret, 200);
    }

    public function search()
    {
        $input = Input::only([
              "search_by",
              "search_val"
        ]);

        $orders = [];

        if (Input::exists('doSearch')) {
            $orders = $this->repo->searchByInputOptions($input);
        } else {
            $orders = $this->repo->getCollection();
        }

        $data = [
          'page_title' => trans(self::ENTITY_SEARCH),
          'options' => $this->repo->getSearchOptionsListWithTypes(),
          'input' => $input,
          'orders' => $orders,
        ];

        return View::make(self::ENTITY_SEARCH, $data);
    }

    public function missingMethod($parameters = array())
    {
        //parent::missingMethod($parameters);
        return '405 Method Not Allowed';
    }

    /**
     * @param int $id
     * @return Order
     */
    private function findOrderOrRedirect($id)
    {
        $order = $this->repo->find($id);

        if ($order) {
            return $order;
        }

        if (!$order) {
            Redirect::route(self::ENTITY_INDEX)->with(static::ERRROR,
                trans(self::ENTITY_PREF . 'not_found') . ': ' . $id)->send();
            /* Response::json(array(
              'error' => true,
              'message' => 'Not found'), 404
              )->send(); */
        }
        return $order;
    }

    public function export()
    {
        $input = $this->repo->getAllowedOptionsInput(self::ENTITY_INDEX);

        $options = $this->repo->getOptions(self::ENTITY_INDEX);

        $category = trans('msg.orders');
        $title = $category;
        foreach ($input as $key => $val) {
            $title .= ' - ' . $options[$key][$val];
        }

        $entries = $this->repo->allForExport($input);
        if (!$entries) {
            return Redirect::route(self::ENTITY_INDEX, $input)->with(static::ERRROR,
                    trans(self::ENTITY_PREF . 'nothing_for_export'))->send();
        }

        $head = array_map(function ($value) {
            $value = trans('order.' . $value);
            $value = is_array($value) ? $value['name'] : $value;
            return $value;
        }, array_keys($entries[0]));

        $Data = new ExportData();
        $Data->setCategory($category);
        $Data->setAuthor(Auth::user()->name);
        $Data->setTitle($title);
        $Data->setHead($head);
        $Data->setEntries($entries);

        $export = new ExcelExportService();
        $export->send($Data);
    }

}
