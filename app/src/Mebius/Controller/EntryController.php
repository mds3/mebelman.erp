<?php

namespace Mebius\Controller;

use \Auth;
use \Entry;
use \EventsLog;
use \Input;
use \Mebius\Controller\Exceptions\ActionNotAllowedException;
use \Mebius\Domain\ExportData;
use \Mebius\Repository\EntryEloquentRepository;
use \Mebius\Repository\EntryRepository;
use \Mebius\Repository\Exceptions\NotFoundException;
use \Mebius\Service\ExcelExportService;
use \Redirect;
use \Response;
use \URL;
use \View;

class EntryController extends BaseController
{
    
    /**
     * @var EntryEloquentRepository
     */
    protected $repo;
    
    /**
     * Используемая сущность
     */
    const ENTITY = 'entry';
    
    /**
     * Используемая сущность префиксом
     */
    const ENTITY_PREF = 'entry.';
    
    /**
     *
     */
    const ENTITY_INDEX    = 'entry.index';
    const ENTITY_SHOW     = 'entry.show';
    const ENTITY_CREATE   = 'entry.create';
    const ENTITY_STORE    = 'entry.store';
    const ENTITY_EDIT     = 'entry.edit';
    const ENTITY_UPDATE   = 'entry.update';
    const ENTITY_EXPORT   = 'entry.export';
    const ENTITY_SEARCH   = 'entry.search';
    const ENTITY_SHOWLOGS = 'entry.showlogs';
    
    public function __construct(EntryRepository $repo)
    {
        $this->repo = $repo;
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = $this->repo->getIndexData(Input::all());
        
        return View::make('entry/index', $data);
        
        /* return Response::json(array(
          'error' => false,
          'entries' => $entries->toArray()), 200
          ); */
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data = $this->repo->getCreateData();
        return View::make('entry/form', $data);
    }
    
    /**
     * Store a newly created resource in storage.
     * @return Response
     */
    public function store()
    {
        
        $entry = $this->repo->create(Input::all());
        
        if ($entry) {
            
            
            return Redirect::route(self::ENTITY_SHOW, $entry->getKey())->with('success',
                'Заявка создана');
            
            /* return Response::json(
              [
              'error' => false,
              'entry' => $entry,
              ], 201
              ); */
            //$entry = Entry::with('metrostation')->find($id);
        } else {
            
            //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
            //return;
            
            return Redirect::route(self::ENTITY_CREATE)->withErrors($this->repo->getErrors())->withInput(Input::all());
            
            /* return Response::json(array(
              'error' => true,
              'messages' => $this->repo->getErrors()), 400
              ); */
        }
    }
    
    /**
     * Display the specified resource.
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $data = $this->repo->getShowData($id);
            //echo '<pre>', print_r($data), '</pre>';
            return View::make(self::ENTITY_SHOW, $data);
        } catch (NotFoundException $e) {
            return Redirect::route(self::ENTITY_INDEX)
                ->with(static::ERRROR, $e->getMessage());
        } catch (ActionNotAllowedException $e) {
            return Redirect::route(self::ENTITY_INDEX)
                ->with(static::ERRROR, $e->getMessage());
        }
        /* return Response::json(array(
          'error' => false,
          'user' => $entry->toArray()), 200
          ); */
    }
    
    public function search()
    {
        $input = Input::only([
            'search_val',
            'search_by',
            'doSearch',
        ]);
        //echo '<pre>', print_r($input), '</pre>';
        
        $entries = [];
        
        //echo '<pre>', print_r(Input::all()), '</pre>';
        //die();
        
        if (isset($input['doSearch'])) {
            $entries = $this->repo->searchByInputOptions($input);
        } else {
            $entries = $this->repo->getCollection();
        }
        
        foreach ($entries as $entry) {
            $entry->url = URL::route(self::ENTITY_SHOW, $entry->id);
        }
        
        $data = [
            'page_title' => 'Поиск заказа',
            'options'    => $this->repo->getSearchOptionsListWithTypes(),
            'input'      => $input,
            'entries'    => $entries,
            'icons'      => $this->repo->getIconsDict(),
        ];
        
        //$options = $this->repo->getSearchOptionsList();
        //echo '<pre>', print_r($data), '</pre>';
        //die();
        
        return View::make(self::ENTITY_SEARCH, $data);
    }
    
    /**
     * Возвращает логи по заявке
     * @param integer $id
     * @return Response
     */
    public function showlogs($id)
    {
        $entry = $this->findEntryOrRedirect($id);
        if (! $entry) {
            return;
        }
        
        $EventsLog = EventsLog::with([
            'creator' => function ($query) {
                $query->select('id', 'name');
            },
        ])
            ->where('model', '=', 'Entry')
            ->where('model_id', '=', $entry->getKey())
            ->get();
        
        return Response::json([
            'error' => false,
            'logs'  => $EventsLog->toArray()], 200
        );
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $entry = $this->findEntryOrRedirect($id);
        if (! $entry) {
            return;
        }
        
        $this->checkIsAllowedAction(self::ENTITY_EDIT, $entry);
        
        $func = is_null($entry->master) ? 'getMastersSelectableList' : 'getMastersList';
        
        $masters = $this->repo->{$func}($this->repo->getMasterRoleId());
        
        $suppliers = $this->repo->getSuppliersSelectableList();
        
        $refers = $this->getRefersList();
        
        $routes = [
            'return' => [
                'method' => 'GET',
                'route'  => route(self::ENTITY_SHOW, $entry->getKey()),
                'name'   => trans('msg.cancel'),
            ],
        ];
        
        return View::make('entry/form',
            [
                'entry'      => $entry,
                'masters'    => $masters,
                'suppliers'  => $suppliers,
                'refers'     => $refers,
                'route'      => route(self::ENTITY_UPDATE, $entry->getKey()),
                'method'     => 'PUT',
                'routes'     => $routes,
                'page_title' => trans('entry.edition'),
            ]);
        
        /* return Response::json(
          array(
          'error' => false,
          'entry' => $entry->toArray(),
          ), 200
          ); */
    }
    
    /**
     * @param int $id
     * @return Redirect|Response
     */
    public function status($id)
    {
        $entry = $this->findEntryOrRedirect($id);
        if (! $entry) {
            return;
        }
        
        $this->checkIsAllowedAction('entry.status', $entry);
        
        $status = Input::get('_status');
        
        $input = [];
        
        switch ($status) {
            case 'refuse':
                $entry->unsetMasterDates();
                $entry->setRefuseTime();
                $entry->setCallTime();
                $entry->setStatus('refuse');
                $input = Input::only(['_status', 'refuse_reason']);
                break;
            case 'defer':
                $entry->unsetMasterDates();
                $entry->setDeferTime();
                $entry->setCallTime();
                $entry->setStatus('defer');
                $input = Input::only(['_status', 'defer_before', 'defer_reason']);
                break;
            case 'reanimate':
                //die('reanimate case');
                $entry->unsetMasterDates();
                $entry->setRefuseTime(0);
                $entry->setStatus('active');
                $input = Input::only(['_status']);
                break;
            case 'visit':
                $input = Input::only(['_status', 'visit_time', 'details']);
                if (empty($input['visit_time'])) {
                    $input['visit_time'] = false;
                }
                $entry->setStatus('active');
                $entry->setCallTime();
                break;
            case 'archiving':
                $entry->unsetMasterDates();
                $entry->setStatus('archive');
                $input = Input::only(['_status']);
            default:
                break;
        }
        
        return $this->doUpdate($entry, $input);
    }
    
    /**
     * Update the specified resource in storage.
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $entry = $this->findEntryOrRedirect($id);
        if (! $entry) {
            return;
        }
        
        $this->checkIsAllowedAction(self::ENTITY_UPDATE, $entry);
        
        $input = Input::all();
        
        //echo '<pre>', print_r($input), '</pre>';
        //die();
        $entry->unsetEmptyDates();
        //$entry->unsetMasterDates();
        
        return $this->doUpdate($entry, $input);
    }
    
    /**
     * @param int $id
     * @return Entry
     */
    private function findEntryOrRedirect($id)
    {
        $entry = $this->repo->find($id);
        
        if (! $entry) {
            
            Redirect::route(self::ENTITY_INDEX)->with('danger',
                'Такая заявка не найдена')->send();
            
            /* Response::json(array(
              'error' => true,
              'message' => 'Not found'), 404
              )->send(); */
        }
        return $entry;
    }
    
    /**
     * @param Entry $entry
     * @param array $input
     * @return Redirect|Response
     */
    private function doUpdate(Entry $entry, array $input)
    {
        
        //echo '<pre>input: ', print_r($input), '</pre>';
        //echo '<pre>entry: ', print_r($entry->toArray()), '</pre>';
        //die();
        
        $dirty = $this->repo->update($entry, $input);
        
        //die();
        
        if ($dirty !== false) {
            
            $ret = Redirect::route(self::ENTITY_SHOW,
                [$entry->getKeyName() => $entry->getKey()]);
            
            if (! count($dirty)) {
                
                return $ret->with('info',
                    'Данные заявки не изменены - изменять нечего');
            } else {
                
                return $ret->with('success',
                    'Данные заявки обновлены: '.implode(', ',
                        array_map(function ($item) {
                            return trans('entry.'.$item);
                        }, array_keys($dirty))).'');
            }
            
            return Response::json([
                'error' => false,
                'entry' => $this->repo->find($entry->getKey())], 201
            );
            //$entry = Entry::with('metrostation')->find($id);
        } else {
            
            //echo '<pre>', print_r($this->repo->getErrors()), '</pre>';
            //return;
            
            if (isset($input['_status'])) {
                $route = 'show';
            } else {
                $route = 'edit';
            }
            
            return Redirect::route('entry.'.$route, $entry->getKey())->withErrors($this->repo->getErrors())->withInput($input);
            
            /* return Response::json(array(
              'error' => true,
              'messages' => $this->repo->getErrors()), 400
              ); */
        }
    }
    
    public function export()
    {
        
        $input = $this->repo->getAllowedOptionsInput(self::ENTITY_INDEX);
        //echo '<pre>', print_r($input), '</pre>';
        $options = $this->repo->getOptions(self::ENTITY_INDEX);
        //echo '<pre>', print_r($options), '</pre>';
        
        $category = "Заявки";
        $title    = $category;
        foreach ($input as $key => $val) {
            $title .= ' - '.(isset($options[$key]) ? $options[$key][$val] : '-');
        }
        
        //echo $title . '<br/>';
        
        $entries = $this->repo->allForExport($input);
        if (! $entries) {
            return Redirect::route(self::ENTITY_INDEX, $input)->with('danger',
                'Нет ни одной записи для экспорта')->send();
        }
        //echo '<pre>', print_r($entries), '</pre>';
        $head = array_map(function ($value) {
            $value = trans('entry.'.$value);
            $value = is_array($value) ? $value['name'] : $value;
            return $value;
        }, array_keys($entries[0]));
        
        /* echo '<pre>', print_r($head), '</pre>';
          echo '<pre>', print_r($entries), '</pre>';
          die(); */
        
        $Data = new ExportData();
        $Data->setCategory($category);
        $Data->setAuthor(Auth::user()->name);
        $Data->setTitle($title);
        $Data->setHead($head);
        $Data->setEntries($entries);
        
        $export = new ExcelExportService();
        $export->send($Data);
    }
    
    /**
     * Remove the specified resource from storage.
     * @param  int $id
     * @return Response
     */
    /* public function destroy($id)
      {
      $entry = $this->findEntryOrRedirect($id);
      if (!$entry) {
      return;
      }

      $this->checkIsAllowedAction('entry.destroy', $entry);

      $ret = Redirect::route(self::ENTITY_INDEX);

      if ($this->repo->destroy($id)) {
      $ret->with('success', 'Заявка удалена');
      } else {
      $ret->with('danger',
      'Не удалось удалить заявку: ' . $this->repo->getErrors()[0]);
      }
      return $ret;
      } */
}
