<?php

namespace Mebius\Controller;

use \Illuminate\Support\Facades\Auth;
use \Mebius\Domain\ExportData;
use \Mebius\Repository\ReportEloquentRepository;
use \Mebius\Repository\ReportRepository;
use \Mebius\Service\ExcelExportService;
use \Redirect;
use \URL;
use \View;

class ReportController extends BaseController
{

    /**
     *
     * @var ReportEloquentRepository
     */
    protected $repo;

    public function __construct(ReportRepository $repo)
    {
        //parent::__construct($repo);
        $this->repo = $repo;
    }

    public function getIndex()
    {
        $input = $this->repo->getAllowedOptionsInput('report.index');
        //echo '<pre>', print_r($input), '</pre>';
        $input = $this->repo->filterInput($input);

        $selects = $this->repo->getOptions('report.index', $input);
        //echo '<pre>', print_r($selects), '</pre>';
        //die();

        $reports = $this->repo->all($input);

        $data = [
          'input' => $input,
          'selects' => $selects,
          'reports' => $reports,
          'fields' => $this->repo->getReportFieldsValues(),
          'page_title' => 'Отчеты',
        ];

        if (route_allowed('report.export') && $reports->count()) {
            $data['routes']['export'] = [
              'route' => URL::route('report.export', $input),
              'name' => trans('msg.export'),
            ];
        }

        return View::make('report.index', $data);
    }

    public function export()
    {

        //$this->repo->flushInputOptions('report.index');

        $input = $this->repo->getAllowedOptionsInput('report.index');
        //echo '<pre>input1: ', print_r($input), '</pre>';

        $input = $this->repo->filterInput($input);
        //echo '<pre>input2: ', print_r($input), '</pre>';

        $only = $this->repo->getAllowedInputDictionary($input);
        //echo '<pre>only: ', print_r($only), '</pre>';

        $options = $this->repo->getOptions('report.index');
        //echo '<pre>options: ', print_r($options), '</pre>';

        $category = "Отчеты";
        $title = $category;

        foreach ($only as $key => $val) {
            if (isset($input[$key])) {
                $title .= ' - ' . $options[$val][$input[$key]];
            }
        }

        //echo $title . '<br/>';

        $entries = $this->repo->allForExport($input);
        //echo '<pre>entries: ', print_r($entries), '</pre>';
        //var_dump($entries);
        //die();

        if (!$entries) {
            return Redirect::route('report.index', $input)->with('danger',
                    'Нет ни одной записи для экспорта')->send();
        }
        //echo '<pre>', print_r($entries), '</pre>';

        $head = $this->repo->getReportFieldsValues();

        //echo '<pre>head: ', print_r($head), '</pre>';
        //echo '<pre>entries: ', print_r($entries), '</pre>';
        //die();

        $Data = new ExportData();
        $Data->setCategory($category);
        $Data->setAuthor(Auth::user()->name);
        $Data->setTitle($title);
        $Data->setHead($head);
        $Data->setEntries($entries);

        $export = new ExcelExportService();
        $export->send($Data);
    }

}
