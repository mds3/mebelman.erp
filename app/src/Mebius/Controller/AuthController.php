<?php

namespace Mebius\Controller;

use Input;
use Redirect;
use User;
use Validator;
use View;

class AuthController extends BaseController
{

    public function showLogin()
    {
        // Check if we already logged in
        if (\Auth::check()) {
            return Redirect::to('/')->with('success', 'Вы уже авторизованы');
        }

        return View::make('auth/loginForm',
            [
                'page_title' => 'Авторизация',
                'route'      => 'login',
            ]);
    }

    public function postLogin()
    {

        $user_data = [
            'email'    => Input::get('user_email'),
            'password' => Input::get('user_password'),
        ];

        $rules               = User::getRules(['email', 'password']);
        $rules['password'][] = 'required';

        // Validate the inputs.
        $validator = Validator::make($user_data, $rules);

        // Check if the form validates with success.
        if ($validator->passes()) {
            if (\Auth::attempt($user_data)) {
                return Redirect::intended('/')->with('success',
                    'Добро пожаловать, '.\Auth::user()->name."!");
            } else {

                return Redirect::to('login')->with('danger',
                    'Некорректная пара email/пароль')->withInput(Input::except('user_password'));
            }
        }

        $errors = $validator->messages()->toArray();

        foreach ($errors as $key => $val) {
            $validator->messages()->add('user_'.$key, $val[0]);
        }

        return Redirect::to('login')->withErrors($validator->messages())->withInput(Input::except('user_password'));
    }

    public function getLogout()
    {
        // Log out
        \Auth::logout();

        // Redirect to homepage
        return Redirect::to('/')->with('success',
            'Вы успешно вышли из своей учетной записи');
    }

    public function putSettings()
    {

        //die('postSettings');

        $only  = ['index_view'];
        $input = \Input::only($only);
        foreach ($input as $key => $val) {
            \Session::set($key, $val);
        }
        return \Response::json([
            'error'   => false,
            'entries' => []], 200
        );
    }

    public function getSettings()
    {
        return Redirect::to('/')->with('success', 'Вы успешно опознаны системой');
    }

}
