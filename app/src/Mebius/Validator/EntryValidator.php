<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Validator;

/**
 * Description of EntryValidator
 *
 * @author 1
 */
class EntryValidator extends AbstractValidator
{

    public static function getRules()
    {
        return [
          'client_name' => ['required', 'alpha_dash', 'max:200'],
          'client_phone' => ['required', 'alpha_dash', 'max:200'],
          'metro_id' => ['numeric'],
          'mo_town' => ['alpha_dash', 'max:200'],
          'title' => ['required', 'alpha_dash', 'max:200'],
          'descr' => ['required', 'alpha_dash', 'max:65000'],
          'call_time' => ['date'],
          'initial_visit' => ['date'],
          'details' => ['alpha_dash', 'max:65000'],
        ];
    }

//put your code here
}
