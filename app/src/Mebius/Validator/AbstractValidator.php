<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Validator;

use Illuminate\Validation\Factory as IlluminateValidator;

/**
 * Description of AbstractValidator
 *
 * @author 1
 */
abstract class AbstractValidator implements ValidatorInterface
{

    /**
     * @var Factory
     */
    protected $validator;
    protected $errors = [];
    protected static $rules = [];
    protected static $messages = [];

    public function __construct(IlluminateValidator $validator)
    {
        $this->validator = $validator;
    }

    public function validate(array $data, array $rules = array(),
        array $custom_errors = array())
    {
        if (empty($rules)/* && !empty($this->rules) && is_array($this->rules) */) {
            //no rules passed to function, use the default rules defined in sub-class
            //$rules = $this->rules;
            $rules = static::getRules();
        }

        //use Laravel's Validator and validate the data
        $validation = $this->validator->make($data, $rules, $custom_errors);

        if ($validation->fails()) {
            //echo 'validation failed';
            //validation failed, throw an exception
            //throw new ValidationException($validation->messages());
            $this->setErrors($validation->messages());
            return false;
        }
        //all good and shiny
        //echo 'validation OK';
        return true;
    }

    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

}
