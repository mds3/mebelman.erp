<?php

/*
 * Пока не используется
 */

namespace Mebius\Validator;

use \Illuminate\Validation\Validator as IlluminateValidator;

/**
 * Description of Validator
 *
 * @author 1
 */
class Validator extends IlluminateValidator
{

    protected function validateIn($attribute, $value, $parameters)
    {
        if (is_array($value)) {
            foreach ($value as $val) {
                if (!in_array($val, $parameters)) {
                    return false;
                }
            }
            return true;
        } else {
            return in_array((string) $value, $parameters);
        }
    }

}
