<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Validator;

/**
 *
 * @author 1
 */
interface ValidatorInterface
{

    public function validate(array $data, array $rules, array $err_msgs);

    public static function getRules();
}
