<?php

namespace Mebius\Domain;

/**
 * DataRoute
 *
 * Обертка над массивом данных для ссылок / кнопок
 *
 * @author topot
 */
final class DataRoute extends Generic
{

    /**
     * Ключ рута
     * @var string
     */
    public $key;

    /**
     * Метод
     * @var string
     */
    public $method;

    /**
     * url адрес
     * @var string
     */
    public $route;

    /**
     * Название ссылки / кнопки
     * @var string
     */
    public $name;

    /**
     * Сообщение. Для алерта например
     * @var string
     */
    public $msg;

    /**
     * Массив скрытых полей
     * @var array
     */
    public $hidden;

    /**
     *
     * @param string $key Ключ рута
     * @param string $method Метод
     * @param string $route url адрес
     * @param string $name Название ссылки / кнопки
     * @param string $msg Сообщение. Для алерта например
     * @param array $hidden Массив скрытых полей
     */
    public function __construct($key = null, $method = null, $route = null,
        $name = null, $hidden = null, $msg = null)
    {
        $this->key = $key;
        $this->method = $method;
        $this->route = $route;
        $this->name = $name;
        $this->msg = $msg;
        $this->hidden = $hidden;
    }

}
