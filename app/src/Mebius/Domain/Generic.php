<?php

namespace Mebius\Domain;

/**
 * Базовая сущность о открытыми свойствами,
 * но с проверяеммыми сеттерами на существование свойства.
 * Работают все базовые сеттеры и геттеры.
 *
 */
abstract class Generic
{

    /**
     * Базовый сеттер
     * @param string $name
     * @param string $value
     * @return boolean set or not
     */
    public function __set($name, $value)
    {
        if (isset($this->{$name}) || is_null($this->{$name})) {
            $this->{$name} = $value;
            return true;
        }
        return false;
    }

    /**
     * Базовый геттер
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->{$name}) || is_null($this->{$name})) {
            return $this->{$name};
        }
        return null;
    }

    public function __isset($name)
    {
        return isset($this->{$name});
    }

    public function __call($name, $arg)
    {
        if (method_exists($this, $name)) {
            call_user_func([$this, $name], $arg[0]);
        }
        $mtch = array();
        if (preg_match("~^(get|set)(.*)~i", $name, $mtch)) {
            return $this->handleGettersAndSetters($mtch, $arg);
        }
        return false;
    }

    private function handleGettersAndSetters($mtch, $arg)
    {
        $var = strtolower($mtch[2]);
        switch ($mtch[1]) {
            case 'get':
                //$arg[0] = isset($arg[0]) ? $arg[0] : null;
                return call_user_func([$this, '__get'], $var);
            case 'set':
                if (isset($arg[1])) {
                    return call_user_func([$this, '__set'], $var, $arg[0]);
                } else {
                    return call_user_func([$this, '__set'], $var, $arg[0]);
                }
            default:
                return false;
        }
    }

}
