<?php

namespace Mebius\Domain;

/**
 * DataObject
 *
 * Обертка над массивом данных для передачи контроллеру
 * Открыт для расширения
 *
 * @author topot
 */
class DataObject extends Generic
{

    /**
     * Заголовок всего пака / страницы
     * @var string
     */
    protected $title;

    /**
     * Массив опций
     * @var array
     */
    protected $options;

    /**
     * Коллекция рутов/кнопок
     * @var array
     */
    protected $routes = [];

    /**
     * Добавляет DataRoute в коллекцию
     * @param DataRoute $route
     */
    public function addRoute(DataRoute $route)
    {
        $this->routes[$route->getKey()] = $route;
    }

    /**
     * Возвращает DataRoute по ключу
     * @param string $key ключ рута
     * @return DataRoute|null
     */
    public function getRoute($key)
    {
        if (isset($this->routes[$key])) {
            return $this->routes[$key];
        }
        return null;
    }

}
