<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Domain;

/**
 * Description of ExportData
 *
 * @author 1
 */
class ExportData
{

    private $category;
    private $title;
    private $author;
    private $head = [];
    private $entries = [];

    public function __construct(array $data = [])
    {
        foreach ($this as $key => $val) {
            if (isset($data[$key])) {
                $this->{'set' . $key}($data[$key]);
            }
        }
        //die();
        //echo '<pre>', print_r($this), '</pre>';
    }

    function getCategory()
    {
        return $this->category;
    }

    function getTitle()
    {
        return $this->title;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function getHead()
    {
        return $this->head;
    }

    function getEntries()
    {
        return $this->entries;
    }

    function setCategory($category)
    {
        $this->category = $category;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function setAuthor($author)
    {
        $this->author = $author;
    }

    function setHead(array $head)
    {
        $this->head = $head;
    }

    function setEntries(array $entries)
    {
        $this->entries = $entries;
    }

}
