<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Model;

use \Mebius\Model\Contracts\IconedInterface;

/**
 * Description of IconedModel
 *
 * @author 1
 */
abstract class IconedModel extends ValidatableModel implements IconedInterface
{

    public function getIcon()
    {
        //die('<h1>getCondition: ' . $this->getCondition() . '</h1>');
        if (array_key_exists($this->getCondition(), $this->getIconsDict())) {
            return $this->getIconsDict()[$this->getCondition()];
        }
        return $this->getDefaultIcon();
    }

    public static function getDefaultIcon()
    {
        return 'plus-sign';
        //return array_shift($this->getIconsDict());
    }

}
