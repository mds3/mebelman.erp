<?php

namespace Mebius\Model;

use \App;
use \Illuminate\Support\MessageBag;
use \Illuminate\Validation\Factory;
use \Illuminate\Validation\Validator;

abstract class ValidatableModel extends BaseModel
{

    /**
     * Error message bag
     *
     * @var MessageBagted $errors;

      /**
     * Validation rules
     *
     * @var Array
     */
    protected static $rules = [];

    /**
     * Custom messages
     *
     * @var Array
     */
    protected static $messages = [];

    /**
     * Validator instance
     *
     * @var Factory
     */
    protected $validator;

    public function __construct(array $attributes = array(),
        Validator $validator = null)
    {
        parent::__construct($attributes);

        //$this->validator = $validator ? : \App::make('Mebius\Validator\Validator');
        //$this->validator = $validator ? : new \Mebius\Validator\Validator;
        //$this->validator = $validator ? : \App::make('\Mebius\Validator\Validator');
        $this->validator = $validator ? : App::make('validator');
    }

    /**
     * Listen for save event
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            //return $model->validate();
            if (!$model->validate()) {
                return false;
            }
        });

        static::updating(function($model) {
            //return $model->validate();
            if (!$model->validate()) {
                //echo '<pre>', print_r($model->getErrors()), '</pre>';
                return false;
            }
        });

        /* static::saving(function($model) {
          //return $model->validate();
          if (!$model->validate()) {
          return false;
          }
          }); */
    }

    /**
     *
     * @return boolean
     */
    public function validate()
    {

        /* echo '<pre>', print_r($this->attributes), '</pre>';
          echo '<pre>', print_r(static::$rules), '</pre>';
          echo $this->getEventDispatcher()->firing();
          echo '<hr/>'; */
        //echo 'Event: ' . $this->getEventDispatcher()->firing() . '<br/>';
        //die();

        /* @var $v Validator */
        $v = $this->validator->make($this->attributes, static::$rules,
            static::$messages);

        //echo '<pre>', print_r($this->attributes), '</pre>';
        //die();

        if ($v->passes()) {
            //echo '<pre>', print_r($v->getRules()), '</pre>';
            //die();
            return true;
        }

        $this->setErrors($v->messages());

        //echo '<pre>', print_r($v->messages()), '</pre>';
        //die();

        return false;
    }

    /**
     *
     * @param MessageBag $errors
     */
    protected function setErrors(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    /**
     *
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Return model is valid or not
     * @return bool
     */
    public function isValid()
    {
        return empty($this->errors);
    }

    /**
     *
     * @param array $only
     * @return array
     */
    public static function getRules(array $only = [])
    {
        if (empty($only)) {
            return static::$rules;
        }
        $ret = [];
        foreach ($only as $key) {
            if (static::ruleExists($key)) {
                $ret[$key] = static::$rules[$key];
            }
        }
        return $ret;
    }

    public static function ruleExists($key)
    {
        return array_key_exists($key, static::$rules);
    }

}
