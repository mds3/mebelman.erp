<?php

namespace Mebius\Model\Contracts;

interface OrderInterface
{

    /**
     * @return string формат времени/даты
     */
    public static function getDateTimeFormat();

    /**
     * @return string формат времени/даты
     */
    public static function getOnlyDateFormat();

    /**
     * Возвращает полный массив типов отображения списка заявок
     */
    public function getTypeOptions();

    /**
     * Возвращает полный массив периодов отображения списка заявок
     */
    public function getPeriodOptions($type);

    /**
     * Возвращает массив сортировок отображения списка заявок
     */
    public function getOrderByOptions();

    /**
     * Возвращает полный массив типов для поиска
     */
    public function getSearchOptions();

    /**
     * Устанавливает время архивирования
     */
    public function setArchivedAt($timestamp);
}
