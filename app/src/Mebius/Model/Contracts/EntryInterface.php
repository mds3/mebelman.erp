<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Model\Contracts;

/**
 *
 * @author 1
 */
interface EntryInterface
{

    /**
     * Устанавливает значение времени звонка в текущее время
     * @return bool Удалось установить или нет
     */
    public function setCallTime();

    /**
     * @return string формат времени/даты
     */
    public static function getDateTimeFormat();

    /**
     * Возвращает полный массив типов отображения списка заявок
     */
    public function getTypeOptions();

    /**
     * Возвращает полный массив периодов отображения списка заявок
     */
    public function getPeriodOptions($type);

    /**
     * Возвращает массив сортировок отображения списка заявок
     */
    public function getOrderByOptions();
}
