<?php

namespace Mebius\Model\Contracts;

/**
 *
 * @author 1
 */
interface IconedInterface
{

    /**
     * @return array словарь состояние - иконка
     */
    public static function getIconsDict();

    /**
     * @return string иконка по умолчанию
     */
    public static function getDefaultIcon();

    /**
     * @return string состояние/статус
     */
    public function getCondition();
}
