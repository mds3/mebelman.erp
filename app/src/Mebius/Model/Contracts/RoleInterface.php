<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Model\Contracts;

/**
 *
 * @author 1
 */
interface RoleInterface
{

    public function users();

    public function permissions();
}
