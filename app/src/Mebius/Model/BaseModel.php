<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Description of BaseModel
 *
 * @author 1
 */
abstract class BaseModel extends Eloquent
{

    /**
     * Ключ поля Имя
     * @var string
     */
    protected $nameKey = 'name';

    /**
     * Поля сокращенной выборки
     * @var array
     */
    protected $shortStatement = ['*'];

    /**
     * Логировать изменения модели или нет
     * @var bool
     */
    protected $isLoggable = false;

    /**
     * Поля, изменения которых не будут писаться в логи
     * @var array
     */
    protected $noLogFields = [];

    /**
     * Массив имен функций
     * для обработки значения при записи в лог
     * формат: ключ => callback
     * @var array
     */
    protected $logCallbacks = [];

    /**
     * Сопоставление отношений по ключам полей
     * @var array
     */
    protected $relationsKeys = [];

    /**
     * Возвращает имя колбэка для поля
     * или false если не найден
     * @param string $key
     * @return string|false
     */
    public function getLogFieldCallback($key)
    {
        if (isset($this->logCallbacks[$key])) {
            return $this->logCallbacks[$key];
        }
        return false;
    }

    /**
     * Возвращает список (словарь) текущей модели
     * @return array
     */
    public function getList()
    {
        return static::all()
                ->keyBy($this->getKeyName())
                ->transform(function($col) {
                    return $col->{$this->getNameKey()};
                })
                ->toArray();
    }

    /**
     *
     * @param string $def
     * @return array
     */
    public function getSelectableList($def = '-выбрать-')
    {
        $list = static::getList();
        $list[0] = $def;
        ksort($list);
        return $list;
    }

    /**
     *
     * @return array
     */
    public function getKeys()
    {
        return static::all()->keyBy($this->getKeyName())->keys();
    }

    /**
     * Возвращает ключ поля Имя
     * @return string
     */
    public function getNameKey()
    {
        return $this->nameKey;
    }

    /**
     * Возвращает значение поля Имя
     * @return mixed
     */
    public function getNameValue()
    {
        return $this->{$this->getNameKey()};
    }

    /**
     * Возвращает массив полей сокращенной выборки
     * @return array
     */
    public function getShortStatement()
    {
        return $this->shortStatement;
    }

    /**
     * Возвращает флаг, логировать изменения модели или нет
     * @return bool
     */
    public function isLoggable()
    {
        return $this->isLoggable;
    }

    /**
     * Возвращает массив полей, изменения которых не нужно писать в лог
     * @return array
     */
    public function getNoLogFields()
    {
        return array_merge($this->noLogFields, $this->getDates());
    }

    /**
     *
     * @param string $field
     * @return boolean
     */
    public function getRelationNameByField($field)
    {
        if (isset($this->relationsKeys[$field])) {
            return $this->relationsKeys[$field];
        }
        return false;
    }

    public static function getCorrectDateString($date_str = null)
    {
        if (is_null($date_str)) {
            return \date(static::getOnlyDateFormat());
        }
        return \date(static::getOnlyDateFormat(), \strtotime($date_str));
    }

    /**
     *
     * @return string
     */
    public static function getCurrencyAttribute()
    {
        return 'руб.';
    }

    /**
     *
     * @return string
     */
    public static function getDateTimeFormat()
    {
        return 'Y-m-d H:i:s';
    }

    /**
     *
     * @return string
     */
    public static function getOnlyDateFormat()
    {
        return 'Y-m-d';
    }

    /**
     *
     * @return string
     */
    public static function getDefaultDateTimeString()
    {
        return '0000-00-00 00:00:00';
    }

    /**
     *
     * @return string
     */
    public static function getDefaultDateString()
    {
        return '0000-00-00';
    }

    /**
     *
     * @return array
     */
    public function getOrderByOptions()
    {
        return [
          'desc' => [
            'name' => 'По убыванию',
            'conditions' => [
              'orderBy' => [
                ['id', 'DESC'],
              ],
            ]
          ],
          'asc' => [
            'name' => 'По возрастанию',
            'conditions' => [
              'orderBy' => [
                ['id', 'ASC'],
              ],
            ]
          ],
        ];
    }

}
