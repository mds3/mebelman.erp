<?php

namespace Mebius\Model;

use \Illuminate\Support\ServiceProvider;
use \Role;

class ModelServiceProvider extends ServiceProvider
{

    public function register()
    {
        //  binding interface HolidayRepository to the concrete implementation of EloquentHolidayRepository
        /* $this->app->bind(
          '\\Mebius\\Repository\\EntryRepository',
          function() {
          return new EntryEloquentRepository(new Entry());
          }); */
        $this->app->bind(
            '\\Mebius\\Model\\Contracts\\RoleInterface',
            function() {
            return new Role();
        });
    }

}
