<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Model;

/**
 * Description of OptionedModel
 *
 * @author 1
 */
abstract class OptionedModel extends IconedModel
{

    abstract public function getTypeDateField($type);

    public function getPeriodOptions($type = 'all')
    {

        //echo 'type: ' . $type;
        $fld = $this->getTypeDateField($type);
        return [
          'today' => [
            'name' => 'Сегодня',
            'conditions' => [
              'whereRaw' => [
                ['DATE(' . $fld . ') = ?', [date(static::getOnlyDateFormat())], 'and'],
              ],
            ]
          ],
          'tomorrow' => [
            'name' => 'Завтра',
            'conditions' => [
              'whereRaw' => [
                ['DATE(' . $fld . ') = ?', [static::getCorrectDateString('tomorrow')], 'and'],
              ],
            ]
          ],
          'yesterday' => [
            'name' => 'Вчера',
            'conditions' => [
              'whereRaw' => [
                ['DATE(' . $fld . ') = ?', [static::getCorrectDateString('yesterday')], 'and'],
              ],
            ]
          ],
          'week' => [
            'name' => 'На этой неделе',
            'conditions' => [
              'whereRaw' => [
                ['WEEK(`' . $fld . '`,1) = ?', [(int) date("W")], 'and'],
              ],
            ]
          ],
          'last_week' => [
            'name' => 'За прошлую неделю',
            'conditions' => [
              'whereRaw' => [
                ['WEEK(`' . $fld . '`,1) = ?', [((int) date("W") - 1 )], 'and'],
              ],
            ]
          ],
          'month' => [
            'name' => 'В этом месяце',
            'conditions' => [
              'whereMonth' => [
                [ $fld, '=', (int) date('m')],
              ],
              'whereYear' => [
                [ $fld, '=', (int) date('Y')],
              ],
            ]
          ],
          'last_month' => [
            'name' => 'За прошлый месяц',
            'conditions' => [
              'whereMonth' => [
                [ $fld, '=', (int) date('m', strtotime('-1 month'))],
              ],
              'whereYear' => [
                [ $fld, '=', (int) date('Y', strtotime('-1 month'))],
              ],
            ]
          ],
          'all' => [
            'name' => 'За всё время',
            'conditions' => []
          ],
        ];
    }

}
