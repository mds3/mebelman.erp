<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mebius\Filter;

use \Auth;
use \Mebius\Model\BaseModel;
use \Redirect;

/**
 * Description of AclPermittedFilter
 *
 * @author 1
 */
class AclPermittedFilter
{

    /**
     * Массив унификаций
     * @var array
     */
    private static $duos = [
      'store' => 'create',
      'update' => 'edit',
    ];

    /**
     *
     * @param string $route
     * @param type $request
     * @return type
     */
    public function filter($route, $request)
    {

        $user = Auth::user();

        // суперадмин
        if ($user->id == 1) {
            // может всё
            return;
        }
        // Загружаем пользовательские допуски
        $user->load('role.permissions');
        // Унифицируем route
        $uni_route = strtr($route->getName(), static::$duos);
        // $uni_route допустим
        if ($this->isAllowedRoute($uni_route)) {
            return;
        }

        // У вас нет прав на выполнение этого действия.
        return Redirect::route('main')->with('danger',
                trans('msg.route_not_allowed'));
    }

    private function isAllowedRoute($route_name)
    {
        foreach (Auth::user()->roles as $role) {
            foreach ($role->permissions as $permission) {
                $_route = explode(":", $permission->route);
                if ($_route[0] == $route_name) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function isAllowedAction($route_name, BaseModel $model)
    {
        return true;
    }

}
