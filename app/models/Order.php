<?php

use Illuminate\Database\Eloquent\Relations\HasMany;
use Mebius\Model\Contracts\OrderInterface;
use Mebius\Model\OptionedModel;

class Order extends OptionedModel implements OrderInterface
{

    protected $table = 'orders';
    protected $primaryKey = 'order_key';

    /**
     *
     */
    const ORDER_KEY = 'order_key';
    const FINISH_DATE = 'finish_date';
    const FINISHED_TIME = 'finished_time';
    const ARCHIVED_AT = 'archived_at';

    /**
     *
     */
    const STATUS_ACTIVE = 'CREATED';
    const STATUS_FINISHED = 'FINISHED';
    const STATUS_ARCHIVED = 'ARCHIVED';

    /**
     *
     */
    const TYPE_DATE = 'date';
    const TYPE_STRING = 'string';
    const TYPE_PHONE = 'phone';

    /* protected $STATUSES = [
      'FINISHED' => 'FINISHED',
      ]; */

    protected $isLoggable = true;
    protected $noLogFields = [
      Order::FINISHED_TIME,
      'updated_by',
    ];
    protected $relationsKeys = [
      'user_id' => 'master',
      'refer_id' => 'refer',
      'supplier_id' => 'supplier',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
      'refer_id',
      'created_by',
      'updated_by',
    ];

    /**
     *
     * @var array
     */
    protected $guarded = [];

    /**
     *
     * @var array
     */
    protected $fillable = [
      'order_key',
      'title',
      'client_name',
      'client_phone',
      'metro',
      'mo_town',
      'descr',
      'details',
      'supplier_id',
      'refer_id',
      'cashless',
      'inplace',
      'cost',
      'prepay',
      'result_amount',
      'cashbox',
      'comment',
      'docs_signed',
      Order::FINISH_DATE,
      'created_by', // чтоб можно было мастера менять
    ];

    /* Array

      [photo] => Array
      (
      [0] =>
      )

      ) */

    /**
     *
     * @var array
     */
    protected static $rules = [
      'order_key' => ['required', 'regex:/^\w\-\d{5,6}\-\d+$/ui'],
      'title' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:100'],
      'client_name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:200'],
      'client_phone' => ['required', 'regex:/^[\d\+\-\(\) ]{11,18}$/ui'],
      'metro' => ['regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:40'],
      'mo_town' => ['regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:50'],
      'descr' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
      'details' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
      'supplier_id' => ['numeric', 'exists:users,id'],
      'refer_id' => ['numeric', 'exists:refers,id'],
      'cashless' => ['integer', 'in:0,1'],
      'inplace' => ['integer', 'in:0,1'],
      'docs_signed' => ['integer', 'in:0,1'],
      'cost' => ['required', 'numeric'],
      'prepay' => ['numeric'],
      'cashbox' => ['numeric'],
      'result_amount' => ['numeric'],
      'comment' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
      Order::FINISH_DATE => ['required', 'date'],
    ];

    /**
     * Поля сокращенной выборки
     * @var array
     */
    protected $shortStatement = [
      'order_key',
      'title',
      'client_name',
      'client_phone',
      'metro',
      'mo_town',
      'descr',
      'details',
      'supplier_id',
      'refer_id',
      'cashless',
      'inplace',
      'cost',
      'prepay',
      'result_amount',
      'cashbox',
      'comment',
      'docs_signed',
      Order::FINISH_DATE,
      'status',
      'created_by',
    ];

    protected static function boot()
    {
        static::creating(function($order) {
            static::$rules['order_key'][] = 'unique:orders';
        });

        static::updating(function($order) {

        });

        parent::boot();

        static::creating(function($order) {
            //echo '<pre>', print_r($order->toArray()), '</pre>';
            //die();
            $order->created_by = Auth::user()->id;
            $order->updated_by = Auth::user()->id;
        });

        static::updating(function($order) {
            $order->updated_by = Auth::user()->id;
        });
    }

    public function master()
    {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function supplier()
    {
        return $this->hasOne('User', 'id', 'supplier_id');
    }

    public function operator()
    {
        return $this->hasOne('User', 'id', 'operator_id');
    }

    public function updater()
    {
        return $this->hasOne('User', 'id', 'updated_by');
    }

    public function refer()
    {
        return $this->hasOne('Refer', 'id', 'refer_id');
    }

    /**
     *
     * @return HasMany
     */
    public function stuff()
    {
        return $this->hasMany('Stuff', 'order_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany('Payment', 'order_id', 'id');
    }

    public function setMasterId($id)
    {
        $this->attributes['created_by'] = $id;
    }

    public function setClientPhoneAttribute($phone)
    {
        $this->attributes['client_phone'] = sanitize_phone($phone);
    }

    public function setDescrAttribute($descr)
    {
        //echo '<h1>SETTED</h1>';
        $this->attributes['descr'] = trim($descr);
    }

    public function getIsFinishedAttribute()
    {
        return isset($this->attributes['finished_time']) && strtotime($this->attributes['finished_time']) > 0;
    }

    public function getIsCanceledAttribute()
    {
        return isset($this->attributes['canceled_at']) ? !is_null($this->attributes['canceled_at'])
                : null;
    }

    public function getIsArchivedAttribute()
    {
        return $this->attributes['status'] == self::STATUS_ARCHIVED;
    }

    public function getIsActiveAttribute()
    {
        return $this->attributes['status'] == self::STATUS_ACTIVE;
    }

    public function getMasterIssetAttribute()
    {
        if ($this->exists) {
            return !is_null($this->master);
        }
    }

    public function getMasterNameAttribute()
    {
        if ($this->master_isset) {
            return $this->master->name;
        }
    }

    public function getIsOverdueAttribute()
    {
        return $this->is_active && date('Ymd',
                strtotime($this->getAttribute(Order::FINISH_DATE))) < date('Ymd');
    }

    public function getOrderKeyPrintAttribute()
    {
        return preg_replace('/(\d\d\d\d)(\d\d)(\-)(\d+)/',
            "<b>$1</b>$2$3<b>$4</b>", $this->order_key);
    }

    public function getCanBeClosedAttribute()
    {
        return $this->is_finished;
    }

    /**
     * Добавление суммы в кассу
     * @param int $int добавляемая сумма в кассу
     */
    public function addToCashBox($int)
    {
        $this->attributes['cashbox'] += $int;
    }

    /**
     *
     * @param int $timestamp
     */
    public function setArchivedAt($timestamp = false)
    {
        if ($timestamp === false) {
            $timestamp = time();
        }
        if ($this->exists) {
            $this->archived_at = date(static::getDateTimeFormat(), $timestamp);
        }
    }

    /**
     * Вычисляемое значение района заказа
     * @return int
     */
    public function getLocationAttribute()
    {
        $loc = '';
        if ($this->metro) {
            $loc .= 'м.' . $this->metro . ' ';
        }
        if ($this->mo_town) {
            $loc .= 'г.' . $this->mo_town;
        }
        if (!$loc) {
            $loc = '-';
        }
        return $loc;
    }

    /**
     * Вычисляемое значение долга мастера в кассу
     * (полученные, но не сданные деньги)
     * @return int
     */
    public function getCashboxMasterDebtAttribute()
    {
        return $this->prepay + $this->result_amount - $this->cashbox;
    }

    /**
     * Вычисляемое значение долга в кассу
     * @return int
     */
    public function getCashboxDebtAttribute()
    {
        //echo 'getCashboxDebtAttribute ' . ($this->cost - $this->cashbox);
        return $this->cost - $this->cashbox;
    }

    /**
     * Имеется-ли долг в кассу
     * @return boolean
     */
    public function getHasCashboxDebtAttribute()
    {
        return $this->cashbox_debt > 0;
    }

    /**
     * Вычисляемое значение долга клиента
     * @return int
     */
    public function getDebtAttribute()
    {
        return $this->cashbox_debt;
    }

    /**
     * Вычисляемое значение долга клиента
     * @return int
     */
    public function getClientDebtAttribute()
    {
        if ($this->is_cashless) {
            return $this->cashbox_debt;
        }
        return $this->cost - $this->prepay - $this->result_amount;
    }

    /**
     * Имеется-ли долг клиента
     * @return boolean
     */
    public function getHasClientDebtAttribute()
    {
        return $this->client_debt > 0;
    }

    /**
     * Имеется-ли долг клиента
     * @return boolean
     */
    public function getHasDebtAttribute()
    {
        return $this->debt > 0;
        //return ($this->has_client_debt || $this->cashbox_master_debt);
    }

    public function getIsCashlessAttribute()
    {
        //echo 'cashless ' . $this->attributes['cashless'];
        return $this->attributes['cashless'] > 0;
    }

    /* public function scopeOfMaster($query, $id)
      {
      return $query->whereCreatedBy($id);
      } */

    public function getTypeDateField($type)
    {
        switch ($type) {
            case 'new':
                return Order::CREATED_AT;
            case 'finish':
                return Order::FINISH_DATE;
            case 'finished':
                return Order::FINISHED_TIME;
            case 'overdue':
                return Order::FINISH_DATE;
            case 'refused':
                return Order::REFUSE_TIME;
            case 'archived':
                return Order::ARCHIVED_AT;
            default:
                return Order::CREATED_AT;
        }
    }

    public function getTypeOptions()
    {
        //$defaultTime = static::getDefaultDateTimeString();

        return [
          'all' => [
            'name' => 'Все',
            'conditions' => [
              'where' => [
                [ 'status', '<>', static::STATUS_ARCHIVED],
              ],
            ],
            'cells' => [
              Order::FINISH_DATE => 'human_date',
            ],
          ],
          'finish' => [
            'name' => 'Завершающиеся',
            'conditions' => [
              'where' => [
                //['refuse_time', '=', $defaultTime],
                [ 'status', '=', self::STATUS_ACTIVE],
              ],
            ],
            'cells' => [
              Order::FINISH_DATE => 'human_date',
            ],
          ],
          'overdue' => [
            'name' => trans('order.overdue'),
            'conditions' => [
              'where' => [
                [Order:: FINISH_DATE, '<', date(static::getOnlyDateFormat())],
                [ 'status', '=', Order::STATUS_ACTIVE],
              ],
            ],
            'cells' => [
              Order::FINISH_DATE => 'human_date',
            ],
          ],
          'finished' => [
            'name' => 'Завершенные',
            'conditions' => [
              'where' => [
                [ 'status', '=', self::STATUS_FINISHED],
              ],
            ],
            'cells' => [
              'finished_time' => 'human_date_time',
            ],
          ],
          'debt' => [
            'name' => 'Долговые',
            'conditions' => [
              'whereRaw' => [
                [ '`cashbox` < (`result_amount` + `prepay`)', []],
              ],
              'where' => [
                [ 'status', '<>', static::STATUS_ARCHIVED],
              ],
            ],
            'cells' => [
              'cashbox_master_debt' => 'with:currency',
            //'cashbox' => '',
            //'prepay' => '',
            //'result_amount' => '',
            ],
          ],
          'archived' => [
            'name' => 'Архив',
            'conditions' => [
              'where' => [
                [ 'status', '=', static::STATUS_ARCHIVED],
              ],
            ],
            'cells' => [
              'archived_at' => 'human_date_time',
            ],
          ],
        ];
    }

    /**
     *
     * @param string $type
     * @return array
     */
    public function getTypeCells($type)
    {
        return array_get(static:: getTypeOptions(), $type . '.cells', []);
    }

    public function getSearchOptions(array $input = [])
    {
        return [
          'order_key' => [
            'name' => 'Номер заказа',
            'type' => 'order_key',
            'conditions' => [
              'where' => [
                [ 'order_key', '=', isset($input['order_key']) ? $input['order_key']
                          : '?'],
              ],
            ]
          ],
          self::FINISH_DATE => [
            'name' => 'Дата завершения',
            'type' => self::TYPE_DATE,
            'conditions' => [
              /* 'where' => [
                ['status', '<>', self::STATUS_FINISHED],
                ], */
              'whereRaw' => [
                ['DATE(' . self::FINISH_DATE . ') = ?', isset($input[self::FINISH_DATE])
                          ? [static::getCorrectDateString($input[self::FINISH_DATE])]
                          : '?', 'and'],
              ],
            ]
          ],
          'client_name' => [
            'name' => 'Имя клиента',
            'type' => self::TYPE_STRING,
            'conditions' => [
              'where' => [
                [ 'client_name', 'LIKE', isset($input['client_name']) ? $input['client_name'] . '%'
                          : '?'],
              ],
            ]
          ],
          'client_phone' => [
            'name' => 'Телефон клиента',
            'type' => self::TYPE_PHONE,
            'conditions' => [
              'where' => [
                [ 'client_phone', '=', isset($input['client_phone']) ? sanitize_phone($input['client_phone'])
                          : '?'],
              ],
            ]
          ],
          'entry_time' => [
            'name' => 'Дата заявки',
            'type' => self::TYPE_DATE,
            'conditions' => [
              'whereRaw' => [
                [ '' . static::ORDER_KEY . ' LIKE ?', isset($input['entry_time'])
                          ? [static::getOrderKeyDatePartByDate($input['entry_time']) . '%']
                          : '?', 'and'],
              ],
            ]
          ],
          'cost' => [
            'name' => 'Сумма заказа',
            'type' => self::TYPE_STRING,
            'conditions' => [
              'where' => [
                [ 'cost', '=', isset($input['cost']) ? (int) $input['cost'] : '?'],
              ],
            ]
          ],
          'metro' => [
            'name' => 'Метро',
            'type' => 'metro',
            'conditions' => [
              'where' => [
                [ 'metro', '=', isset($input['metro']) ? $input['metro'] : '?'],
              ],
            ]
          ],
        ];
    }

    public static function getOrderKeyDatePartByDate($date)
    {
        return \date("dmy", \strtotime($date));
    }

    public function getCondition()
    {
        return $this->status;
    }

    public function getFinishStatusAttribute()
    {
        if ($this->is_archived) {
            if ($this->is_canceled) {
                return 'warning';
            }
            return 'success';
        } elseif (!$this->is_finished) {
            $point = date('Ymd', strtotime($this->finish_date));
            $now = date('Ymd');
            if ($point < $now) {
                return 'danger';
            } elseif ($point == $now) {
                return 'warning';
            } else {
                return 'success';
            }
        } else {
            if ($this->has_debt) {
                return 'warning';
            }
            return 'success';
        }
        return 'default';
    }

    public function getStatusAttribute()
    {
        if ($this->is_overdue) {
            return 'OVERDUE';
        } elseif ($this->is_archived && $this->is_canceled) {
            return 'CANCELED';
        } elseif ($this->is_finished && $this->has_debt) {
            return 'FINISHED_DEBT';
        }
        return $this->attributes['status'];
    }

    public static function getIconsDict()
    {
        return [
          'CREATED' => 'briefcase',
          //'REFUSED' => 'trash',
          'OVERDUE' => 'fire',
          'FINISHED' => 'ok-sign',
          'FINISHED_DEBT' => 'ok',
          'ARCHIVED' => 'compressed',
          'CANCELED' => 'ban-circle',
        ];
    }

}
