<?php

use \Mebius\Model\BaseModel;

/**
 * AuthAttempt - Попытка авторизации
 *
 * @author topot
 */
class AuthAttempt extends BaseModel
{

    /**
     *
     * @var string
     */
    protected $table = 'auth_attempts';

    /**
     *
     * @var array
     */
    protected $fillable = [
      'browser',
      'ip',
      'created_by',
    ];

    /**
     *
     * @var array
     */
    protected $guarded = [
      'created_at',
      'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Создает и возвращает новую запись об авторизации пользователя
     * @return static
     */
    public function addNewAttempt()
    {
        return $this->create([
              'created_by' => Auth::id(),
              'browser' => $_SERVER['HTTP_USER_AGENT'],
              'ip' => $_SERVER['REMOTE_ADDR'],
        ]);
    }

    /**
     * Создает и возвращает новую запись об авторизации пользователя
     * @return static
     */
    public function getLastAttempt()
    {
        return $this->firstByAttributes([
              'created_by' => Auth::id(),
        ]);
    }

}
