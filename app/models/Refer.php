<?php

use \Mebius\Model\ValidatableModel;

class Refer extends ValidatableModel
{

    protected $table = 'refers';
    protected $guarded = [
      "id",
      "created_at",
      "updated_at",
    ];
    protected $fillable = [
      "name",
      "descr",
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'between:1,255'],
      'descr' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'between:1,255'],
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($entry) {
            $entry->created_by = Auth::user()->id;
            $entry->updated_by = Auth::user()->id;
        });

        static::updating(function($entry) {
            $entry->updated_by = Auth::user()->id;
        });
    }

    public function entries()
    {
        return $this->hasMany('Entry');
    }

}
