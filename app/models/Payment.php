<?php

use \Mebius\Model\ValidatableModel;

class Payment extends ValidatableModel
{

    protected $table = 'payments';
    protected $guarded = [
      "id",
      "created_at",
      "updated_at",
    ];
    protected $fillable = [
      "order_id",
      "summ",
      "cash",
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'order_id' => ['required', 'numeric', 'exists:orders,id'],
      'summ' => ['required', 'numeric'],
      'cash' => ['required', 'in:Y,N'],
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($entry) {
            $entry->created_by = Auth::user()->id;
            $entry->updated_by = Auth::user()->id;
        });

        static::updating(function($entry) {
            $entry->updated_by = Auth::user()->id;
        });
    }

    public function order()
    {
        return $this->belongsTo('Order', 'order_id', 'id');
    }

}
