<?php

use \Illuminate\Auth\Reminders\RemindableInterface;
use \Illuminate\Auth\Reminders\RemindableTrait;
use \Illuminate\Auth\UserInterface;
use \Illuminate\Auth\UserTrait;
use \Mebius\Model\ValidatableModel;

class User extends ValidatableModel implements UserInterface, RemindableInterface
{

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
      'password',
      'remember_token',
      'api_key',
        //'role_id'
    ];

    /**
     *
     * @var array type
     */
    protected $guarded = ['id'];
    //protected $visible = ['name', 'phone', 'email'];
    protected $fillable = [
      'name',
      'phone',
      'email',
      //'role_id',
      'role',
      'password',
    ];

    /*
     * `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
      `password` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
      `api_key` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
      `phone` VARCHAR(16) NOT NULL COLLATE 'utf8_unicode_ci',
      `email` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',
      `role_id` TINYINT(3) UNSIGNED NOT NULL,
     */

    /**
     *
     * @var array
     */
    protected static $rules = [
      'name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'between:4,100'],
      'password' => ['alpha_dash', 'between:4,100'],
      'phone' => ['required', 'regex:/^[\d\+\-\(\) ]{11,18}$/ui'],
      'email' => ['required', 'email', 'max:200'],
        //'role_id' => ['required', 'exists:roles,id'],
    ];

    protected static function boot()
    {

        static::creating(function($model) {
            static::$rules['password'][] = 'required';
            static::$rules['email'][] = 'unique:users';
        });

        static::updating(function($model) {
            /* @var $model \Illuminate\Database\Eloquent\Model */
            if (!$model->getAttribute('password')) {
                //$model->original['password'] = '';
                $model->syncOriginalAttribute('password');
            }
            if ($model->getAttribute('email') != $model->getOriginal('email')) {
                static::$rules['email'][] = 'unique:users';
            }
        });


        parent::boot();

        //echo '<pre>', print_r(static::$rules['role_id']), '</pre>';

        /* static::saving(function($model) {
          if ($model->getAttribute('password')) {
          $model->password = Hash::make($model->password);
          }
          }); */
        static::creating(function($model) {
            $model->password = Hash::make($model->password);
            $model->created_by = \Auth::user()->id;
            $model->updated_by = \Auth::user()->id;
        });
        static::updating(function($model) {
            //echo 'firing a after parent::boot updating <br/>';
            /* @var $model ValidatableModel */
            if ($model->getAttribute('password')) {
                $model->password = Hash::make($model->password);
            }
            $model->updated_by = \Auth::user()->id;
        });
    }

    public function role()
    {
        return $this->hasOne('Role', 'id', 'role_id');
    }

    public function roles()
    {
        return $this->belongsToMany('Role');
    }

    public function authAttempts()
    {
        return $this->hasMany('AuthAttempt', 'created_by', 'id');
    }

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = sanitize_phone($phone);
    }

}
