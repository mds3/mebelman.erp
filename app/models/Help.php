<?php

use \Mebius\Model\BaseModel;
use \Mebius\Model\Contracts\HelpInterface;

class Help extends BaseModel implements HelpInterface
{

    public function get()
    {
        $filename = storage_path() . '/help.php';
        $sections = include($filename);
        return $sections;
    }

}
