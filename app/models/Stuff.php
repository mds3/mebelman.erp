<?php

use \Mebius\Model\ValidatableModel;

class Stuff extends ValidatableModel
{

    protected $table = 'order_stuff';
    protected $guarded = [
      "id",
      "created_at",
      "updated_at",
    ];
    protected $fillable = [
      "order_id",
      "name",
      "quantity",
      "cost",
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'order_id' => ['required'],
      'name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'between:2,255'],
      'quantity' => ['regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'between:1,255'],
      'cost' => ['numeric'],
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($entry) {
            $entry->created_by = Auth::user()->id;
            $entry->updated_by = Auth::user()->id;
        });

        static::updating(function($entry) {
            $entry->updated_by = Auth::user()->id;
        });
    }

    public function order()
    {
        return $this->belongsTo('Order');
    }

}
