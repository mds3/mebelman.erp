<?php

use \Illuminate\Database\Eloquent\Collection;
use \Mebius\Model\Contracts\RoleInterface;
use \Mebius\Model\ValidatableModel;

class Role extends ValidatableModel implements RoleInterface
{

    /**
     *
     */
    const SUPPLIER_ROLE_ID = 4;

    /**
     *
     * @var type
     */
    protected $table = 'roles';

    /**
     *
     * @var type
     */
    protected $guarded = [
      "id",
      "created_at",
      "updated_at",
    ];

    /**
     *
     * @var type
     */
    protected $fillable = [
      "name",
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'between:4,100'],
    ];

    protected static function boot()
    {
        //static::$rules['permission_id'] = ['in:' . implode(',', Permission::getKeys())];

        static::creating(function($model) {
            static::$rules['permission_id'] = ['in:' . implode(',',
                  Permission::getKeys())];
        });

        static::updating(function($model) {
            static::$rules['permission_id'] = ['in:' . implode(',',
                  Permission::getKeys())];
        });

        parent::boot();
    }

    public function users()
    {
        return $this->belongsToMany('User');
    }

    public function groups()
    {
        return $this->belongsToMany('Group');
    }

    public function permissions()
    {
        return $this->belongsToMany('Permission', 'role_permission');
    }

    public function makePermissionsStructList(Collection $perms)
    {
        $ret = [];
        if ($perms->count()) {
            $perms->sortBy(function($perm) {
                return $perm->route;
            });
            foreach ($perms as $perm) {
                $r = explode('.', $perm->route);
                $ret[$r[0]][$perm->id] = $perm->descr;
            }
        }
        //echo '<pre>', print_r($ret), '</pre>';
        return $ret;
    }

}
