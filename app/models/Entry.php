<?php

use Mebius\Model\Contracts\EntryInterface;
use Mebius\Model\OptionedModel;

class Entry extends OptionedModel implements EntryInterface
{

    protected $table = 'entries';

    /**
     *
     */
    const ORDER_KEY = 'order_key';
    const VISIT_TIME = 'visit_time';
    const DEFER_BEFORE = 'defer_before';

    /**
     *
     */
    const TYPE_DATE = 'date';
    const TYPE_STRING = 'string';

    /**
     * Statuses
     */
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_DEFERRED = 'DEFERRED';
    const STATUS_REFUSED = 'REFUSED';
    const STATUS_ARCHIVED = 'ARCHIVED';

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
      'user_id',
      'supplier_id',
      'refer_id',
      'created_by',
      'updated_by',
    ];

    /**
     *
     */
    protected $nameKey = 'order_key';

    /**
     *
     * @var array
     */
    protected $guarded = ['order_created'];

    /**
     *
     * @var array
     */
    /* protected $dates = [
      'visit_time',
      'view_time',
      'call_time'
      ]; */


    protected $isLoggable = true;
    protected $noLogFields = [
      'finish_time',
      'updated_by',
      'defer_time',
    ];
    protected $logCallbacks = [
      'visit_time' => 'human_date_time',
      'call_time' => 'human_date_time',
      'defer_time' => 'human_date_time',
      'view_time' => 'human_date_time',
      'refuse_time' => 'human_date_time',
      'defer_before' => 'human_date',
    ];
    protected $relationsKeys = [
      'user_id' => 'master',
      'refer_id' => 'refer',
      'supplier_id' => 'supplier',
    ];

    /**
     *
     * @var array
     */
    protected $fillable = [
      'title',
      'client_name',
      'client_phone',
      'metro',
      'mo_town',
      'descr',
      'user_id',
      'supplier_id',
      'refer_id',
      'visit_time',
      'details',
      'refuse_reason',
      'defer_before',
      'defer_reason',
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'title' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:100'],
      'client_name' => ['required', 'regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:200'],
      'client_phone' => ['required', 'regex:/^[\d\+\-\(\) ]{11,18}$/ui'],
      'metro' => ['regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:40'],
      'mo_town' => ['regex:/^[\pL\pZ\pS\pN\pP]+$/ui', 'max:50'],
      'descr' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
      //'user_id' => ['sometimes', 'exists:users,id'],
      'supplier_id' => ['numeric', 'exists:users,id'],
      'refer_id' => ['numeric', 'exists:refers,id'],
      //'view_time' => ['date'],
      //'call_time' => ['date'],
      'visit_time' => ['date'],
      //'refuse_time' => ['date'],
      'details' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
      'refuse_reason' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000', 'required_if:status,REFUSED'],
      'defer_reason' => ['regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000', 'required_if:status,DEFERRED'],
      'defer_before' => ['required_if:status,DEFERRED', 'date'],
    ];

    /**
     * Поля сокращенной выборки
     * @var array
     */
    protected $shortStatement = [
      'order_key',
      'title',
      'client_name',
      'client_phone',
      'metro',
      'mo_town',
      'descr',
      'user_id',
      'supplier_id',
      'refer_id',
      'view_time',
      'visit_time',
      'details',
      'status',
      'defer_time',
      'defer_before',
      'defer_reason',
      'refuse_time',
      'refuse_reason',
      'created_at',
      'created_by',
    ];

    protected static function boot()
    {
        static::updating(function($entry) {
            //static::$rules['visit_time'][] = 'after:' . date('d-m-Y');
            if (strtotime($entry->defer_before) <= 0) {
                unset($entry->defer_before);
            }
        });

        parent::boot();

        static::creating(function($entry) {
            $entry->order_key = static::makeOrderKey();
            $entry->created_by = Auth::user()->id;
            $entry->updated_by = Auth::user()->id;
        });

        static::updating(function($entry) {
            $entry->updated_by = Auth::user()->id;
        });
    }

    public function master()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function supplier()
    {
        return $this->hasOne('User', 'id', 'supplier_id');
    }

    public function operator()
    {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function updater()
    {
        return $this->hasOne('User', 'id', 'updated_by');
    }

    public function refer()
    {
        return $this->hasOne('Refer', 'id', 'refer_id');
    }

    public static function makeOrderKey()
    {
        //return date("dmy-") . (self::whereRaw('DATE(' . static::CREATED_AT . ') = ?',
        //array(date("Y-m-d")))->count() + 1);
        $dp = date("dmy-");
        $res = self::whereRaw('DATE(' . static::CREATED_AT . ') = ?',
                [date("Y-m-d")])->orderBy('id', 'DESC')->first([static::ORDER_KEY]);
        /* , strtotime('tomorrow') */
        if ($res) {
            $dp .= (explode($dp, $res->{static::ORDER_KEY})[1] + 1);
        } else {
            $dp .= '1';
        }
        $dp = "M-{$dp}";
        //die($dp);
        return $dp;
    }

    public function setOrderCreated($bool = true)
    {
        $this->attributes['order_created'] = (int) $bool;
    }

    public function setCallTime($timestamp = false)
    {
        if ($timestamp === false) {
            $timestamp = time();
        }
        if ($this->exists && !$this->is_called) {
            $this->call_time = date(static::getDateTimeFormat(), $timestamp);
        }
    }

    public function setArchivedAt($timestamp = false)
    {
        if ($timestamp === false) {
            $timestamp = time();
        }
        if ($this->exists) {
            $this->archived_at = date(static::getDateTimeFormat(), $timestamp);
        }
    }

    public function setStatus($status)
    {
        if ($this->exists) {
            switch ($status) {
                case 'refuse':
                    $this->status = self::STATUS_REFUSED;
                    break;
                case 'defer':
                    $this->status = self::STATUS_DEFERRED;
                    break;
                case 'active':
                    $this->status = self::STATUS_ACTIVE;
                    break;
                case 'archive':
                    $this->status = self::STATUS_ARCHIVED;
                    $this->setArchivedAt();
                    break;
                default:
                    break;
            }
        }
    }

    public function setRefuseTime($timestamp = false)
    {
        if ($this->exists) {
            if (!$this->is_refused) {
                if ($timestamp === false) {
                    $timestamp = time();
                }
                $this->refuse_time = date(static::getDateTimeFormat(),
                    $timestamp);
            } elseif ($timestamp === 0) {
                $this->refuse_time = static::getDefaultDateTimeString();
            }
        }
    }

    public function setDeferTime($timestamp = false)
    {
        if ($this->exists) {
            if (!$this->is_defered) {
                if ($timestamp === false) {
                    $timestamp = time();
                }
                $this->defer_time = date(static::getDateTimeFormat(), $timestamp);
            } elseif ($timestamp === 0) {
                $this->defer_time = static::getDefaultDateTimeString();
            }
        }
    }

    public function unsetEmptyDates()
    {
        if ($this->exists) {
            if (strtotime($this->visit_time) <= 0) {
                unset($this->visit_time);
            }
            if (strtotime($this->call_time) <= 0) {
                unset($this->call_time);
            }
        }
    }

    /**
     * Удаляет из текущего объекта даты выезда и звонка
     * чтоб не было конфликтов правил при сохранении
     */
    public function unsetMasterDates()
    {
        if ($this->exists) {
            unset($this->visit_time);
            unset($this->call_time);
        }
    }

    public function getIsNewAttribute()
    {
        if ($this->exists) {
            return (strtotime($this->view_time) <= 0);
        }
        return true;
    }

    public function getIsCallWaitAttribute()
    {
        if ($this->exists) {
            return (!$this->is_new && !$this->is_called);
        }
    }

    public function getIsCalledAttribute()
    {
        if ($this->exists) {
            return (strtotime($this->call_time) > 0);
        }
    }

    public function getIsVisitWaitAttribute()
    {
        if ($this->exists) {
            $visit_time = strtotime($this->visit_time);
            return date('Ymd', $visit_time) >= date('Ymd');
            //return strtotime($this->visit_time) > time();
        }
    }

    public function getIsVisitTimeWaitAttribute()
    {
        if ($this->exists) {
            return strtotime($this->visit_time) <= 0 && strtotime($this->call_time) > 0;
        }
    }

    public function getVisitTimeIssetAttribute()
    {
        if ($this->exists) {
            return strtotime($this->visit_time) > 0;
        }
    }

    public function getMasterIssetAttribute()
    {
        if ($this->exists) {
            return !is_null($this->master);
        }
    }

    public function getIsOverdueAttribute()
    {
        if ($this->exists) {
            $visit_time = strtotime($this->visit_time);
            return ($visit_time > 0 && date('Ymd', $visit_time) < date('Ymd'));
        }
    }

    public function getIsRefusedAttribute()
    {
        if ($this->exists) {
            //return (strtotime($this->refuse_time) > 0);
            return $this->attributes['status'] == static::STATUS_REFUSED;
        }
    }

    public function getWasRefusedAttribute()
    {
        if ($this->exists) {
            //return (strtotime($this->refuse_time) > 0);
            return 0 < strtotime($this->attributes['refuse_time']);
        }
    }

    public function getIsDeferredAttribute()
    {
        if ($this->exists) {
            //return (strtotime($this->defer_time) > 0);
            return $this->attributes['status'] == static::STATUS_DEFERRED;
        }
    }

    public function getIsOverdueDeferredAttribute()
    {
        if ($this->exists) {
            //return (strtotime($this->defer_time) > 0);
            return $this->attributes['status'] == static::STATUS_DEFERRED && date('Ymd',
                    \strtotime($this->attributes['defer_before'])) < date('Ymd');
        }
    }

    private function getDaysCount($date_str, $callback = 'floor')
    {
        return $callback((time() - strtotime($date_str)) / 86400);
    }

    public function getCallWaitDaysCountAttribute()
    {
        if ($this->is_call_wait) {
            return $this->getDaysCount($this->attributes['view_time'], 'ceil');
        }
    }

    public function getOverdueDeferredDaysCountAttribute()
    {
        if ($this->is_overdue_deferred) {
            return $this->getDaysCount($this->attributes['defer_before']);
        }
    }

    public function getOverdueDaysCountAttribute()
    {
        if ($this->is_overdue) {
            return $this->getDaysCount($this->attributes['visit_time'], 'ceil');
        }
    }

    public function getIsArchivedAttribute()
    {
        if ($this->exists) {
            //return (strtotime($this->defer_time) > 0);
            return $this->attributes['status'] == static::STATUS_ARCHIVED;
        }
    }

    public function getConditionAttribute()
    {
        if ($this->is_archived) {
            return 'archived';
        } elseif ($this->is_refused) {
            return 'refused';
        } elseif ($this->is_overdue_deferred) {
            return 'overdue_deferred';
        } elseif ($this->is_deferred) {
            return 'deferred';
        } elseif ($this->is_new) {
            return 'new';
        } elseif ($this->is_call_wait) {
            return 'call_wait';
        } elseif ($this->is_visit_time_wait) {
            return 'call_wait';
        } elseif ($this->is_visit_wait) {
            return 'visit_wait';
        } elseif ($this->is_overdue) {
            return 'overdue';
        } else {
            return 'overdue';
        }
    }

    public function getFinishStatusAttribute()
    {
        if ($this->is_archived) {
            if ($this->was_refused) {
                return 'warning';
            }
            return 'success';
        } elseif ($this->is_deferred) {
            if ($this->is_overdue_deferred) {
                return 'danger';
            }
            return 'warning';
        } else {
            if ($this->is_call_wait || is_null($this->master)) {
                return 'warning';
            } elseif ($this->is_refused || $this->is_overdue) {
                return 'danger';
            }
            return 'success';
        }
        return 'default';
    }

    public function getIsAllowedForOrderAttribute()
    {
        if ($this->exists) {
            return $this->is_called;
        }
    }

    public function getOrderKeyPrintAttribute()
    {
        return preg_replace('/(\d\d\d\d)(\d\d)(\-)(\d+)/',
            "<b>$1</b>$2$3<b>$4</b>", $this->order_key);
    }

    public function setClientPhoneAttribute($phone)
    {
        $this->attributes['client_phone'] = sanitize_phone($phone);
    }

    public function getTypeDateField($type)
    {
        switch ($type) {
            case 'new':
                return Entry::CREATED_AT;
            case 'call_wait':
                return Entry::CREATED_AT;
            case 'visit_wait':
                return Entry::VISIT_TIME;
            case 'overdue':
                return Entry::CREATED_AT;
            case 'overdue_deferred':
                return Entry::DEFER_BEFORE;
            case 'refused':
                return Entry::CREATED_AT;
            case 'deferred':
                return Entry::DEFER_BEFORE;
            default:
                return Entry::CREATED_AT;
        }
    }

    public function getTypeOptions()
    {
        $defaultTime = static::getDefaultDateTimeString();

        return [
          'all' => [
            'name' => trans('entry.all'),
            'permissions' => [
              'entry.index',
            ],
            'conditions' => [
              'where' => [
                ['status', '<>', static::STATUS_ARCHIVED],
              ],
            ],
            'cells' => [
              'visit_time' => 'human_date_time',
            ],
          ],
          'new' => [
            'name' => trans('entry.statuses.new'),
            'permissions' => [
              'entry.show',
            ],
            'conditions' => [
              'where' => [
                ['view_time', '=', $defaultTime],
                ['status', '=', static::STATUS_ACTIVE],
              ],
            ],
            'cells' => [
              'created_at' => 'human_date_time',
            ],
          ],
          'call_wait' => [
            'name' => trans('entry.statuses.call_wait'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['call_time', '=', $defaultTime],
                ['view_time', '>', $defaultTime],
                ['status', '=', static::STATUS_ACTIVE],
              ],
            ],
            'cells' => [
              'view_time' => 'human_date_time',
            ],
          ],
          'visit_wait' => [
            'name' => trans('entry.statuses.visit_wait'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['view_time', '>', $defaultTime],
                ['call_time', '>', $defaultTime],
                ['status', '=', static::STATUS_ACTIVE],
              ],
            ],
            'cells' => [
              'visit_time' => 'human_date_time',
            ],
          ],
          'deferred' => [
            'name' => trans('entry.statuses.deferred'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['status', '=', static::STATUS_DEFERRED],
              ],
            ],
            'cells' => [
              'defer_before' => 'human_date',
            ],
          ],
          'overdue' => [
            'name' => trans('entry.statuses.overdue'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['visit_time', '>', $defaultTime],
                //['visit_time', '<', date(self::getDateTimeFormat())],
                ['status', '=', static::STATUS_ACTIVE],
              ],
              'whereRaw' => [
                ['DATE(visit_time) < DATE(NOW())', [], 'and'],
              ],
            ],
            'cells' => [
              'visit_time' => 'human_date_time',
            ],
          ],
          'overdue_deferred' => [
            'name' => trans('entry.statuses.overdue_deferred'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['status', '=', static::STATUS_DEFERRED],
                ['defer_before', '<', date(self::getOnlyDateFormat())],
              ],
            ],
            'cells' => [
              'defer_before' => 'human_date_time',
            ],
          ],
          'refused' => [
            'name' => trans('entry.statuses.refused'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            ],
            'conditions' => [
              'where' => [
                ['status', '=', static::STATUS_REFUSED],
              ],
            ],
            'cells' => [
              'refuse_time' => 'human_date_time',
            ],
          ],
          'archived' => [
            'name' => trans('entry.statuses.archived'),
            'permissions' => [
              'entry.edit',
              'entry.status',
            //'entry.archiving',
            ],
            'conditions' => [
              'where' => [
                ['status', '=', static::STATUS_ARCHIVED],
              ],
            ],
            'cells' => [
              'archived_at' => 'human_date_time',
            ],
          ],
        ];
    }

    /**
     *
     * @param string $type
     * @return array
     */
    public function getTypeCells($type)
    {
        return array_get(static::getTypeOptions(), $type . '.cells', []);
    }

    public function getSearchOptions(array $input = [])
    {
        return [
          'order_key' => [
            'name' => 'Номер заявки',
            'type' => 'order_key',
            'conditions' => [
              'where' => [
                ['order_key', '=', isset($input['order_key']) ? $input['order_key']
                          : '?'],
              ],
            ]
          ],
          self::CREATED_AT => [
            'name' => 'Дата заявки',
            'type' => self::TYPE_DATE,
            'conditions' => [
              'whereRaw' => [
                ['DATE(' . self::CREATED_AT . ') = ?', isset($input[self::CREATED_AT])
                          ? [static::getCorrectDateString($input[self::CREATED_AT])]
                          : '?', 'and'],
              ],
            ]
          ],
          self::VISIT_TIME => [
            'name' => 'Дата выезда',
            'type' => self::TYPE_DATE,
            'conditions' => [
              /* 'where' => [
                ['status', '<>', self::STATUS_FINISHED],
                ], */
              'whereRaw' => [
                ['DATE(' . self::VISIT_TIME . ') = ?', isset($input[self::VISIT_TIME])
                          ? [static::getCorrectDateString($input[self::VISIT_TIME])]
                          : '?', 'and'],
              ],
            ]
          ],
          'client_name' => [
            'name' => 'Имя клиента',
            'type' => self::TYPE_STRING,
            'conditions' => [
              'where' => [
                ['client_name', 'LIKE', isset($input['client_name']) ? $input['client_name'] . '%'
                          : '?'],
              ],
            ]
          ],
          'client_phone' => [
            'name' => 'Телефон клиента',
            'type' => 'phone',
            'conditions' => [
              'where' => [
                ['client_phone', 'LIKE', isset($input['client_phone']) ? '%' . sanitize_phone($input['client_phone']) . '%'
                          : '?'],
              ],
            ]
          ],
          'metro' => [
            'name' => 'Метро',
            'type' => 'metro',
            'conditions' => [
              'where' => [
                ['metro', '=', isset($input['metro']) ? $input['metro'] : '?'],
              ],
            ]
          ],
        ];
    }

    public function getCondition()
    {
        return $this->condition;
    }

    public static function getIconsDict()
    {
        return [
          'new' => 'eye-close',
          'call_wait' => 'phone-alt',
          'visit_wait' => 'road', //plane
          'overdue' => 'fire',
          'overdue_deferred' => 'minus-sign',
          'refused' => 'remove',
          'deferred' => 'time',
          'archived' => 'compressed',
        ];
    }

}
