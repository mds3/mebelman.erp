<?php

use \Mebius\Model\ValidatableModel;

class EventsLog extends ValidatableModel
{

    protected $table = 'events_log';
    public $timestamps = false;
    protected $guarded = [
      "id",
      "created_at",
    ];
    protected $fillable = [
      "model",
      "model_id",
      "event",
      "items",
    ];

    /**
     *
     * @var array
     */
    protected static $rules = [
      'model' => ['required'],
      'model_id' => ['required', 'integer'],
      'event' => ['required', 'alpha'],
      'items' => ['required', 'regex:/[\pL\pZ\pS\pN\pP]+/ui', 'max:65000'],
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($entry) {
            $entry->created_by = Auth::user()->id;
            //$entry->updated_by = Auth::user()->id;
        });

        static::updating(function($entry) {
            //$entry->updated_by = Auth::user()->id;
        });
    }

    public function creator()
    {
        return $this->hasOne('User', 'id', 'created_by');
    }

    public function setItemsAttribute(array $items)
    {
        $this->attributes['items'] = serialize($items);
    }

    public function getItemsAttribute()
    {
        return unserialize($this->attributes['items']);
    }

}
