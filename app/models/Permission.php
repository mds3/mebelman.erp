<?php

class Permission extends Eloquent
{

    protected $table = 'permissions';
    protected $fillable = array(
      'route',
      'descr'
    );
    public $timestamps = false;

    public function roles()
    {
        return $this->belongsToMany('Role', 'role_permission');
    }

    public static function getKeys()
    {
        //echo '<pre>', print_r(self::all()->keyBy('id')->keys()), '</pre>';
        //die();
        return self::all()->keyBy('id')->keys();
    }

    /* public function getKey()
      {
      return $this->attributes['route'];
      } */
}
