<?php

Form::macro('deleteButton',
    function($route, $msg) {
    $form = Form::open(array(
          'route' => $route,
          'method' => 'DELETE',
          'onsubmit' => "return confirm(\"$msg\") ? true : false;"));
    $form .= Form::button('Удалить',
            array('type' => 'submit', 'class' => 'btn btn-default btn-sm',
          'style' => 'margin:4px 0 0 0;',));
    $form .= Form::close();
    return $form;
});

Form::macro('btnDelete',
    function($route, $msg) {
    $form = Form::open(array(
          'url' => $route,
          'method' => 'DELETE',
          'onsubmit' => "return confirm(\"$msg\") ? true : false;"));
    $form .= Form::button('Удалить',
            array('type' => 'submit', 'class' => 'btn btn-default btn-block',
          'style' => 'margin:4px 0 0 0;',));
    $form .= Form::close();
    return $form;
});


Form::macro('reanimateButton',
    function($route, $msg) {
    $form = Form::open(array(
          'url' => $route['route'],
          'method' => $route['method'],
          'onsubmit' => "return confirm(\"$msg\") ? true : false;"));
    $form .= Form::hidden('_status', 'reanimate');
    $form .= Form::button($route['name'],
            array('type' => 'submit', 'class' => 'btn btn-info btn-block',
          'style' => 'margin:4px 0 0 0;',));
    $form .= Form::close();
    return $form;
});

Form::macro('postButton',
    function($route, $msg, $class = 'primary') {
    $form = Form::open(array(
          'url' => $route['route'],
          'method' => isset($route['method']) ? $route['method'] : 'POST',
          'onsubmit' => "return confirm(\"$msg\") ? true : false;"));
    if (isset($route['hidden'])) {
        foreach ($route['hidden'] as $key => $val) {
            $form .= Form::hidden($key, $val);
        }
    }
    $form .= Form::button($route['name'],
            [
          'type' => 'submit',
          'class' => 'btn btn-block btn-' . $class,
          'style' => 'margin:4px 0 0 0;',
    ]);
    $form .= Form::close();
    return $form;
});
