<?php

return array(
  /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used
    | by the validator class. Some of the rules contain multiple versions,
    | such as the size (max, min, between) rules. These versions are used
    | for different input types such as strings and files.
    |
    | These language lines may be easily changed to provide custom error
    | messages in your application. Error messages for custom validation
    | rules may also be added to this file.
    |
   */

  "accepted" => "Вы должны принять :attribute.",
  "active_url" => "Поле :attribute должно быть полным URL.",
  "after" => "Поле :attribute должно быть датой после :date.",
  "alpha" => "Поле :attribute может содержать только буквы.",
  "alpha_dash" => "Поле :attribute может содержать только буквы, цифры и тире.",
  "alpha_num" => "Поле :attribute может содержать только буквы и цифры.",
  "before" => "Поле :attribute должно быть датой перед :date.",
  "between" => array(
    "numeric" => "Поле :attribute должно быть между :min и :max.",
    "file" => "Поле :attribute должно быть от :min до :max Килобайт.",
    "string" => "Поле :attribute должно быть от :min до :max символов.",
  ),
  "confirmed" => "Поле :attribute не совпадает с подтверждением.",
  "date" => "Значение поля :attribute должно быть датой.",
  "different" => "Поля :attribute и :other должны различаться.",
  "email" => "Поле :attribute имеет неверный формат.",
  "exists" => "Выбранное значение :attribute не существует.",
  "image" => "Поле :attribute должно быть картинкой.",
  "in" => "Выбранное значение для :attribute не верно.",
  "integer" => "Поле :attribute должно быть целым числом.",
  "ip" => "Поле :attribute должно быть полным IP-адресом.",
  "match" => "Поле :attribute имеет неверный формат.",
  "max" => array(
    "numeric" => "Поле :attribute должно быть меньше :max.",
    "file" => "Поле :attribute должно быть меньше :max Килобайт.",
    "string" => "Поле :attribute должно быть короче :max символов.",
  ),
  "mimes" => "Поле :attribute должно быть файлом одного из типов: :values.",
  "min" => array(
    "numeric" => "Поле :attribute должно быть не менее :min.",
    "file" => "Поле :attribute должно быть не менее :min Килобайт.",
    "string" => "Поле :attribute должно быть не короче :min символов.",
  ),
  "not_in" => "Выбранное значение :attribute не верно.",
  "numeric" => "Поле :attribute должно быть числом.",
  "regex" => "Поле :attribute содержит недопустимые символы или неверный формат.",
  "required" => "Поле :attribute обязательно для заполнения.",
  "required_if" => "Поле \":attribute\" обязательно для заполнения, когда значение поля :other = :value.",
  "same" => "Значение :attribute должно совпадать с :other.",
  "size" => array(
    "numeric" => "Поле :attribute должно быть :size.",
    "file" => "Поле :attribute должно быть :size Килобайт.",
    "string" => "Поле :attribute должно быть длиной :size символов.",
  ),
  "unique" => "Такое значение поля :attribute уже существует.",
  "url" => "Поле :attribute имеет неверный формат.",
  /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute_rule" to name the lines. This helps keep your
    | custom validation clean and tidy.
    |
    | So, say you want to use a custom validation message when validating that
    | the "email" attribute is unique. Just add "email_unique" to this array
    | with your custom message. The Validator will handle the rest!
    |
   */
  'custom' => array(
    'client_phone' => array(
      'regex' => 'Поле :attribute может состоять только из цифр, тире и знака плюс.',
    ),
    'phone' => array(
      'regex' => 'Поле :attribute может состоять только из цифр, тире и знака плюс.',
    ),
  ),
  /*
    |--------------------------------------------------------------------------
    | Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". Your users will thank you.
    |
    | The Validator class will automatically search this array of lines it
    | is attempting to replace the :attribute place-holder in messages.
    | It's pretty slick. We think you'll like it.
    |
   */
  'attributes' => array(
    'password' => 'Пароль',
    'title' => 'Заголовок',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'role_id' => 'Роль',
    'user_id' => 'Пользователь',
    'client_name' => 'Имя клиента',
    'client_phone' => 'Телефон клиента',
    'descr' => 'Описание',
    'visit_time' => 'Дата первичного выезда',
    'cost' => 'Стоимость',
    'finish_date' => 'Дата исполнения заказа',
    'status' => 'Статус',
    'DEFERRED' => 'Отложен',
    'REFUSED' => 'Отказной',
    'refuse_reason' => 'Причина отказа',
    'defer_before' => 'Отложить до',
    'defer_reason' => 'Причина откладывания',
  ),
);

/*
return array(

  "accepted" => "The :attribute must be accepted.",
  "active_url" => "The :attribute is not a valid URL.",
  "after" => "The :attribute must be a date after :date.",
  "alpha" => "The :attribute may only contain letters.",
  "alpha_dash" => "Значение поля :attribute может содержать только буквы, цифры и знаки тире и подчеркивания.",
  "alpha_num" => "Значение поля :attribute может содержать только буквы и цифры.",
  "array" => "The :attribute must be an array.",
  "before" => "The :attribute must be a date before :date.",
  "between" => array(
    "numeric" => "Значение :attribute должно быть от :min до :max.",
    "file" => "The :attribute must be between :min and :max kilobytes.",
    "string" => "Значение поля :attribute должно быть от :min до :max символов.",
    "array" => "Значение поля :attribute должно иметь от :min до :max пунктов.",
  ),
  "confirmed" => "The :attribute confirmation does not match.",
  "date" => "The :attribute is not a valid date.",
  "date_format" => "The :attribute does not match the format :format.",
  "different" => "The :attribute and :other must be different.",
  "digits" => "The :attribute must be :digits digits.",
  "digits_between" => "The :attribute must be between :min and :max digits.",
  "email" => "The :attribute must be a valid email address.",
  "exists" => "The selected :attribute is invalid.",
  "image" => "The :attribute must be an image.",
  "in" => "Значение поля :attribute не корректно.",
  "integer" => "The :attribute must be an integer.",
  "ip" => "The :attribute must be a valid IP address.",
  "max" => array(
    "numeric" => "The :attribute may not be greater than :max.",
    "file" => "The :attribute may not be greater than :max kilobytes.",
    "string" => "The :attribute may not be greater than :max characters.",
    "array" => "The :attribute may not have more than :max items.",
  ),
  "mimes" => "The :attribute must be a file of type: :values.",
  "min" => array(
    "numeric" => "The :attribute must be at least :min.",
    "file" => "The :attribute must be at least :min kilobytes.",
    "string" => "The :attribute must be at least :min characters.",
    "array" => "The :attribute must have at least :min items.",
  ),
  "not_in" => "The selected :attribute is invalid.",
  "numeric" => "Значение поля :attribute должно состоять из цифр.",
  "regex" => "The :attribute format is invalid.",
  "required" => "Значение поля :attribute является обязательным.",
  "required_if" => "The :attribute field is required when :other is :value.",
  "required_with" => "The :attribute field is required when :values is present.",
  "required_with_all" => "The :attribute field is required when :values is present.",
  "required_without" => "The :attribute field is required when :values is not present.",
  "required_without_all" => "The :attribute field is required when none of :values are present.",
  "same" => "The :attribute and :other must match.",
  "size" => array(
    "numeric" => "The :attribute must be :size.",
    "file" => "The :attribute must be :size kilobytes.",
    "string" => "The :attribute must be :size characters.",
    "array" => "The :attribute must contain :size items.",
  ),
  "unique" => "The :attribute has already been taken.",
  "url" => "The :attribute format is invalid.",
 *
 *

  'custom' => array(
    'attribute-name' => array(
      'rule-name' => 'custom-message',
    ),
  ),
 *
 *
  'attributes' => array(
    'password' => 'Пароль',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'role_id' => 'Роль',
  ),
);
 *
 */