<?php

return [
  'created' => 'В работе',
  'active' => 'В работе',
  'refused' => 'Отказной',
  'finished' => 'Завершен без долга в кассу',
  'finished_debt' => 'Завершен с долгом в кассу',
  'overdue' => 'Просрочен',
  'closed' => 'Закрыт',
  'new' => 'Не просмотрен',
  'call_wait' => 'Ожидает звонка',
  'visit_wait' => 'Ожидает выезда',
  'overdue' => 'Просрочен',
  'deferred' => 'Отложена',
  'overdue_deferred' => 'Просроченные отложенные',
  'archived' => 'Архивирован',
  'canceled' => 'Аннулирован',
];
