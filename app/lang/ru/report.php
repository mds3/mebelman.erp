<?php

return [
  'name' => ' - ',
  'entryCnt' => 'Заявок',
  'orderCnt' => 'Заказов',
  'orderSumm' => 'На сумму',
  'prepaySumm' => 'Предоплат',
  'amountSumm' => 'Получено',
  'cashboxSumm' => 'В кассе',
  'masterDebt' => 'Долг мастера',
  'clientDebt' => 'Долг клиента',
  'no_reports' => 'Отчетов за выбранный период не найдено.',
];
