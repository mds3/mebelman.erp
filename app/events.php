<?php

use \Mebius\Model\BaseModel;
use \Mebius\Repository\EntryRepository;

Event::listen('auth.login',
    function () {
        /* @var $Attempt AuthAttempt */
        $Attempt = App::make("\AuthAttempt");
        $Attempt->addNewAttempt();
    });

Event::listen('eloquent.updating: Entry',
    function ($entry) {
        /* @var $entry Entry */
        if (is_null($entry->master) || $entry->user_id != $entry->master->id) {
            $entry->view_time = $entry->getDefaultDateTimeString();
        }
    });

Event::listen('order.created_ok',
    function ($entry, $order) {
        /*
         * @var $entry Entry
         * @var $order Order
         */
        $entry->setStatus('archive');
        $entry->setOrderCreated(true);
        $entry->save();
        
        notifySuppler($order);
    });

Event::listen('order.stuff_changed',
    function ($order) {
        /* @var $order Order */
        notifySuppler($order);
        logStuff($order);
    });

function logStuff(Order $obj)
{
    $stuff = $obj->stuff;
    
    if (is_object($stuff) && method_exists($stuff, 'toArray')) {
        $stuff = $stuff->toArray();
    }
    
    $arr = [];
    
    $arr['items'][] = 'Изменил '.trans('order.stuff').' на "'.implode('; ',
            array_map(function ($item) {
                return $item['name'].': '.$item['quantity'];
            }, $stuff)).'"';
    
    if ($arr) {
        $arr['model_id'] = $obj->id;
        $arr['model']    = get_class($obj);
        $arr['event']    = 'updated';
        EventsLog::create($arr);
    }
}

function sendMailToSupplier(Order $order)
{
    $to      = "=?UTF-8?B?".base64_encode($order->supplier->name)."?= <{$order->supplier->email}>";
    $title   = 'Материалы / №'.$order->order_key;
    $subject = '=?UTF-8?B?'.base64_encode($title).'?=';
    $message = '
        <html>
        <head>
          <title>'.$title.'</title>
        </head>
        <body>
          <h2>Добавлены или изменены материалы к заказу №'.$order->getKey().'</h2>';
    if (count($order->stuff())) {
        $message .= '<u>Обновленный список материалов</u>:<br/>';
        $message .= '<ul>';
        foreach ($order->stuff as $stuff) {
            $message .= '<li>'.$stuff->name.' : '.$stuff->quantity.'</li>';
        }
        $message .= '</ul>';
    } else {
        $message .= 'Материалы отсутствуют / были удалены из заказа.';
    }
    $message .= '<a href="'.route('order.show', $order->getKey()).'">Перейти к заказу №'.$order->order_key.'</a><br/>
        </body>
        </html>
        ';
    
    $headers = 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    $headers .= 'From: DoctorMebius <doctormebius@'.$_SERVER['HTTP_HOST'].'>'."\r\n";
    
    mail($to, $subject, $message, $headers);
}

function sendMailToMaster(Entry $entry)
{
    $to      = "=?UTF-8?B?".base64_encode($entry->master->name)."?= <{$entry->master->email}>";
    $title   = 'Новая заявка №'.$entry->order_key;
    $subject = '=?UTF-8?B?'.base64_encode($title).'?=';
    $message = '
        <html>
        <head>
          <title>'.$title.'</title>
        </head>
        <body>
          <h2>Добавлена новая заявка №'.$entry->order_key.'</h2>
              <a href="'.route('entry.show', [$entry->id]).'">Перейти к заявке №'.$entry->order_key.'</a><br/>
        </body>
        </html>
        ';
    
    $headers = 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    $headers .= 'From: DoctorMebius <doctormebius@'.$_SERVER['HTTP_HOST'].'>'."\r\n";
    
    mail($to, $subject, $message, $headers);
}

function notify_entry_master_by_sms(Entry $entry)
{
    if (App::environment() !== 'local') {
        $msg = 'Добавлена заявка №'.$entry->order_key."\n"
            .''.route('entry.show', [$entry->id]);
        /* @var $SMS \Mebius\SmsGate\SmsGateInterface */
        $SMS = App::make("\Mebius\Service\SmsGate\SmsGateInterface");
        $SMS->send($entry->master->phone, $msg);
    }
}

function notifyEntryMaster(Entry $entry)
{
    $dirty = $entry->getDirty();
    $orig  = $entry->getOriginal();
    if (isset($dirty['user_id'])) {
        if (! isset($orig['user_id']) || $dirty['user_id'] != $orig['user_id']) {
            $ER      = App::make("\Mebius\Repository\EntryRepository");
            $updated = $ER->find($entry->id);
            //sendMailToMaster($updated);
            notify_entry_master_by_sms($updated);
        }
    }
}

function notifyEntryClient(Entry $entry)
{
    if (App::environment() !== 'local') {
        $msg = "Номер заявки {$entry->order_key}\nМастер с Вами свяжется\nwww.mebelman-group.ru";
        
        /* @var $SMS \Mebius\SmsGate\SmsGateInterface */
        $SMS = App::make("\Mebius\Service\SmsGate\SmsGateInterface");
        $SMS->send($entry->client_phone, $msg);
    }
}

function notifySuppler(Order $order)
{
    if ($order->supplier->id != Auth::id()) {
        // уведомляем мылом
        sendMailToSupplier($order);
    }
}

Event::listen('eloquent.updated: Entry',
    function ($entry) {
        // тут отправляем уведомление новому мастеру
        notifyEntryMaster($entry);
    });

Event::listen('eloquent.created: Entry',
    function ($entry) {
        // тут отправляем уведомление назначенному мастеру
        notifyEntryMaster($entry);
        // и клиенту
        notifyEntryClient($entry);
    });

Event::listen('entry.show',
    function (Entry $entry, EntryRepository $repo) {
        
        if (! is_null($entry->master) && Auth::id() == $entry->master->id && $entry->is_new) {
            if (! $repo->setMasterSawEntry($entry)) {
                //echo '<pre>', print_r($repo->getErrors()), '</pre>';
                //die('not saved');
            }
        }
    });

Event::listen('eloquent.deleting: Entry',
    function ($entry) {
        // тут заносим заявку в архив
        Log::info('Entry deleting. Try to add it into archive '.$entry->order_key);
        $arch            = new EntryArchive;
        $arch->order_key = $entry->order_key;
        $arch->data      = serialize($entry->toArray());
        $arch->save();
    });

Event::listen('eloquent.updated:*',
    function ($obj) {
        /* @var $obj BaseModel */
        if (! $obj->isLoggable()) {
            return;
        }
        $arr    = [];
        $none   = $obj->getNoLogFields();
        $dirty  = $obj->getDirty();
        $orig   = $obj->getOriginal();
        $oldRel = $obj->relationsToArray();
        $obj->syncOriginal();
        $obj->load(array_keys($oldRel));
        $rel = array_map(function ($item) {
            return is_object($item) ? $item->toArray() : null;
        }, $obj->getRelations());
        
        foreach ($dirty as $key => $val) {
            if (! in_array($key, $none)) {
                $relName = $obj->getRelationNameByField($key);
                $name    = $relName ? $obj->getRelation($relName)->getNameKey() : 'name';
                
                $from = $relName ? $oldRel[$relName][$name] : $orig[$key];
                $to   = $relName ? $rel[$relName][$name] : $val;
                
                $callback = $obj->getLogFieldCallback($key);
                if ($callback) {
                    $from = $callback($from, true);
                    $to   = $callback($to, true);
                }
                
                if ($key == 'status') {
                    $from = trans(strtolower(get_class($obj).'.statuses.'.$from));
                    $to   = trans(strtolower(get_class($obj).'.statuses.'.$to));
                }
                $arr['items'][] = 'Изменил значение "<b>'.trans(strtolower(get_class($obj))
                        .'.'.$key).'</b>"'
                    ."\n"
                    .'<i>стало:</i> "<b>'.$to.'</b>"'."\n"
                    .'<i>было:</i> "'.$from.'"';
            }
        }
        if ($arr) {
            $arr['model_id'] = $obj->id;
            $arr['model']    = get_class($obj);
            $arr['event']    = 'updated';
            EventsLog::create($arr);
        }
    });

Event::listen('eloquent.deleted:*',
    function ($obj) {
        /* @var $obj BaseModel */
        if (! $obj->isLoggable()) {
            return;
        }
        $arr['model_id'] = $obj->id;
        $arr['model']    = get_class($obj);
        $arr['event']    = 'deleted';
        $arr['items'][]  = 'Удалил: '.trans('msg.'.strtolower($arr['model'])).' '.$obj->getNameValue();
        EventsLog::create($arr);
    });
