<?php

use \Illuminate\Database\Eloquent\Model;

if (!function_exists("route_allowed")) {

    function route_allowed($route)
    {
        if (Auth::check()) {
            if (Auth::user()->id == 1) {
                // superadmin full rights hardcode
                return true;
            }
            foreach (Auth::user()->roles as $role) {
                foreach ($role->permissions as $permission) {
                    //$path = Route::getCurrentRoute()->getPath();
                    $_route = explode(":", $permission->route);
                    if ($_route[0] == $route) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}

function auth_has_role($role_id)
{
    foreach (Auth::user()->roles as $role) {
        if ($role_id == $role->id) {
            return true;
        }
    }
    return false;
}

if (!function_exists("get_allowed_options")) {

    function get_allowed_options($route)
    {

        //echo '<h1>' . __FUNCTION__ . '</h1>';

        static $ret = [];

        $route = getUnifiedRouteName($route);
        //echo "$route: ";

        if (Auth::check()) {
            if (Auth::user()->id == 0) {
                // суперадмин может всё
                return [];
            }
            if (isset($ret[$route])) {
                if ($ret[$route]) {
                    //echo "isset - return<br/>";
                    return $ret[$route];
                }
            } else {
                //echo '<h2>calculate</h2>';
                $ok = false;
                foreach (Auth::user()->roles as $role) {
                    foreach ($role->permissions as $permission) {
                        //$path = Route::getCurrentRoute()->getPath();
                        $_route = explode(":", $permission->route);
                        if ($_route[0] == $route) {
                            $ok = true;
                            if (isset($_route[1])) {
                                $ret[$route][] = $_route[1];
                            } else {
                                $ret[$route][] = '';
                            }
                            //echo "not isset - ok - created<br/>";
                            //die();
                            //return true;
                        }
                    }
                }
                if ($ok) {
                    //echo '<pre>', print_r($ret), '</pre>';
                    //die();
                    return $ret[$route];
                }
                unset($ret[$route]);
            }
            //echo "not ok - return false <br/>";

            return false;
        }
        Redirect::route('main')->with('danger',
            'У вас нет прав на выполнение этого действия...')->send();
    }

}

if (!function_exists("action_allowed")) {

    function action_allowed($route_name, Model $model)
    {
        $opts = get_allowed_options($route_name);
        //echo '<pre>', print_r($opts), '</pre>';
        if ($opts !== false) {
            //echo "$route_name<pre>", print_r($opts), '</pre>';
            //die();
            $ok = false;
            if (empty($opts)) {
                $ok = true;
            }
            foreach ($opts as $opt) {
                if ($ok) {
                    break;
                }
                //echo 'opt: ' . $opt . '<br/>';
                if (empty($opt)) {
                    $ok = true;
                    continue;
                }
                if (isset($model->{$opt}) && $model->{$opt} == Auth::id()) {
                    //echo 'Action ALLOWED ' . get_class($model) . '->' . $opt . '<br/>';
                    $ok = true;
                } else {
                    //echo 'Action NOT allowed ' . get_class($model) . '->' . $opt . '<br/>';
                    $ok = false;
                }
            }
            //die('ok: ' . ((int) $ok));
            //echo "Action $route_name ALLOWED for " . get_class($model) . " {$model->getKeyName()}: {$model->getKey()} <br/>";
            return $ok;
        }
        //echo "Action $route_name NOT ALLOWED for model " . get_class($model) . " {$model->getKeyName()}: {$model->getKey()} <br/>";
        return false;
    }

}

function getUnifiedRouteName($route_name)
{
    $duos = [
      'store' => 'create',
      'update' => 'edit',
    ];
    return strtr($route_name, $duos);
}

function get_phone_format($s = 'd')
{
    return '+7 (' . $s . $s . $s . ') ' . $s . $s . $s . '-' . $s . $s . '-' . $s . $s;
}

function get_phone_sanitize_regexp()
{
    return '/[^\d\+]+/i';
}

function sanitize_phone($phone)
{
    return preg_replace(get_phone_sanitize_regexp(), '', $phone);
}

function human_date_time($datetime_str = false, $inline = false)
{
    if (!$datetime_str) {
        $time = time();
    } else {
        $time = \strtotime($datetime_str);
    }
    if ($time <= 0) {
        return '-';
    }

    $sep = get_datetime_separator($inline);
    return \strftime('%d %b %Y' . $sep . '%A в %H:%M', $time);
    //return \date('j.m.y в H:i', $time);
}

function human_date($datetime_str = false, $inline = false)
{
    if (!$datetime_str) {
        $time = time();
    } else {
        $time = \strtotime($datetime_str);
    }
    if ($time <= 0) {
        return '-';
    }

    $sep = get_datetime_separator($inline);
    return \strftime('%d %b %Y' . $sep . '%A', $time);
    //return \date('j.m.y', strtotime($datetime_str));
}

function get_datetime_separator($inline)
{
    if ($inline) {
        return ' ';
    } else {
        return '<br/>';
    }
}
