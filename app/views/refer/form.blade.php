@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php //echo '<pre>', print_r($refer->permissions->toArray()), '</pre>'; ?>

{{ Form::model($refer, array('route' => $route, 'class' => 'form-horizontal','role'=>'form','method'=>(isset($method)?$method:'POST'))) }}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$page_title}}</h3>
    </div>
    <div class="panel-body">

        <!-- Name -->
        <div class="form-group{{{ $errors->has('name') ? ' has-error' : '' }}}">
            {{ Form::label('name', 'Название', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <!-- Descr -->
        <div class="form-group{{{ $errors->has('descr') ? ' has-error' : '' }}}">
            {{ Form::label('descr', 'Описание', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::textarea('descr', Input::old('descr'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('descr') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                {{ Form::submit('Сохранить', array('class' => 'btn btn-large btn-primary')) }}
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}

@stop