@extends('includes.extends_master')

{{-- Content --}}
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title">Источник</h2>
    </div>
    <div class="panel-body">
        <h3>{{{$refer->name}}}</h3>
        <p>{{{$refer->descr}}}</p>
        @include('includes.control_unit',[
        'edit_route' => 'refer.edit',
        'delete_route' => 'refer.destroy',
        'model' => $refer,
        ])
    </div>
</div>
@stop