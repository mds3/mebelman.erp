@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if(route_allowed('refer.create'))
<div class="row">
    <a class="btn btn-large btn-primary col-sm-4" href="<?=
    URL::route('refer.create', null, false)

    ?>">Новый источник</a>
</div>
@endif

<?php /* @var $refer \Refer */ ?>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Все источники</div>

    <div class="list-group">
        @foreach ($refers as $refer)
        <a href="{{URL::route('refer.show', $refer->id, false)}}" class="list-group-item">
            <h4 class="list-group-item-heading">{{{$refer->name}}}</h4>
        </a>
        @endforeach
    </div>
</div>
@stop