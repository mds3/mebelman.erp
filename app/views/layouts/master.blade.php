<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('title')
            {{ \Config::get('app.name') }}
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" href="/favicon.{{\App::environment()}}.ico" type="image/x-icon"/>

        <!-- Scripts are placed here -->
        {{ HTML::script('/js/jquery-1.11.1.min.js') }}
        {{ HTML::script('/js/bootstrap.min.js') }}

        <!-- CSS are placed here -->
        {{ HTML::style('/css/bootstrap.min.css') }}
        {{ HTML::style('/css/bootstrap-theme.min.css') }}
        {{ HTML::style('/css/style.css') }}

    </head>

    <body>
        <!-- Navbar -->
        @if (! Auth::guest() )
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/">
                        {{ Auth::user()->name }}
                    </a>

                </div>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="collapse navbar-collapse">
                    @include('includes.menu')
                </div>
            </div>
        </div>
        @endif

        <noscript>
        <style type="text/css">
            #page-container {display:none;}
        </style>
        <div class="alert alert-danger alert-block">
            Для корректной работы системы необходимо, что бы JavaScript был включен в вашем браузере.
        </div>
        </noscript>

        <!-- Container -->
        <div id="page-container" class="container-fluid">

            <!-- Messages -->
            @foreach (['success','danger','info'] as $alert)
            @if (Session::get($alert))
            <div class="alert alert-{{$alert}} alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4 class="text-capitalize">{{ucfirst(trans('msg.'.$alert))}}!</h4>
                {{{ Session::get($alert) }}}
            </div>
            @endif
            @endforeach

            <!-- Content -->
            @yield('content')

        </div>

        <footer class="text-warning text-center">
            {{ \Config::get('app.name') }} &copy; 2014 - {{ date('Y') }}
        </footer>

    </body>
</html>
