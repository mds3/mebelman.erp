@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $order->getIcon() }}"></span>
            {{ $order->order_key_print }} / {{{ $order->master->name }}}
        </h3>
    </div>
    <div class="panel-body">

        <div style="padding-left:5px;">
            <small>
                {{ human_date($order->created_at, true) }}
            </small>
            <br/>
            <h4 style="margin-top:0;">
                {{{$order->title}}}
            </h4>

            @if($order->mo_town || $order->metro)
            <p style="margin-top:0;">
                @if($order->metro)
                <span class="text-info">
                    {{{ trans('msg.metro_pref').$order->metro.' ' }}}
                </span>
                @endif
                @if($order->metro && $order->mo_town)
                /
                @endif
                @if($order->mo_town)
                <span class="text-warning">
                    {{{ trans('msg.town_pref').$order->mo_town }}}
                </span>
                @endif
            </p>
            @endif

            @if($order->is_archived)
            <div class="alert alert-{{$order->finish_status}} alert-block">
                @if($order->is_canceled)
                {{ trans('status.canceled').' '.human_date_time($order->canceled_at, true) }}
                @else
                {{ trans('status.archived').' '.human_date_time($order->archived_at, true) }}
                @endif
            </div>
            @endif

            @if($order->is_finished && !$order->is_archived)
            <div class="alert alert-block alert-{{ $order->finish_status }}" style="margin:0">
                {{ trans('status.'.strtolower($order->status)).' '.human_date_time($order->finished_time, true) }}
                <br/>
            </div>
            @endif

            @if(!$order->is_archived && !$order->is_finished && $order->finish_status != 'success')
            <div class="alert alert-block alert-{{ $order->finish_status }}" style="margin:7px 0px 0px 3px;">

                {{ trans('order.finish_'.$order->finish_status) }}
                <br/>
                <b>{{{ strftime('%d %b %Y %A',strtotime($order->finish_date)) }}}</b>

            </div>
            @endif

            <br/>

            @if($order->descr)
            <p style="margin-top:10px;">
                <span class="glyphicon glyphicon-phone-alt"></span>
                <em>{{\nl2br($order->descr)}}</em>
            </p>
            @endif

            @if(!empty($order->details))
            <p>
                <span class="glyphicon glyphicon-question-sign"></span>
                <em>{{\nl2br($order->details)}}</em>
            </p>
            @endif

            @if(!empty($order->comment))
            <p>
                <span class="glyphicon glyphicon-info-sign"></span>
                <em>{{\nl2br($order->comment)}}</em>
            </p>
            @endif

        </div>

        <div class="well">

            <h4 style="margin:0 0 10px 2px;">
                {{{ $order->client_name }}}
            </h4>

            <a class="btn btn-success" href="tel:{{{ $order->client_phone }}}">
                <span class="glyphicon glyphicon-earphone"> </span>
                <span class="bfh-phone" data-format="<?= get_phone_format() ?>" data-number="{{$order->client_phone}}"></span>
            </a>
            <br/>

            @if(!$order->is_archived && !$order->is_finished && $order->finish_status == 'success')
            <p class="text-{{ $order->finish_status }}" style="margin:7px 0px 0px 3px;">

                {{ trans('order.finish_'.$order->finish_status) }}
                <br/>
                <b>{{{ strftime('%d %b %Y %A',strtotime($order->finish_date)) }}}</b>

            </p>
            @endif

        </div>

        <dl class="dl-horizontal details" style="margin-left:8px;">

            <dt>
            {{ trans('order.cost') }}
            </dt>
            <dd id="cost">
                <b>{{{ $order->cost.' '.$order->currency }}}</b>
                <br/>
                <span class="text-<?= $order->cashless ? 'danger' : 'success' ?>">
                    {{ $order->cashless ? trans('order.cashless') : trans('order.cash') }}
                </span>
            </dd>

            <dt>
            {{ trans('order.cashbox') }}
            </dt>
            <dd id="cashbox">
                @if(isset($routes['payments']))
                <a id="payments" href="{{ $routes['payments']['route'] }}" class="btn btn-default">
                    <span>{{ $order->cashbox }}</span> {{ $order->currency }}
                </a>
                @else
                <span>{{ $order->cashbox }}</span> {{ $order->currency }}
                @endif
            </dd>

            @if($order->has_debt)

            @if(!$order->cashless)

            <dt>
            {{ trans('order.prepay') }}
            </dt>
            <dd id="prepay">
                {{ $order->prepay ? $order->prepay.' '.$order->currency
                   .'<br/><small class="text-warning">'.human_date($order->created_at, true).'</small>' : trans('msg.no') }}
            </dd>

            @if($order->is_finished || $order->is_archived)

            @if($order->cashless)
            <dt>{{ trans('order.docs_signed') }}</dt>
            <dd>{{{ $order->docs_signed ? trans('msg.yes') : trans('msg.no') }}}</dd>
            @else
            <dt>{{ trans('order.result_amount') }}</dt>
            <dd id="result_amount">
                {{ $order->result_amount ? $order->result_amount.' '.$order->currency.'<br/><small class="text-warning">'.human_date($order->finished_time, true).'</small>' : trans('msg.no') }}
            </dd>
            <dt>
            {{ trans('order.client_debt') }}
            </dt>
            <dd>{{{ $order->client_debt.' '.$order->currency }}}</dd>
            @endif

            @endif

            <dt>
            {{ trans('order.cashbox_master_debt') }}
            </dt>
            <dd id="cashbox"><span>{{ $order->cashbox_master_debt }}</span> {{ $order->currency }}</dd>

            @endif

            <dt>
            {{ trans('order.cashbox_debt') }}
            </dt>
            <dd id="cashbox_debt"><span>{{ $order->cashbox_debt }}</span> {{ $order->currency }}</dd>

            @endif

            @if(route_allowed('refer.index'))
            <dt>
            <span class="glyphicon glyphicon-user"></span>
            Мастер
            </dt>
            <dd>
                @if(!$order->master_isset)
                {{ trans('msg.not_assigned') }}
                @else
                <a class="btn btn-success" href="tel:{{{ $order->master->phone }}}">
                    <span class="glyphicon glyphicon-earphone"> </span>
                    <span>{{ $order->master->name }}</span>
                </a>
                @endif
            </dd>
            @endif

            <dt>
            @if(isset($routes['stuff']) && !$order->is_finished)
            <button id="stuff-button" type="button" class="btn btn-default btn-large">
                <b>{{ trans('order.stuff') }}</b>
            </button>
            @else
            {{ trans('order.stuff') }}
            @endif
            </dt>
            <dd id="stuff-list">
                @if($order->stuff->count())
                <ul style="margin-top:7px;">
                    @foreach($order->stuff as $item)
                    <li>
                        {{ $item->name }} - {{ $item->quantity }}
                    </li>
                    @endforeach
                </ul>
                @else
                -
                @endif
            </dd>

        </dl>


        <div id="data-loading" class="well">
            <h4><img src="/img/ajax-loader.gif"/> {{ trans('msg.loading') }}</h4>
        </div>
        <div id="results" class="alert alert-block alert-success">
        </div>

        @if(isset($routes['stuff']))
        @include('order.inc.stuff')
        @endif

        @if(isset($routes['finish']))
        @include('order.inc.finishForm',['route' => $routes['finish']])
        @endif

        @if(isset($routes['cashbox']))
        @include('order.inc.cashbox',['routes' => $routes])
        @endif

        <div class="col-lg-3">

            @if(isset($routes['cashbox']))
            <a id="cashbox-button" type="button" class="btn btn-primary btn-block">{{$routes['cashbox']['name']}}</a>
            @endif

            @if(isset($routes['finish']))
            <a id="finish-button" type="button" class="btn btn-primary btn-block" href="#js">{{ $routes['finish']['name'] }}</a>
            @endif

            <!-- archiving-button -->
            @if(isset($routes['archivate']))
            {{ Form::postButton($routes['archivate'],
                trans('order.do_archived')." ".$order->order_key."?") }}
            @endif

            @if(isset($routes['edit']))
            <a id="edit-button" type="button" class="btn btn-primary btn-block" href="{{$routes['edit']['route']}}">
                {{ $routes['edit']['name'] }}
            </a>
            @endif

            @if(isset($routes['cancel']))
            {{ Form::postButton($routes['cancel'],
                trans('order.cancel-button-confirm'), 'default') }}
            @endif

        </div>

        <div class="row col-lg-12" style="margin-top:5px;">
            @if(isset($routes['showlogs']))
            @include('includes.showlogs',['route' => $routes['showlogs']])
            @endif
        </div>

    </div>
</div>

@if(isset($routes['order.index']))
<?php $route = $routes['order.index']; ?>
<a href="{{ $route->route }}" type="button" class="btn btn-default col-xs-12 col-lg-3">
    {{ $route->name }}
</a>
@endif
<br/>
<br/>

{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}

<script>
    $(document).ready(function () {

        close_all_forms();

        $(".reset").click(function () {
            close_all_forms();
        });

        $("#stuff-button").click(function () {
            close_all_forms();
            $("#stuff-form").show();
            $("#stuff-list").hide();
        });

        function close_all_forms() {
            $("#order-finish-form").hide();
            $("#order-cashbox-form").hide();
            $("#stuff-form").hide();
            $("#finish-button").show();
            $("#cancel-button").show();
            $("#cashbox-button").show();
            $('#data-loading').hide();
            $("#results").hide();
            $("#stuff-list").show();
        }

<?php if (isset($routes['cancel'])): ?>

            $("#cancel-button").click(function () {
                close_all_forms();
                if (confirm("<?= trans('msg.cancel-button-confirm') ?>")) {
                    $('#data-loading').show();
                    $(this).hide();
                    $.ajax({
                        type: "POST",
                        data: '_token=<?= csrf_token() ?>',
                        cache: false,
                        url: '<?= $routes['cancel']['route'] ?>',
                        success: function (data) {
                            if (data.error === false) {
                                $("#results").text("<?= trans('order.cancelled_ok') ?>")
                                        .removeClass('alert-danger')
                                        .addClass('alert-success')
                                        .show();
                            } else {
                                $("#results").text('<?= trans('order.an_error') ?>: ' + data.msg)
                                        .removeClass('alert-success')
                                        .addClass('alert-danger')
                                        .show();
                            }
                            $('#data-loading').hide();
                        },
                        error: function (jqXHR, textStatus) {
                            $("#results").html('<?= trans('order.an_error') ?>: ' + textStatus).show();
                            $('#data-loading').hide();
                            $("#cancel-button").show();
                        }
                    });
                    return false;
                }
            });
<?php endif; ?>
    });
</script>


@stop