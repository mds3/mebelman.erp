@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
/* @var $errors Illuminate\Support\ViewErrorBag */

?>

@if($errors->count())
<div class="alert alert-danger alert-block">
    На странице имеются ошибки.
</div>
@endif

{{ HTML::style('/css/typeahead.css') }}
{{ HTML::style('/css/jquery.datetimepicker.css') }}
{{ HTML::script('/js/jquery.datetimepicker.js') }}

{{ Form::model($order, array('route' => $route, 'class' => 'form-horizontal','role'=>'form','method'=>(isset($method)?$method:'POST'))) }}

<div class="panel panel-default col-sm-6 col-xs-12">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $order->getIcon() }}"></span>
            {{ $order->order_key_print }} / {{ trans('order.edition')}}
        </h3>
    </div>
    <div class="panel-body">

        <!-- Title -->
        <div class="form-group{{{ $errors->has('title') ? ' has-error' : '' }}}">
            {{ Form::label('title', 'Заголовок', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('title') }}</span>
                {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Name -->
        <div class="form-group{{{ $errors->has('client_name') ? ' has-error' : '' }}}">
            {{ Form::label('client_name', 'Имя', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('client_name') }}</span>
                {{ Form::text('client_name', Input::old('client_name'), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Phone -->
        <div class="form-group{{{ $errors->has('client_phone') ? ' has-error' : '' }}}">
            {{ Form::label('client_phone', 'Телефон', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('client_phone') }}</span>
                {{ Form::input('tel', 'client_phone', Input::old('client_phone'), ['class' => 'form-control bfh-phone','data-format'=>get_phone_format()]) }}
            </div>
        </div>

        <!-- MetroStation -->
        <div class="form-group{{{ $errors->has('metro') ? ' has-error' : '' }}}">
            {{ Form::label('metro', 'Метро', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('metro') }}</span>
                {{ Form::text('metro', Input::old('metro'), array('class' => 'form-control typeahead')) }}
            </div>
        </div>

        <!-- MO_town -->
        <div class="form-group{{{ $errors->has('mo_town') ? ' has-error' : '' }}}">
            {{ Form::label('mo_town', 'Город М.О.', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('mo_town') }}</span>
                {{ Form::text('mo_town', Input::old('mo_town'), array('class' => 'form-control typeahead')) }}
            </div>
        </div>

        <!-- Master -->
        <div class="form-group{{{ $errors->has('created_by') ? ' has-error' : '' }}}">
            {{ Form::label('created_by', 'Мастер-приемщик', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('created_by') }}</span>
                {{Form::select('created_by', $masters, (is_null($order->master) ? 0 : $order->master->id), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Descr -->
        <div class="form-group{{{ $errors->has('descr') ? ' has-error' : '' }}}">
            {{ Form::label('descr', 'Описание', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('descr') }}</span>
                {{ Form::textarea('descr', Input::old('descr'), array('class' => 'form-control')) }}

            </div>
        </div>

        <!-- refer -->
        <div class="form-group{{{ $errors->has('refer_id') ? ' has-error' : '' }}}">
            <label class="col-sm-4 control-label" for="refer_id">Источник</label>
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('refer_id') }}</span>
                {{Form::select('refer_id', $refers, $order->refer_id, array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- cashless form -->
        <div class="form-group{{{ $errors->has('cashless') ? ' has-error' : '' }}}" id="cashless-form">
            {{ Form::label('cashless', ' ', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::checkbox('cashless', 1, Input::old('cashless'), array('class' => '')) }}
                Безналичный расчет
                <span class="text-danger">{{ $errors->first('cashless') }}</span>
            </div>
        </div>

        <!-- Cost -->
        <div class="form-group{{{ $errors->has('cost') ? ' has-error' : '' }}}">
            {{ Form::label('cost', 'Стоимость', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-3 input-group">
                <span class="text-danger">{{ $errors->first('cost') }}</span>
                {{ Form::input('tel', 'cost', Input::old('cost'), array('class' => 'form-control')) }}
                <span class="input-group-addon"> руб.</span>
            </div>
        </div>

        <!-- Prepay -->
        <div class="form-group{{{ $errors->has('prepay') ? ' has-error' : '' }}}" id="prepay-form">
            {{ Form::label('prepay', 'Предоплата', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-3 input-group">
                <span class="text-danger">{{ $errors->first('prepay') }}</span>
                {{ Form::input('tel', 'prepay', Input::old('prepay'), array('class' => 'form-control')) }}
                <span class="input-group-addon"> руб.</span>
            </div>
        </div>

        <!-- inplace form -->
        <div class="form-group{{{ $errors->has('inplace') ? ' has-error' : '' }}}" id="inplace-form">
            {{ Form::label('inplace', ' ', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-7 col-md-6">
                {{ Form::checkbox('inplace', 1, Input::old('inplace'), array('class' => '')) }}
                Ремонт на месте
                <span class="text-danger">{{ $errors->first('inplace') }}</span>
            </div>
        </div>

        <!-- finish-date-form -->
        <div class="form-group{{{ $errors->has('finish_date') ? ' has-error' : '' }}}" id="finish-date-form">
            {{ Form::label('finish_date', 'Дата исполнения заказа', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-7 col-md-6">
                <span class="text-danger">{{ $errors->first('finish_date') }}</span>
                {{ Form::text('finish_date', Input::old('finish_date'), array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group{{{ $errors->has('stuff') ? ' has-error' : '' }}}">
            {{ Form::label('stuff', 'Материалы', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-md-8 well">
                <span class="text-danger">{{ $errors->first('stuff') }}</span>
                @include('order.inc.stuff_fields')
            </div>
        </div>

        <!-- comment -->
        <div class="form-group{{{ $errors->has('comment') ? ' has-error' : '' }}}">
            {{ Form::label('comment', 'Комментарий', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-7 col-md-6">
                <span class="text-danger">{{ $errors->first('comment') }}</span>
                {{ Form::textarea('comment', Input::old('comment'), array('class' => 'form-control', 'size' => '20x4')) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                {{ Form::submit('Сохранить', array('class' => 'btn btn-block btn-primary')) }}
                <a href="{{ URL::route('order.show', $order->getKey()) }}" type="button" class="btn btn-default btn-block">Вернуться</a>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}

{{ HTML::script('/js/typeahead.bundle.min.js') }}
{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}

<script>
    $(document).ready(function () {


        var metro = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: '/assets/metro.obj.json'
        });
        var mo_towns = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '/assets/mo_towns.obj.json'
        });
        mo_towns.initialize();
        metro.initialize();
        $('#metro').typeahead(null, {
            name: 'metro',
            displayKey: 'name',
            source: metro.ttAdapter()
        });
        $('#mo_town').typeahead(null, {
            name: 'mo_town',
            displayKey: 'name',
            source: mo_towns.ttAdapter()
        });

        $('#finish_date').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d',
            timepicker: false,
            closeOnDateSelect: true
        });
    });
</script>

@stop

