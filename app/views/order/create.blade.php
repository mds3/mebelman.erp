@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
$view_time = strtotime($entry->view_time);
$call_time = strtotime($entry->call_time);
$visit_time = strtotime($entry->visit_time);
$owner = \Auth::id() == $entry->master->id;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
            {{ $entry->order_key_print }} /
            {{{ is_null($entry->master)? 'без мастера' : $entry->master->name }}}
        </h3>
    </div>
    <div class="panel-body">

        <div style="padding-left:5px;">

            <h4 style="margin-top:0;">
                <small>
                    <small>
                        {{{date('j.m.Y в H:i',strtotime($entry->created_at))}}}
                    </small>
                </small>
                <br/>
                {{{$entry->title}}}
            </h4>

            @if($entry->mo_town || $entry->metro)
            <p style="margin-top:0;">
                @if($entry->metro)
                <span class="text-info">
                    {{{ 'м.'.$entry->metro.' ' }}}
                </span>
                @endif
                @if($entry->metro && $entry->mo_town)
                /
                @endif
                @if($entry->mo_town)
                <span class="text-warning">
                    {{{ 'г.'.$entry->mo_town }}}
                </span>
                @endif
            </p>
            @endif

            @include('order.inc.create_fields_form')
            <br/>

            @if($entry->descr)
            <p>
                <span class="glyphicon glyphicon-phone-alt"></span>
                <em>{{\nl2br($entry->descr)}}</em>
            </p>
            @endif

            @if(!empty($entry->details))
            <p>
                <span class="glyphicon glyphicon-info-sign"></span>
                <em>{{{$entry->details}}}</em>
            </p>
            @endif

        </div>



        @if($entry->is_deferred)
        <div class="alert alert-warning alert-block">
            <dl class="dl-horizontal details">
                <!-- deferred -->
                <dt>Отложена до</dt>
                <dd>
                    {{ human_date($entry->defer_before) }}
                </dd>
                <!-- defer_reason -->
                <dt>Причина</dt>
                <dd>
                    {{ $entry->defer_reason }}
                </dd>
            </dl>
        </div>
        @endif


        @if($entry->is_refused)
        <!-- refused -->
        <div class="alert alert-danger alert-block">
            <dl class="dl-horizontal details">
                <dt>Отказ</dt>
                <dd>
                    {{ human_date_time($entry->refuse_time) }}
                </dd>
                <!-- details -->
                <dt>Причина отказа</dt>
                <dd>
                    @if(!empty($entry->refuse_reason))
                    {{ \nl2br($entry->refuse_reason) }}
                    @else
                    <i>Не указана</i>
                    @endif
                </dd>
            </dl>
        </div>
        @endif

        <div class="well">

            <h4 style="margin:0 0 10px 2px;">
                {{{ $entry->client_name }}}
            </h4>

            <a class="btn btn-success" href="tel:{{{ $entry->client_phone }}}">
                <span class="glyphicon glyphicon-earphone"> </span>
                <span class="bfh-phone" data-format="<?= get_phone_format() ?>" data-number="{{$entry->client_phone}}"></span>
            </a>

        </div>

        <dl class="dl-horizontal details" style="margin-left:8px;">

            @if($entry->visit_time_isset)
            <!-- visit_time -->
            <dt>
            <span class="glyphicon glyphicon-road col-lg-offset-0"></span>
            Выезд
            </dt>
            <dd>
                <span class="text-info">
                    <b>{{ human_date_time($entry->visit_time) }}</b>
                </span>
            </dd>
            @endif

            @if($entry->master_isset)

            <dt>
            <span class="glyphicon glyphicon-eye-open"></span>
            Видел
            </dt>
            <dd>
                @if(!$entry->is_new)
                {{ human_date_time($entry->view_time) }}
                @else
                Нет
                @endif
            </dd>

            @if($entry->visit_time_isset)
            <dt>
            <span class="glyphicon glyphicon-phone-alt"></span>
            Звонил
            </dt>
            <dd>
                @if($entry->is_called)
                {{ human_date_time($entry->call_time) }}
                @else
                Нет
                @endif
            </dd>
            @endif

            @endif

            @if(route_allowed('refer.index'))
            <dt>
            <span class="glyphicon glyphicon-user"></span>
            Мастер
            </dt>
            <dd>
                @if(!$entry->master_isset)
                не назначен
                @else
                <a class="btn btn-success" href="tel:{{{ $entry->master->phone }}}">
                    <span class="glyphicon glyphicon-earphone"> </span>
                    <span>{{ $entry->master->name }}</span>
                </a>
                @endif
            </dd>
            <dt>Источник</dt>
            <dd>{{{$entry->refer->name}}}</dd>
            @endif

        </dl>

    </div>
</div>

{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}


@stop