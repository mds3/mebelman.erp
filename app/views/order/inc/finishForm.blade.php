<?php
/* @var $order Order */

?>



{{ Form::model($order, array(
                    'url' => $route['route'],
                    'class' => 'form-horizontal well',
                    'id' => 'order-finish-form',
                    'role'=>'form',
                    'files' => true,
                    'method'=>(isset($route['method'])?$route['method']:'POST'))) }}

<div class="form-group" id="debt-form-group">
    <label class="col-sm-4 control-label">Долг клиента</label>
    <div class="col-sm-8">
        <span id="debt-value" class="form-control-static">{{{($order->cost - $order->prepay)}}}</span>  {{$order->currency }}
    </div>
</div>

@include('order.inc.finish_fields_form')

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-large btn-primary')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">Отмена</button>
    </div>
</div>

{{ Form::close() }}

<script>
    $(document).ready(function () {

        var order = <?= $order->toJson() ?>;

        if (order.cashless == 0) {
            $("#docs-signed-form").hide();
            $("#result-amount-form").show();
            $("#debt-form-group").show();
        } else {
            $("#docs-signed-form").show();
            $("#result-amount-form").hide();
            $("#debt-form-group").hide();
        }

        $("#finish-button").click(function () {
            //alert('clicked');
            $(this).hide();
            //close_all_forms();
            $("#order-finish-form").show();
            $("#result_amount").val(function () {
                $(this).val() ? $(this).val() : '';
            });
        });

        $("#result_amount").on('input', function () {
            $("#debt-value").text(order.cost - order.prepay - $(this).val());
        });
    });
</script>