<?php
/* @var $order Order */

?>

<div id="data-loading" class="well">
    <h4><img src="/img/ajax-loader.gif"/> Обработка данных...</h4>
</div>

<div id="results" class="alert alert-block alert-success">
</div>

<button id="btn-order-close" class="btn btn-primary btn-sm">{{$routes['close']['name']}}</button>

<script>
    $(document).ready(function () {

        var order = <?= $order->toJson() ?>;

        $('#data-loading').hide();
        $("#results").hide();

        if (<?= $order->cashbox_debt ?>) {
            $("#btn-order-close").hide();
        }

        $("#btn-order-close").click(function () {
            if (confirm("Закрыть заказ?")) {
                $('#data-loading').show();
                $(this).hide();
                $.ajax({
                    type: "POST",
                    data: '_token=<?= csrf_token() ?>',
                    dataType: "json",
                    cache: false,
                    url: '<?= $routes['close']['route'] ?>',
                    success: function (data) {
                        if (data.error === false) {
                            $("#results").text("Заказ успешно закрыт")
                                    .removeClass('alert-danger')
                                    .addClass('alert-success')
                                    .show();
                        } else {
                            $("#results").text('Произошла ошибка: ' + data.msg)
                                    .removeClass('alert-success')
                                    .addClass('alert-danger')
                                    .show();
                        }
                        $('#data-loading').hide();
                    },
                    error: function (jqXHR, textStatus) {
                        $("#results").html('Произошла ошибка: ' + textStatus).show();
                        $('#data-loading').hide();
                        $("#btn-order-close").show();
                    }
                });
                return false;
            }
        });
    });
</script>