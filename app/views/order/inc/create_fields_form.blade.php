{{ HTML::style('/css/jquery.datetimepicker.css') }}
{{ HTML::script('/js/jquery.datetimepicker.js') }}

{{ Form::model($entry, array(
                    'route' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'files' => true,
                    'method'=>(isset($method)?$method:'POST'))) }}

{{Form::hidden('id',$entry->id)}}

<!-- cashless form -->
<div class="form-group{{{ $errors->has('cashless') ? ' has-error' : '' }}}" id="cashless-form">
    {{ Form::label('cashless', ' ', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-8">
        {{ Form::checkbox('cashless', 1, Input::old('cashless'), array('class' => '')) }}
        Безналичный расчет
        <span class="text-danger">{{ $errors->first('cashless') }}</span>
    </div>
</div>

<!-- Cost -->
<div class="form-group{{{ $errors->has('cost') ? ' has-error' : '' }}}">
    {{ Form::label('cost', 'Стоимость', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-3 input-group">
        <span class="text-danger">{{ $errors->first('cost') }}</span>
        {{ Form::input('tel', 'cost', Input::old('cost'), array('class' => 'form-control')) }}
        <span class="input-group-addon"> руб.</span>
    </div>
</div>

<!-- Prepay -->
<div class="form-group{{{ $errors->has('prepay') ? ' has-error' : '' }}}" id="prepay-form">
    {{ Form::label('prepay', 'Предоплата', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-3 input-group">
        <span class="text-danger">{{ $errors->first('prepay') }}</span>
        {{ Form::input('tel', 'prepay', Input::old('prepay'), array('class' => 'form-control')) }}
        <span class="input-group-addon"> руб.</span>
    </div>
</div>

<!-- inplace form -->
<div class="form-group{{{ $errors->has('inplace') ? ' has-error' : '' }}}" id="inplace-form">
    {{ Form::label('inplace', ' ', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-7 col-md-6">
        {{ Form::checkbox('inplace', 1, Input::old('inplace'), array('class' => '')) }}
        Ремонт на месте
        <span class="text-danger">{{ $errors->first('inplace') }}</span>
    </div>
</div>

<!-- finish-date-form -->
<div class="form-group{{{ $errors->has('finish_date') ? ' has-error' : '' }}}" id="finish-date-form">
    {{ Form::label('finish_date', 'Дата исполнения заказа', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-7 col-md-6">
        <span class="text-danger">{{ $errors->first('finish_date') }}</span>
        {{ Form::text('finish_date', Input::old('finish_date'), array('class' => 'form-control')) }}
    </div>
</div>

<div class="form-group{{{ $errors->has('stuff') ? ' has-error' : '' }}}">
    {{ Form::label('stuff', 'Материалы', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-7 col-md-6 well">
        <span class="text-danger">{{ $errors->first('stuff') }}</span>
        <div id="stuff-form">
            @if(Input::old('stuff_name'))
            @foreach(Input::old('stuff_name') as $i => $stuff_name)
            {{ addStuffField($i, $stuff_name, Input::old('stuff_quantity')[$i]) }}
            @endforeach
            @endif
        </div>
        <a id="btn-add-stuff" class="btn btn-primary btn-sm" href="#js">Добавить материал</a>
    </div>
</div>

<!-- comment -->
<div class="form-group{{{ $errors->has('comment') ? ' has-error' : '' }}}">
    {{ Form::label('comment', 'Комментарий', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-7 col-md-6">
        <span class="text-danger">{{ $errors->first('comment') }}</span>
        {{ Form::textarea('comment', Input::old('comment'), array('class' => 'form-control', 'size' => '20x4')) }}
    </div>
</div>

<!-- Photo -->
<!--
<div class="form-group{{{ $errors->has('photo') ? ' has-error' : '' }}}">
    {{ Form::label('photo', 'Изображения', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-8 input-group" id="photo-form">
        <span class="text-danger">{{ $errors->first('photo') }}</span>
    </div>
</div>
-->

@include('order.inc.finish_fields_form')


<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить заказ', ['class' => 'btn btn-large btn-primary col-xs-12  col-lg-3']) }}
    </div>
</div>

{{ Form::close() }}

<script>
    $(document).ready(function () {

        $("#finish-fields-form").hide();
        $("#docs-signed-form").hide();

        //addFileInputFieldTo("#photo-form");
        //addStuffFieldTo('#stuff-form');

        $('#btn-add-stuff').click(function () {
            addStuffFieldTo('#stuff-form');
        });

        $('#finish_date').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d',
            timepicker: false,
            closeOnDateSelect: true,
            minDate: 0
        });
        $("#cashless").change(function () {
            if (this.checked) {
                $("#prepay-form").hide();
                $("#prepay-form input").val('');
                $("#result-amount-form").hide();
                $("#result-amount-form input").val('');
                $("#docs-signed-form").show();
            } else {
                $("#docs-signed-form").hide();
                $("#docs-signed-form input:checkbox").prop("checked", false);
                if (!$("#inplace-form input:checkbox").prop("checked")) {
                    $("#prepay-form").show();
                }
                $("#result-amount-form").show();
            }
        });
        $("#inplace").change(function () {
            if (this.checked) {
                $("#finish_date").val(new Date().dateFormat('Y-m-d'));
                //$("#finish_date").prop("disabled", true);
                $("#finish-fields-form").show();
                $("#prepay-form").hide();
                $("#prepay-form input").val('');
            } else {
                $("#finish_date").val('');
                //$("#finish_date").prop("disabled", false);
                $("#finish-fields-form").hide();
                $("#finish-fields-form input").val('');
                if (!$("#cashless-form input:checkbox").prop("checked")) {
                    $("#prepay-form").show();
                }
                $("#docs-signed-form input:checkbox").prop("checked", false);
            }
        });

        function addStuffFieldTo(elem, key, name_val, qty_val) {
            if (key === undefined) {
                key = '';
            }
            if (name_val === undefined) {
                name_val = '';
            }
            if (qty_val === undefined) {
                qty_val = '';
            }
            $('<div class="input-group" style="margin:3px;"><input type="text" name="stuff_name[' + key + ']" placeholder="название" value="' + name_val + '" />&nbsp;<input type="text" name="stuff_quantity[' + key + ']" placeholder="кол-во" size="8" value="' + qty_val + '" />&nbsp;<a class="btn btn-sm btn-danger remove_item" onclick="$(this).parent().remove()" href="#js"> X </a></div>').appendTo(elem);
        }
    });

    function fileInputChanged(elem) {
        if ($(elem).val() !== '') {
            addFileInputFieldTo("#photo-form");
        } else {
            removeFileInputField(elem);
        }
        //$('<input type="file">').change(fileChangeHandler).appendTo(form);
    }

    function addFileInputFieldTo(elem) {
        $('<p><input type="file" name="photo[]" onChange="fileInputChanged(this);" /></p>').appendTo(elem);
    }

    function removeFileInputField(elem) {
        $(elem).parent('p').replaceWith("");
    }

</script>