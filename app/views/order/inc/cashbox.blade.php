<?php
/* @var $order Order */

?>

{{ Form::model($order, array(
                    'url' => $routes['cashbox']['route'],
                    'class' => 'form-horizontal well',
                    'id' => 'order-cashbox-form',
                    'role'=>'form',
                    'method'=>(isset($routes['cashbox']['method'])?$routes['cashbox']['method']:'POST'))) }}

<div class="form-group{{{ $errors->has('cash') ? ' has-error' : '' }}}" id="cash-summ-form">
    {{ Form::label('cash', 'Сдаваемая сумма', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-sm-3 input-group">
        {{ Form::text('cash', Input::old('cash'), array('class' => 'form-control')) }}
        <span class="input-group-addon"> {{$order->currency}}</span>
        <span class="text-danger">{{ $errors->first('cash') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit($routes['cashbox']['name'], array('class' => 'btn btn-large btn-primary')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">Отмена</button>
    </div>
</div>

{{ Form::close() }}



<script>
    $(document).ready(function () {

        var order = <?= $order->toJson() ?>;
        $("#order-cashbox-form").hide();
        $('#data-loading').hide();
        $("#results").hide();
        if (<?= $order->cashbox_debt ?>) {
            $("#close-button").hide();
        } else {
            $("#cashbox-button").hide();
        }

        $("#cashbox-button").click(function () {
            $(this).hide();
            //close_all_forms();
            $("#results").hide();
            $("#order-cashbox-form").show();
            $("#cash").val(function () {
                $(this).val() ? $(this).val() : '';
            });
        });
        $("#cashbox_debt").bind('DOMNodeInserted', function () {
            //alert('cashbox_debt changed');
            if ($("#cashbox_debt span").text() == '0') {
                $("#cashbox-button").prop("disabled", true);
                //alert("#cashbox_debt == 0");
                $("#close-button").show();
            }
        });
        $("#cash").on('input', function () {
            sum = (order.cost - order.cashbox - $(this).val());
            if (sum < 0) {
                alert('Вносимая сумма превышает сумму долга');
                $("#order-cashbox-form :submit").prop("disabled", true);
            } else {
                $("#order-cashbox-form :submit").prop("disabled", false);
                $("#cashbox_debt span").text(sum);
            }
        });
        $("#order-cashbox-form").submit(function () {
            $('#data-loading').show();
            $(this).hide();
            $.ajax({
                type: "POST",
                data: $(this).serialize(),
                dataType: "json",
                cache: false,
                url: '<?= $routes['cashbox']['route'] ?>',
                success: function (data) {
                    if (data.error === false) {
                        order.cashbox = data.data.cashbox;
                        $("#cashbox span").text(data.data.cashbox);
                        $("#cashbox_debt span").text(data.data.cashbox_debt);
                        if (data.data.cashbox_debt == 0) {
                            window.location.replace("<?= route('order.show',
    $order->getKey()) ?>");
                        }

                        $("#results").text('Внесено в кассу: ' + data.data.added + ' ' + '<?= $order->currency ?>')
                                .removeClass('alert-danger')
                                .addClass('alert-success')
                                .show();
                    } else {
                        $("#results").html('Произошла ошибка: ' + data.msg)
                                .removeClass('alert-success')
                                .addClass('alert-danger')
                                .show();
                    }
                    $('#data-loading').hide();
                    $("#cashbox-button").show();
                },
                error: function (jqXHR, textStatus) {
                    $("#results").html('Произошла ошибка: сервер вернул статус ' + textStatus).show();
                    $('#data-loading').hide();
                    $("#cashbox-button").show();
                }
            });
            return false;
        });
    });
</script>