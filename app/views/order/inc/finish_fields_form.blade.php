<div id="finish-fields-form">

    <!-- result_amount -->
    <div class="form-group{{{ $errors->has('result_amount') ? ' has-error' : '' }}}" id="result-amount-form">
        {{ Form::label('result_amount', trans('order.result_amount'), array('class' => 'col-sm-4 control-label')) }}
        <div class="col-sm-3 input-group">
            {{ Form::text('result_amount', Input::old('result_amount'), array('class' => 'form-control')) }}
            <span class="input-group-addon">  руб.</span>
            <span class="text-danger">{{ $errors->first('result_amount') }}</span>
        </div>
    </div>

    <!-- docs_signed form -->
    <div class="form-group{{{ $errors->has('docs_signed') ? ' has-error' : '' }}}" id="docs-signed-form">
        {{ Form::label('docs_signed', ' ', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-sm-8">
            {{ Form::checkbox('docs_signed', 1, Input::old('docs_signed'), array('class' => '')) }}
            Документы подписаны
            <span class="text-danger">{{ $errors->first('docs_signed') }}</span>
        </div>
    </div>

</div>