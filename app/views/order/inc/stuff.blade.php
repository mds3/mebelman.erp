{{ Form::model($order, array(
                    'url' => $routes['stuff']['route'],
                    'class' => 'form-horizontal well',
                    'id' => 'stuff-form',
                    'role'=>'form'
                    )) }}

@include('order.inc.stuff_fields')

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-large btn-primary')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">Отмена</button>
    </div>
</div>

{{ Form::close() }}