<div class="form-group{{{ $errors->has('stuff') ? ' has-error' : '' }}}">
    <div class="col-md-8" style="margin-left:10px;">
        <span class="text-danger">{{ $errors->first('stuff') }}</span>
        <div id="stuff-form">
            @if(Input::old('stuff_name'))
            @foreach(Input::old('stuff_name') as $i => $stuff_name)
            {{ addStuffField($i, $stuff_name, Input::old('stuff_quantity')[$i], Input::old('stuff_cost')[$i]) }}
            @endforeach
            @elseif(isset($order->stuff) && $order->stuff)
            @foreach($order->stuff as $i => $stuff)
            {{ addStuffField($i, $stuff->name, $stuff->quantity, $stuff->cost, $stuff->id) }}
            @endforeach
            @endif
        </div>
        <a id="btn-add-stuff" class="btn btn-primary btn-sm" href="#js">Добавить материал</a>
    </div>
</div>


<script>
    $(document).ready(function () {

        $('#btn-add-stuff').click(function () {
            addStuffFieldTo('div#stuff-form');
        });

        $('.remove_item').click(function () {
            $(this).parent().remove();
        });

        function addStuffFieldTo(elem, key, name_val, qty_val, cost_val) {
            if (key === undefined) {
                key = '';
            }
            if (name_val === undefined) {
                name_val = '';
            }
            if (qty_val === undefined) {
                qty_val = '';
            }
            if (cost_val === undefined) {
                cost_val = 0;
            }
            $('<div class="input-group" style="margin:3px;">'
                    + '<input type="text" name="stuff_name[' + key + ']" placeholder="название" value="' + name_val + '" />'
                    + '&nbsp;<input type="text" name="stuff_quantity[' + key + ']" placeholder="кол-во" size="8" value="' + qty_val + '" />'
                    + '&nbsp;<input type="hidden" name="stuff_cost[' + key + ']" value="' + cost_val + '" />'
                    + '&nbsp;<a class="btn btn-sm btn-danger remove_item" onclick="$(this).parent().remove()" href="#js"> X </a>'
                    + '</div>').appendTo(elem);
        }
    });
</script>
<?php

function addStuffField($key, $name_val, $qty_val, $cost_val, $id = null)
{
    return '<div class="input-group" style="margin:3px;">'
        . '<input type="text" name="stuff_name[' . $key . ']" placeholder="название" value="' . $name_val . '" />'
        . '&nbsp;<input type="text" name="stuff_quantity[' . $key . ']" placeholder="кол-во" size="8" value="' . $qty_val . '" />'
        . '&nbsp;<input type="hidden" name="stuff_cost[' . $key . ']" placeholder="цена" size="8" value="' . $cost_val . '" />'
        . '&nbsp;<a class="btn btn-sm btn-danger remove_item" href="#js"> X </a>'
        . '&nbsp;<input type="hidden" name="id[' . $key . ']" value="' . $id . '" />'
        . '</div>';
}
