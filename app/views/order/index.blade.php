@extends('includes.extends_master')

{{-- Content --}}
@section('content')


{{ HTML::style('/css/dropdowns-enhancement.css') }}
{{ HTML::script('/js/dropdowns-enhancement.js') }}

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <b>
            {{{ trans('msg.orders') }}}
        </b> <small>{{{ $orders->count() }}} из {{{ $orders->getTotal() }}}</small>
        <?php
        $btns = [
          'list' => 'th',
          'table' => 'th-list',
        ];

        ?>
        <div class="btn-group" role="group" aria-label="..." style="margin-left:10px;">
            @foreach($btns as $k => $btn)
            <button type="button" id="btn-<?= $k ?>" class="btn btn-default <?=
            \Session::get('index_view', 'list') == $k ? ' active' : ''

            ?>">
                <span class="glyphicon glyphicon-<?= $btn ?>" aria-hidden="true"></span>
            </button>
            @endforeach
        </div>

        @if(isset($routes['search']))
        <a href="{{$routes['search']['route']}}">
            <span class="glyphicon glyphicon-search pull-right"></span>
        </a>
        @endif
    </div>

    <div class="panel-body">
        <form method="GET" action="">

            <?php
            $col = floor(12 / count($options));

            ?>
            <div class="row">
                @foreach($options as $k => $option)
                <div class="col-md-<?= $col ?>">
                    <select name="{{$k}}" id="{{$k}}" class="form-control" onChange="this.form.submit()">
                        @foreach($option as $key => $val)
                        <option value="{{$key}}"
                        <?=
                        isset($input[$k]) && $input[$k] == $key ? ' selected="selected"'
                                : '';

                        ?>
                                >{{$val}}</option>
                        @endforeach
                    </select>
                </div>
                @endforeach
            </div>
        </form>
    </div>

    @if(!$orders->count())
    <div class="panel-body">
        <h4 class="text-warning">
            {{ trans('order.orders_not_found') }}
        </h4>
    </div>
    @else
    <table class="table table-hover" id="table-view">
        <thead>
            <tr>
                <th>{{ trans('msg.num') }}</th>
                <th>{{ trans('msg.title') }}</th>
                <th>{{ trans('order.cost').', '.trans('msg.cur') }}</th>
                @foreach($cells as $cell => $func)
                <th>{{trans("order.$cell")}}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <td>
                    <a class="btn btn-default" style="margin:0;padding-bottom:3px;" href="{{ $order->url }}">
                        <span class="glyphicon glyphicon-{{ $order->getIcon() }}"></span>
                        {{ $order->order_key_print }}<br/>
                        <small>
                            {{{ trans('msg.from') }}} {{date("j.m.Y",strtotime($order->created_at))}}
                        </small>
                        <br/>
                        <small>
                            {{{ $order->cost.' '.$order->currency }}}
                        </small>
                    </a>
                </td>
                <td>
                    {{{ $order->title }}}<br/>
                    @if($order->metro)
                    <span class="text-info">
                        {{{ trans('msg.metro_pref').$order->metro.' ' }}}
                    </span>
                    @endif
                    @if($order->mo_town)
                    <span class="text-warning">
                        {{{ trans('msg.town_pref').$order->mo_town }}}
                    </span>
                    @endif
                    @if(!$order->mo_town && !$order->metro)
                    -
                    @endif
                </td>
                <td>
                    {{ $order->cost }}
                </td>
                @foreach($cells as $cell => $func)
                <?php
                $var = object_get($order, $cell, '-');
                if ($func) {
                    $func = explode(":", $func);
                    //echo '<pre>', print_r($func), '</pre>';
                    $var = $func[0] == 'with' ? $func[0]($var) . ' ' . $order->{$func[1]}
                            : $func[0]($var);
                }

                ?>
                <td>{{ $var }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>


    <!-- Кнопочный интерфейс -->
    <div id="list-view">
        @foreach ($orders as $order)
        <div class="row">
            <a class="btn btn-default order-btn col-md-6 col-xs-12" href="{{ $order->url }}">
                <div class="panel panel-{{$order->finish_status}} order-panel text-left">
                    <div class="panel-heading" style="">
                        <span class="glyphicon glyphicon-{{ $order->getIcon() }}"></span>
                        {{ $order->order_key_print }}
                        <small>
                            {{{ trans('msg.from') }}} {{date("j.m.Y",strtotime($order->created_at))}}
                        </small>
                        /
                        {{{ $order->master->name }}}
                        /
                        {{{ $order->cost.' '.$order->currency }}}
                    </div>

                    <div class="panel-body">
                        <div>
                            {{{ $order->title }}}<br/>
                            @if($order->metro)
                            <span class="text-info">
                                {{{ trans('msg.metro_pref').$order->metro.' ' }}}
                            </span>
                            @endif
                            @if($order->mo_town)
                            <span class="text-warning">
                                {{{ trans('msg.town_pref').$order->mo_town }}}
                            </span>
                            @endif
                            @if(!$order->mo_town && !$order->metro)
                            -
                            @endif
                        </div>

                        @if($order->is_archived)
                        <div class="text-{{$order->finish_status}}">
                            @if($order->is_canceled)
                            {{ trans('order.statuses.canceled').' '.human_date_time($order->canceled_at, true) }}
                            @else
                            {{ trans('order.statuses.archived').' '.human_date_time($order->archived_at, true) }}
                            @endif
                        </div>
                        @elseif($order->is_finished)
                        <div class="text-{{ $order->finish_status }}">
                            {{ trans('order.statuses.'.strtolower($order->status)).' '.human_date($order->finished_time, true) }}
                        </div>
                        @elseif($order->is_deferred)
                        <div class="text-warning">
                            Отложена до {{ human_date($order->defer_before, true) }}<br/>
                            {{ $order->defer_reason }}
                        </div>
                        @elseif($order->is_refused)
                        <div class="text-danger">
                            Отказ {{ human_date_time($order->refuse_time, true) }}
                            @if(!empty($order->refuse_reason))
                            {{ \nl2br($order->refuse_reason) }}
                            @endif
                        </div>
                        @elseif(!$order->is_archived && !$order->is_finished)
                        <div class="text-{{ $order->finish_status }}">
                            {{ trans('order.finish_'.$order->finish_status) }}
                            {{{ human_date($order->finish_date, true) }}}
                        </div>

                        @endif

                    </div>

                </div>
            </a>
        </div>
        @endforeach
    </div>
    <!-- конец - Кнопочный интерфейс -->

    <!-- пагинация -->
    {{ $orders->links() }}

    @endif
</div>

@if(isset($routes['export']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-primary col-xs-12 col-md-3" href="{{$routes['export']['route']}}">
        {{$routes['export']['name']}}
    </a>
</div>
@endif



<div class="well">
    @foreach($icons as $key => $val)
    <span class="glyphicon glyphicon-{{ $val }}"></span> - {{ trans('order.statuses.' . strtolower($key)) }}<br/>
    @endforeach
</div>

@include('includes.index_view')

@stop