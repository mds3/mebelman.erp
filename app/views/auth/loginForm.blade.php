@extends('includes.extends_master')

{{-- Content --}}
@section('content')

    <div class="panel panel-default col-md-4 col-md-push-4 text-center">

        <div class="panel-heading">
            <h3 class="panel-title">
                <b class="text-success">
                    {{ \Config::get('app.name') }}:
                </b> {{ trans('msg.need_auth') }}</h3>
        </div>
        <div class="panel-body">

        {{ Form::open(array('route' => $route, 'class' => 'form-horizontal')) }}

        <!-- Name -->
            <div class="form-group{{{ $errors->has('user_email') ? ' has-error' : '' }}}">
                {{ Form::label('user_email', trans('msg.email'), array('class' => 'control-label')) }}
                <div>
                    {{ Form::email('user_email', Input::old('user_email'), array('class' => 'form-control', 'placeholder' => trans('msg.email'))) }}
                    <span class="text-danger">{{ $errors->first('user_email') }}</span>
                </div>
            </div>

            <!-- Password -->
            <div class="form-group{{{ $errors->has('user_password') ? ' has-error' : '' }}}">
                {{ Form::label('user_password', trans('msg.pass'), array('class' => 'control-label')) }}
                <div>
                    {{ Form::password('user_password', array('class' => 'form-control', 'placeholder' => trans('msg.pass'))) }}
                    <span class="text-danger">{{ $errors->first('user_password') }}</span>
                </div>
            </div>

            <!-- Login button -->
            <div class="form-group">
                <div class="">
                    {{ Form::submit('Авторизоваться', ['class' => 'btn btn-large btn-primary', 'style' => 'padding:20px;font-weight:bold;']) }}
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </div>


@stop
