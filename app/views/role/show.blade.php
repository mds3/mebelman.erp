@extends('includes.extends_master')

{{-- Content --}}
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title">
            {{ trans('role.role') }}
        </h2>
    </div>
    <div class="panel-body">
        <h3>{{{$role->name}}}</h3>
        @include('role.inc.role_permissions_list')
        @include('includes.control_unit',[
        'edit_route' => 'role.edit',
        'delete_route' => 'role.destroy',
        'model' => $role,
        ])
    </div>
</div>
@stop