@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if(route_allowed('role.create'))
<div class="row">
    <a class="btn btn-large btn-primary col-sm-4" href="
    <?= URL::route('role.create') ?>
       ">
        {{ trans('role.create')}}
    </a>
</div>
@endif

<?php /* @var $role \Role */ ?>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        {{ trans('role.roles') }}
    </div>

    <div class="list-group">
        @foreach ($roles as $role)
        <a href="{{URL::route('role.show', $role->id, false)}}" class="list-group-item">
            <h4 class="list-group-item-heading">{{{ $role->name }}}</h4>
            <p class="list-group-item-text"></p>
            <p class="list-group-item-text">
                {{--@include('role.inc.role_permissions_list')--}}
            </p>
        </a>
        @endforeach
    </div>
</div>

<?php /*
  foreach ($users as $key => $user) {
  echo $user->name . ' (' . $user->role->name . ')<br/>';
  echo '<a href="' . URL::route('user.edit', ['id' => $user['id']], false) . '">Править</a> '
  . '| <a href="' . URL::route('user.destroy', ['id' => $user['id']], false) . '">Удалить</a><br/>';
  echo ' <hr/>';
  }

 */ ?>
@stop