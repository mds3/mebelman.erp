<?php $arr = $role->makePermissionsStructList($role->permissions); ?>
<i>Обладает правами:</i>
<ul>
    @foreach ($arr as $name => $group)
    <fieldset>
        <legend>{{{ trans('msg.'.$name) }}}</legend>
        @foreach ($group as $perm)
        <li>{{{ $perm }}}</li>
        @endforeach
    </fieldset>
    @endforeach
</ul>