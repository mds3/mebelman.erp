@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php //echo '<pre>', print_r($role->permissions->toArray()), '</pre>'; ?>

{{ Form::model($role, array('route' => $route, 'class' => 'form-horizontal','role'=>'form','method'=>(isset($method)?$method:'POST'))) }}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $page_title }}</h3>
    </div>
    <div class="panel-body">

        <!-- Name -->
        <div class="form-group{{{ $errors->has('name') ? ' has-error' : '' }}}">
            {{ Form::label('name', trans('msg.name_title'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <div class="form-group{{{ $errors->has('permissions') ? ' has-error' : '' }}}">
            {{ Form::label('permissions', trans('msg.permissions'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <?php $arr = $role->makePermissionsStructList($permissions); ?>


                @foreach ($arr as $name => $group)
                <fieldset>
                    <legend>{{{ trans('msg.'.$name) }}}</legend>
                    @foreach ($group as $id => $perm)
                    <div class="form-group-sm">
                        {{ Form::checkbox("permission_id[]", $id, $role->permissions->contains($id)) }}
                        {{ $perm }}
                    </div>
                    @endforeach
                </fieldset>
                @endforeach

                <span class="text-danger">{{ $errors->first('permissions') }}</span>

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                {{ Form::submit(trans('msg.save'), array('class' => 'btn btn-large btn-primary')) }}
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}

@stop