@extends('includes.extends_master')

{{-- Content --}}
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Пользователь</h3>
    </div>
    <div class="panel-body">
        <dl>
            <dt>{{{$user->name}}}</dt>
            <dd>
                @foreach($user->roles as $role)
                <p class="list-group-item-text text-primary"><i>{{{$role->name}}}</i></p>
                @endforeach
            </dd>
            <dd>{{{$user->phone}}}</dd>
            <dd>{{{$user->email}}}</dd>
            <dd>{{$user->{\User::CREATED_AT};}}</dd>
        </dl>
        @include('includes.control_unit',[
        'edit_route' => 'user.edit',
        'delete_route' => 'user.destroy',
        'model' => $user,
        ])
    </div>
</div>
@stop