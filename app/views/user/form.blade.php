@extends('includes.extends_master')

{{-- Content --}}
@section('content')

{{ Form::model($user, array('route' => $route, 'class' => 'form-horizontal','role'=>'form','method'=>(isset($method)?$method:'POST'))) }}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$page_title}}</h3>
    </div>
    <div class="panel-body">

        <!-- Name -->
        <div class="form-group{{{ $errors->has('name') ? ' has-error' : '' }}}">
            {{ Form::label('name', 'Имя', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <!-- Email -->
        <div class="form-group{{{ $errors->has('email') ? ' has-error' : '' }}}">
            {{ Form::label('email', 'Email', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
        </div>

        <!-- Phone -->
        <div class="form-group{{{ $errors->has('phone') ? ' has-error' : '' }}}">
            {{ Form::label('phone', 'Телефон', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::input('tel', 'phone', Input::old('phone'), ['class' => 'form-control bfh-phone','data-format'=>get_phone_format()]) }}
                <span class="text-danger">{{ $errors->first('phone') }}</span>
            </div>
        </div>

        <!-- Role -->
        <div class="form-group">
            <label class="col-sm-4 control-label" for="role_id">Роль</label>
            <div class="col-sm-8">
                <fieldset>
                    <span class="text-danger">{{ $errors->first('role_id') }}</span>
                    @foreach ($roles as $role_id => $role_name)
                    <div class="form-group-sm">
                        {{ Form::checkbox("role_id[]", $role_id, $user->roles->contains($role_id)) }}
                        {{ $role_name }}
                    </div>
                    @endforeach
                </fieldset>
            </div>
        </div>

        <!-- Password -->
        <div class="form-group{{{ $errors->has('password') ? ' has-error' : '' }}}">
            {{ Form::label('password', 'Пароль', array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                {{ Form::text('password', '', array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('password') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                {{ Form::submit('Сохранить', array('class' => 'btn btn-large btn-primary')) }}
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}

{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}

@stop