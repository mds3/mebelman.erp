@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if(route_allowed('user.create'))
<div class="row">
    <a class="btn btn-large btn-primary col-sm-4" href="<?=
    URL::route('user.create', null, false)

    ?>">
        Добавить нового пользователя
    </a>
</div>
@endif

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Список пользователей</div>

    <div class="list-group">
        @foreach ($users as $user)
        <a href="{{URL::route('user.show', $user->id, false)}}" class="list-group-item<?=
        $user->id == \Auth::user()->id ? ' active' : ''

        ?>">
            <h4 class="list-group-item-heading">{{{$user->name}}}</h4>
            @foreach($user->roles as $role)
            <p class="list-group-item-text text-primary"><i>{{{$role->name}}}</i></p>
            @endforeach
            <p class="list-group-item-text">{{{$user->phone}}}</p>
            <p class="list-group-item-text">{{{$user->email}}}</p>
        </a>
        @endforeach
    </div>
</div>

<?php /*
  foreach ($users as $key => $user) {
  echo $user->name . ' (' . $user->role->name . ')<br/>';
  echo '<a href="' . URL::route('user.edit', ['id' => $user['id']], false) . '">Править</a> '
  . '| <a href="' . URL::route('user.destroy', ['id' => $user['id']], false) . '">Удалить</a><br/>';
  echo ' <hr/>';
  }

 */ ?>
@stop