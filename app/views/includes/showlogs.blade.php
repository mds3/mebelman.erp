
<a id="btn-logs" class="btn btn-primary btn-sm" href="#js">{{$route['name']}}</a>

<div id="logs-area" class="well">
    <div id="logs"></div>
    <div id="logs-loading">
        <h4><img src="/img/ajax-loader.gif"/> Данные загружаются</h4>
    </div>
    <a id="btn-logs-hide" class="btn btn-primary btn-sm" href="#js">Скрыть</a>
</div>

<script>
    $(document).ready(function () {
        $("#btn-logs-hide").hide();
        $('#logs-loading').hide();
        $("#logs").hide();
        $("#btn-logs").click(function () {
            //$('#modal-loading').modal('show');
            $('#logs-loading').show();
            $("#btn-logs").hide();
            $.ajax({
                url: '<?= $route['route'] ?>',
                dataType: 'json',
                success: function (data) {
                    var str = [];
                    if (data.logs.length > 0) {
                        $.each(data.logs, function (k, log) {
                            var _str = [];
                            var date = new Date(log.created_at);
                            _str.push('<dl style="margin:10px;" class="details"><dt class="text-success">');
                            _str.push(log.creator.name + '<br/><small class="text-warning">' + date.toLocaleDateString() + ' в ' + date.toLocaleTimeString() + '</small>');
                            _str.push('</dt><dd><ul>');
                            $.each(log.items, function (key, value) {
                                _str.push('<li>' + value + '</li>');
                            });
                            _str.push('</ul></dd></dl>');
                            str.push(_str.join('').replace(/(?:\r\n|\r|\n)/g, '<br />'));
                        });
                        $('#logs').html(str.join('<br/>'));
                    } else {
                        $('#logs').html('Изменений не зафиксировано.');
                    }
                    $('#logs').show();
                    //$('#modal-loading').modal('hide');
                    $('#logs-loading').hide();
                    $("#btn-logs-hide").show();
                }
            });
            return false;
        });
        $("#btn-logs-hide").click(function () {
            $("#logs").hide();
            $('#logs').empty();
            $(this).hide();
            $("#btn-logs").show();
        });
    });
</script>