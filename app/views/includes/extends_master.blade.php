@extends('layouts.master')

@section('title')
@parent
/ {{{ $page_title}}}
@stop

@section('content')
<?php /* @var $errors Illuminate\Support\ViewErrorBag */ ?>
@if($errors->count())
<div class="alert alert-danger alert-block">
    На странице имеются ошибки: {{{ $errors->first() }}}
</div>
@endif