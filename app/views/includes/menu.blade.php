<?php
$menu = [
  'entry.index' => trans('msg.entries'),
  'order.index' => trans('msg.orders'),
  'cashbox.index' => trans('msg.cashbox'),
  'report.index' => trans('msg.reports'),
  'system' => [
    'user.index' => trans('msg.users'),
    'role.index' => trans('msg.roles'),
    'refer.index' => trans('msg.refers'),
  ],
  'help.index' => trans('msg.help'),
];

//echo '<pre>', print_r($menu), '</pre>';

foreach ($menu as $route => $name) {
    if (is_array($name)) {
        foreach ($name as $sub_route => $sub_name) {
            if (!route_allowed($sub_route)) {
                unset($menu[$route][$sub_route]);
            }
        }
    } else {
        if (!route_allowed($route)) {
            unset($menu[$route]);
        }
    }
}

$menu += [
  'logout' => trans('msg.logout'),
];

// удаляем пустые dropdowns
foreach ($menu as $route => $name) {
    if (is_array($name) && empty($name)) {
        unset($menu[$route]);
    }
}

$dir = explode('.', \Route::getCurrentRoute()->getName())[0];

$class = isset($class) ? $class : 'nav navbar-nav';

?>
<ul class="<?= $class ?>">
    <?php
    foreach ($menu as $route => $name):
        if (is_array($name)) {

            ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ trans('msg.'.$route) }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <?php foreach ($name as $sub_route => $sub_name) : ?>
                        <li<?= explode('.', $sub_route)[0] == $dir ? ' class="active"' : '' ?>>
                            <a href="<?= URL::route($sub_route, null, false) ?>">
                                <?= $sub_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php } else { ?>
            <li<?= explode('.', $route)[0] == $dir ? ' class="active"' : '' ?>>
                <a href="<?= URL::route($route, null, false) ?>">
                    <?= $name ?>
                </a>
            </li>
        <?php } ?>
    <?php endforeach; ?>
</ul>