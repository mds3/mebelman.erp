<div class="btn-group">
    @if(route_allowed($edit_route))
    <a class="btn btn-primary btn-sm" href="{{URL::route($edit_route, $model->id, false)}}">Править</a>
    @endif
    @if(route_allowed($delete_route))
    {{ Form::deleteButton([$delete_route, $model->id],"Удалить ".(isset($delete_msg)?$delete_msg:$model->name)." безвозвратно?") }}
    @endif
</div>