<script>
    $(document).ready(function () {

        $("#list-view").hide();
        $("#table-view").hide();
        $("#<?= \Session::get('index_view', 'list') ?>-view").show();

        $("#btn-list").click(function () {
            $("#btn-list").addClass('active');
            $("#btn-table").removeClass('active');
            $("#list-view").show();
            $("#table-view").hide();

            $.ajax({
                url: '<?= route('auth.settings.put') ?>',
                type: 'PUT',
                data: "_token=<?= csrf_token() ?>&index_view=list",
                success: function (data) {
                }
            });
        });

        $("#btn-table").click(function () {
            $("#btn-table").addClass('active');
            $("#btn-list").removeClass('active');
            $("#table-view").show();
            $("#list-view").hide();
            $.ajax({
                url: '<?= route('auth.settings.put') ?>',
                type: 'PUT',
                data: "_token=<?= csrf_token() ?>&index_view=table",
                success: function (data) {
                }
            });
        });

    });

</script>