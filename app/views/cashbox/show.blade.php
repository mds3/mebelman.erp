@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $order->getIcon() }}"></span>
            {{ $order->order_key_print }} / {{{ $order->master->name }}}
        </h3>
    </div>
    <div class="panel-body">

        <div style="padding-left:5px;">
            <small class="text-warning">
                {{ human_date($order->created_at, true) }}
            </small>
            <br/>
            <h4 style="margin-top:0;">
                {{{$order->title}}}
            </h4>

            @if($order->mo_town || $order->metro)
            <p style="margin-top:0;">
                @if($order->metro)
                <span class="text-info">
                    {{{ trans('msg.metro_pref').$order->metro.' ' }}}
                </span>
                @endif
                @if($order->metro && $order->mo_town)
                /
                @endif
                @if($order->mo_town)
                <span class="text-warning">
                    {{{ trans('msg.town_pref').$order->mo_town }}}
                </span>
                @endif
            </p>
            @endif

            @if($order->payments->count())
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Дата платежа</th>
                        <th>Сумма</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order->payments as $payment)
                    <tr<?= $payment->id ? '' : ' class="warning"' ?>>
                        <td>{{ isset($payment->created_at ) ? human_date_time($payment->created_at, true) : '-' }}</td>
                        <td>{{{ isset($payment->summ ) ? $payment->summ.' '.$payment->currency : '-' }}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <div class="panel-body">
                <h4 class="text-warning">
                    {{ trans('order.no_payments') }}
                </h4>
            </div>
            @endif

        </div>

    </div>
</div>

<!-- Order link -->
<div class="col-lg-3">
    @if(isset($routes['order']))
    <a href="{{ $routes['order']['route'] }}" type="button" class="btn btn-default btn-block">
        {{ $routes['order']['name'] }}
    </a>
</div>
@endif


@stop