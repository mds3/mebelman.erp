@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
//echo '<pre>', print_r($payments->toArray()), '</pre>';
//echo '<pre>', print_r($selects), '</pre>';
//echo '<pre>', print_r($input), '</pre>';

?>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        Список платежей
        @if(isset($routes['search']))
        <a href="{{$routes['search']['route']}}">
            <span class="glyphicon glyphicon-search pull-right">
                {{--$routes['search']['name']--}}
            </span>
        </a>
        @endif
    </div>

    @if(!empty($options))
    <div class="panel-body">
        <form method="GET" action="">
            <div class="row">
                <?php $col = floor(12 / count($options)); ?>
                @foreach($options as $k => $option)
                <div class="col-md-<?= $col ?>">
                    <select name="{{$k}}" id="{{$k}}" class="form-control" onChange="this.form.submit()">
                        @foreach($option as $key => $val)
                        <option value="{{$key}}"
                        <?=
                        isset($input[$k]) && $input[$k] == $key ? ' selected="selected"'
                                : '';

                        ?>
                                >{{$val}}</option>
                        @endforeach
                    </select>
                </div>
                @endforeach
            </div>
        </form>
    </div>
    @endif

    @if($payments->count())
    <table class="table table-hover">
        <thead>
            <tr>
                <th>{{ trans('cashbox.order_key') }}</th>
                <th>{{ trans('cashbox.summ') }}</th>
                <th>{{ trans('cashbox.type') }}</th>
                <th>{{ trans('cashbox.created_at') }}</th>
                <th>{{ trans('cashbox.master') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($payments as $payment)
            <tr<?= $payment->id ? '' : ' class="warning"' ?>>
                <td>
                    @if($payment->id)
                    <span class="glyphicon glyphicon-{{ $payment->order->getIcon() }}"></span>
                    <a href="{{ $payment->url }}">{{{$payment->order->getKey()}}}</a><br/>
                    <small>от {{date("j.m.Y",strtotime($payment->order->created_at))}}</small>
                    @else
                    <b>Итого:</b>
                    @endif
                </td>
                <td>{{{ isset($payment->summ )? $payment->summ : '-' }}}</td>
                <td>{{ isset($payment->cash )? trans('cashbox.'.$payment->cash) : '-' }}</td>
                <td>{{ isset($payment->created_at )? human_date_time($payment->created_at) : '-' }}</td>
                <td>{{{ isset($payment->order->master->name )? $payment->order->master->name : '-' }}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="panel-body">
        <div class="panel-body">
            <h4 class="text-warning">
                {{ trans('cashbox.no_payments') }}
            </h4>
        </div>
    </div>
    @endif
</div>

@if(isset($routes['export']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-primary col-xs-12 col-md-3" href="{{$routes['export']['route']}}">
        {{$routes['export']['name']}}
    </a>
</div>
@endif

@stop