@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
//echo '<pre>', print_r($payments->toArray()), '</pre>';
//echo '<pre>', print_r($selects), '</pre>';
//echo '<pre>', print_r($input), '</pre>';

?>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        Отчеты
        @if(isset($routes['search']))
        <a href="{{$routes['search']['route']}}">
            <span class="glyphicon glyphicon-search pull-right">
                {{--$routes['search']['name']--}}
            </span>
        </a>
        @endif
    </div>

    @if(!empty($selects))
    <?php $col = floor(12 / (count($selects) + 1)); ?>
    <div class="panel-body">
        <form method="GET" action="">
            <div class="row">
                @foreach($selects as $k => $option)
                <div class="col-md-<?= $col ?>">
                    <select name="{{$k}}" id="{{$k}}" class="form-control">
                        @foreach($option as $key => $val)
                        <option value="{{$key}}"
                        <?=
                        isset($input[$k]) && $input[$k] == $key ? ' selected="selected"'
                                : '';

                        ?>
                                >{{$val}}</option>
                        @endforeach
                    </select>
                </div>
                @endforeach
                <div class="col-md-<?= $col ?>">
                    {{ Form::submit('Показать', array('class' => 'btn btn-large btn-primary')) }}
                </div>
            </div>
        </form>
    </div>
    @endif

    @if($reports->count())
    <table class="table table-hover">
        <thead>
            <tr>
                @foreach ($fields as $field)
                <th>{{ $field }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($reports as $report)
            <tr<?= $report->user_id ? '' : ' class="warning"' ?>>
                <td>
                    @if($report->user_id)
                    <a href="{{ $report->link_url }}">{{{$report->name}}}</a>
                    @else
                    <b>{{{$report->name}}}</b>
                    @endif
                </td>
                <td>{{{ $report->entryCnt }}}</td>
                <td>{{{ $report->orderCnt }}}</td>
                <td>{{{ $report->orderSumm }}}</td>
                <td>{{{ $report->prepaySumm }}}</td>
                <td>{{{ $report->amountSumm }}}</td>
                <td>{{{ $report->cashboxSumm }}}</td>
                <td>{{{ $report->masterDebt }}}</td>
                <td>{{{ $report->clientDebt }}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="panel-body">
        <h4 class="text-warning">
            {{ trans('report.no_reports') }}
        </h4>
    </div>
    @endif
</div>

@if(isset($routes['export']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-primary col-xs-12 col-md-3" href="{{$routes['export']['route']}}">
        {{$routes['export']['name']}}
    </a>
</div>
@endif

@stop