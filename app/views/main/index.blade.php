@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if($route = $data->getRoute('entry.create'))
<div class="nav">
    <a href="{{ $route->route }}" class="btn btn-large btn-success col-xs-12 col-md-4">
        {{ $route->name }}
    </a>
</div>
@endif

<ul class="nav nav-pills nav-stacked col-xs-12 col-md-4">
    <li style="text-align:center;">
        <h4>
            <?= \strftime('%d %b %Y %A %H:%M'); ?>
        </h4>
    </li>
    @foreach($data->getOptions() as $key => $option)
    <li class="<?= $option['count'] ? 'active' : 'alert-link' ?>" role="navigation">
        <a href="{{ $option['route'] }}">
            <span class="glyphicon glyphicon-{{ $option['icon'] }}"></span>
            {{ $option['name'] }}
            <span class="pull-right">
                <b>{{ $option['count'] }}</b>
                {{ isset($option['str']) ? ' '.$option['str'] : '' }}
            </span>
        </a>
    </li>
    @endforeach
</ul>
@stop