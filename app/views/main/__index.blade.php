@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if(isset($routes['entry.create']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-success col-xs-12 col-md-3" href="{{$routes['entry.create']['route']}}">
        {{$routes['entry.create']['name']}}
    </a>
</div>
@endif

<?php
//echo '<pre>', print_r($options), '</pre>';

?>

<ul class="nav nav-pills nav-stacked col-xs-12 col-md-4">
    @foreach($options as $name => $option)
    <li<?= $option['count'] ? ' class="active"' : ' class="alert-info"' ?> role="navigation">
        <a href="{{ $option['route'] }}">
            {{{ $name }}}
            <span class="pull-right"><b>{{ $option['count'] }}</b></span>
        </a>
    </li>
    @endforeach
</ul>
@stop