@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@foreach($sections as $key => $section)
<h4><?= trans('msg.' . $key) ?></h4>
{{ nl2br($section['all']) }}<br/>
@endforeach

@stop