@extends('includes.extends_master')

{{-- Content --}}
@section('content')

@if(isset($routes['entry.create']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-primary col-xs-12 col-md-3" href="{{$routes['entry.create']['route']}}">
        {{$routes['entry.create']['name']}}
    </a>
</div>
@endif

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <b>
            {{{ trans('msg.entries') }}}
        </b> <small>{{{ $entries->count() }}} из {{{ $entries->getTotal() }}}</small>
        <?php
        $btns = [
          'list' => 'th',
          'table' => 'th-list',
        ];

        ?>
        <div class="btn-group" role="group" aria-label="..." style="margin-left:10px;">
            @foreach($btns as $k => $btn)
            <button type="button" id="btn-<?= $k ?>" class="btn btn-default <?=
            \Session::get('index_view', 'list') == $k ? ' active' : ''

            ?>">
                <span class="glyphicon glyphicon-<?= $btn ?>" aria-hidden="true"></span>
            </button>
            @endforeach
        </div>

        @if(isset($routes['search']))
        <a class="btn btn-default pull-right" href="{{$routes['search']['route']}}">
            <span class="glyphicon glyphicon-search">
                {{--$routes['search']['name']--}}
            </span>
        </a>
        @endif
    </div>
    <form method="GET" action="">
        <div class="row">
            <?php $col = floor(12 / count($options)); ?>
            @foreach($options as $k => $option)
            <div class="col-md-<?= $col ?>">
                <select name="{{$k}}" id="{{$k}}" class="form-control" onChange="this.form.submit()">
                    @foreach($option as $key => $val)
                    <option value="{{$key}}"
                    <?=
                    isset($input[$k]) && $input[$k] == $key ? ' selected="selected"'
                            : '';

                    ?>
                            >{{$val}}</option>
                    @endforeach
                </select>
            </div>
            @endforeach
        </div>
    </form>

    @if(!$entries->count())
    <div class="panel-body">
        <h4 class="text-warning">
            {{ trans('entry.entries_not_found') }}
        </h4>
    </div>
    @else
    <table class="table table-hover" id="table-view">
        <thead>
            <tr>
                <th>№</th>
                <th>Заголовок</th>
                @foreach($cells as $cell => $func)
                <th>{{ trans("entry.$cell") }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ($entries as $entry)
            <tr>
                <td>
                    <a class="btn btn-default" href="{{ $entry->url }}">
                        <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
                        {{ $entry->order_key_print }}
                    </a>
                </td>
                <td>
                    {{{ $entry->title }}}<br/>
                    @if($entry->metro)
                    <span class="text-info">
                        {{{ trans('msg.metro_pref').$entry->metro.' ' }}}
                    </span>
                    @endif
                    @if($entry->mo_town)
                    <span class="text-warning">
                        {{{ trans('msg.town_pref').$entry->mo_town }}}
                    </span>
                    @endif
                    @if(!$entry->mo_town && !$entry->metro)
                    -
                    @endif
                </td>
                @foreach($cells as $cell => $func)
                <?php
                $var = object_get($entry, $cell, '-');
                $var = $func ? $func($var) : $var;

                ?>
                <td>{{$var}}</td>
                @endforeach

            </tr>
            @endforeach
        </tbody>
    </table>

    <?php
    //echo '<pre>', print_r($entries->toArray()), '</pre>';
    //die();

    ?>


    <!-- Кнопочный интерфейс -->
    <div class="panel-body">
        <div id="list-view">
            @foreach ($entries as $entry)
            <?php $status = $entry->finish_status; ?>
            <div class="row">
                <a class="btn btn-default order-btn col-md-6 col-sm-12 col-xs-12" href="{{ $entry->url }}">
                    <div class="panel panel-{{ $status }} order-panel text-left">
                        <div class="panel-heading" style="">
                            <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
                            {{ $entry->order_key_print }}
                            <small>{{{ trans('msg.from') }}} {{date("j.m.Y",strtotime($entry->created_at))}}</small>
                            /
                            {{{ is_null($entry->master)? 'без мастера' : $entry->master->name }}}
                        </div>

                        <div class="panel-body">
                            <div>
                                {{{ $entry->title }}}<br/>
                                @if($entry->metro)
                                <span class="text-info">
                                    {{{ trans('msg.metro_pref').$entry->metro.' ' }}}
                                </span>
                                @endif
                                @if($entry->mo_town)
                                <span class="text-warning">
                                    {{{ trans('msg.town_pref').$entry->mo_town }}}
                                </span>
                                @endif
                                @if(!$entry->mo_town && !$entry->metro)
                                -
                                @endif
                            </div>

                            @if($entry->is_archived)
                            <div class="text-{{ $status }}">
                                @if($entry->was_refused)
                                {{ trans('entry.refuse').' '.human_date_time($entry->canceled_at, true) }}
                                @else
                                {{ trans('entry.statuses.archived').' '.human_date_time($entry->archived_at, true) }}
                                @endif
                            </div>

                            @elseif($entry->is_deferred)
                            <div class="text-{{ $status }}">
                                {{ trans('entry.defer_before') .' '. human_date($entry->defer_before, true) }}
                                @if($entry->is_overdue_deferred)
                                <br/>
                                <b class="small">
                                    {{ trans('msg.days_deferred') }}: {{ $entry->overdue_deferred_days_count }}
                                </b>
                                @endif
                            </div>

                            @elseif($entry->is_refused)
                            <div class="text-danger">
                                {{  trans('entry.refuse') .' '. human_date_time($entry->refuse_time, true) }}<br/>
                                @if(!empty($entry->refuse_reason))
                                <i class="small">
                                    {{ \nl2br($entry->refuse_reason) }}
                                </i>
                                @endif
                            </div>

                            @elseif($entry->is_overdue)
                            <div class="text-{{ $status }}">
                                {{ trans('entry.visit_time') }} {{ human_date($entry->visit_time, true) }}
                                <br/>
                                <b>
                                    {{ trans('msg.days_deferred') }}: {{ $entry->overdue_days_count }}
                                </b>
                            </div>

                            @elseif($entry->is_visit_wait)
                            <div class="text-success">
                                {{ trans('entry.statuses.visit_wait').' '.human_date_time($entry->visit_time, true) }}
                            </div>

                            @elseif($entry->is_call_wait)
                            <div class="text-warning">
                                {{ trans('entry.call_wait_str', ['days' => $entry->call_wait_days_count]) }}
                            </div>

                            @elseif(is_null($entry->master))
                            <div class="text-warning">
                                {{ trans('entry.no_master') }}
                            </div>

                            @endif

                        </div>

                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <!-- конец - Кнопочный интерфейс -->

    <!-- пагинация -->
    {{ $entries->links() }}

    @endif
</div>

@if(isset($routes['export']))
<div class="nav" style="margin-bottom:10px;">
    <a class="btn btn-large btn-primary col-sm-3 col-xs-12" href="{{$routes['export']['route']}}">
        {{$routes['export']['name']}}
    </a>
</div>
@endif

<div class="well">
    @foreach($icons as $key => $val)
    <span class="glyphicon glyphicon-{{ $val }}"></span> - {{ trans('entry.statuses.' . strtolower($key)) }}<br/>
    @endforeach
</div>


@include('includes.index_view')

@stop