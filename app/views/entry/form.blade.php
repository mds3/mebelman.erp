@extends('includes.extends_master')

{{-- Content --}}
@section('content')

{{ HTML::style('/css/jquery.datetimepicker.css') }}
{{ HTML::script('/js/jquery.datetimepicker.js') }}
{{ HTML::style('/css/typeahead.css') }}



{{ Form::model($entry, array('url' => $route, 'class' => 'form-horizontal col-md-7','role'=>'form','method'=>(isset($method)?$method:'POST'))) }}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
            @if($entry->order_key)
            {{{ $entry->order_key }}} / {{ trans('entry.edition') }}
            @else
            {{ trans('entry.new') }}
            @endif
        </h3>
    </div>
    <div class="panel-body">

        <!-- Title -->
        <div class="form-group{{{ $errors->has('title') ? ' has-error' : '' }}}">
            {{ Form::label('title', trans('msg.title'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('title') }}</span>
                {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Name -->
        <div class="form-group{{{ $errors->has('client_name') ? ' has-error' : '' }}}">
            {{ Form::label('client_name', trans('entry.client_name'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('client_name') }}</span>
                {{ Form::text('client_name', Input::old('client_name'), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Phone -->
        <div class="form-group{{{ $errors->has('client_phone') ? ' has-error' : '' }}}">
            {{ Form::label('client_phone', trans('entry.client_phone'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('client_phone') }}</span>
                {{ Form::input('tel', 'client_phone', Input::old('client_phone'), ['class' => 'form-control bfh-phone','data-format'=>get_phone_format()]) }}
            </div>
        </div>

        <!-- MetroStation -->
        <div class="form-group{{{ $errors->has('metro') ? ' has-error' : '' }}}">
            {{ Form::label('metro', trans('entry.metro'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('metro') }}</span>
                {{ Form::text('metro', Input::old('metro'), array('class' => 'form-control typeahead')) }}
            </div>
        </div>

        <!-- MO_town -->
        <div class="form-group{{{ $errors->has('mo_town') ? ' has-error' : '' }}}">
            {{ Form::label('mo_town', trans('entry.mo_town'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('mo_town') }}</span>
                {{ Form::text('mo_town', Input::old('mo_town'), array('class' => 'form-control typeahead')) }}
            </div>
        </div>

        <!-- Master -->
        <div class="form-group{{{ $errors->has('user_id') ? ' has-error' : '' }}}">
            {{ Form::label('user_id', trans('entry.user_id'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                {{Form::select('user_id', $masters, (is_null($entry->master) ? 0 : $entry->master->id), array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Supplier -->
        <div class="form-group{{{ $errors->has('supplier_id') ? ' has-error' : '' }}}">
            {{ Form::label('supplier_id', trans('entry.supplier'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('supplier_id') }}</span>
                {{Form::select('supplier_id', $suppliers, $entry->supplier_id, array('class' => 'form-control')) }}
            </div>
        </div>

        <!-- Descr -->
        <div class="form-group{{{ $errors->has('descr') ? ' has-error' : '' }}}">
            {{ Form::label('descr', trans('msg.descr'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('descr') }}</span>
                {{ Form::textarea('descr', Input::old('descr'), array('class' => 'form-control', 'size' => '20x4')) }}

            </div>
        </div>

        <!-- refer -->
        <div class="form-group{{{ $errors->has('refer_id') ? ' has-error' : '' }}}">
            <label class="col-sm-4 control-label" for="refer_id">
                {{ trans('entry.refer_id') }}
            </label>
            <div class="col-sm-8">
                <span class="text-danger">{{ $errors->first('refer_id') }}</span>
                {{Form::select('refer_id', $refers, $entry->refer_id, array('class' => 'form-control')) }}
            </div>
        </div>

        @if(!$entry->is_new)
        <!-- visit_time form -->
        <div class="form-group{{{ $errors->has('visit_time') ? ' has-error' : '' }}}" id="visit-time-form">
            {{ Form::label('visit_time', trans('entry.visit_time'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-md-8">
                {{ Form::text('visit_time', Input::old('visit_time'), array('class' => 'form-control')) }}
                <span class="text-danger">{{ $errors->first('visit_time') }}</span>
            </div>
        </div>

        <!-- details form -->
        <div class="form-group{{{ $errors->has('details') ? ' has-error' : '' }}}" id="details-form">
            {{ Form::label('details', trans('entry.details'), array('class' => 'col-sm-4 control-label')) }}
            <div class="col-md-8">
                {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control', 'size' => '20x4')) }}
                <span class="text-danger">{{ $errors->first('details') }}</span>
            </div>
        </div>
        @endif

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                {{ Form::submit(trans('entry.save'), array('class' => 'btn btn-large btn-primary')) }}
                @if(isset($routes['return']))
                <a id="return-button" href="{{ $routes['return']['route'] }}" type="button" class="btn btn-default">
                    {{ $routes['return']['name'] }}
                </a>
                @endif
            </div>
        </div>


    </div>
</div>

{{ Form::close() }}

{{ HTML::script('/js/typeahead.bundle.min.js') }}
{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}

<script>
    $(document).ready(function () {


        var metro = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: '/assets/metro.obj.json'
        });
        var mo_towns = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '/assets/mo_towns.obj.json'
        });
        mo_towns.initialize();
        metro.initialize();
        $('#metro').typeahead(null, {
            name: 'metro',
            displayKey: 'name',
            source: metro.ttAdapter()
        });
        $('#mo_town').typeahead(null, {
            name: 'mo_town',
            displayKey: 'name',
            source: mo_towns.ttAdapter()
        });

        $('#visit_time').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d H:i',
            minDate: 0
        });

    });
</script>


@stop