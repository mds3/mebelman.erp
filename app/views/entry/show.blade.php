@extends('includes.extends_master')

{{-- Content --}}
@section('content')

    <div class="row">

        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
                        {{ $entry->order_key_print }} /
                        {{{ is_null($entry->master)? 'без мастера' : $entry->master->name }}}
                    </h3>
                </div>
                <div class="panel-body ">
                    <div style="padding-left:5px;">

                        <!-- order_created-button -->
                        @if(isset($routes['order_created']))
                            <p class="row" style="margin-top:0;">
                                <a class="btn btn-primary col-lg-3" href="{{ $routes['order_created']['route'] }}">
                                    {{ $routes['order_created']['name'] }}
                                </a>
                            </p>
                        @endif

                        <small>
                            {{ human_date($entry->created_at, true) }}
                        </small>
                        <br/>
                        <h4 style="margin-top:0;">
                            {{{ $entry->title }}}
                        </h4>

                        @if($entry->mo_town || $entry->metro)
                            <p style="margin-top:0;">
                                @if($entry->metro)
                                    <span class="text-info">
                            {{{ trans('msg.metro_pref').$entry->metro.' ' }}}
                        </span>
                                @endif
                                @if($entry->metro && $entry->mo_town)
                                    /
                                @endif
                                @if($entry->mo_town)
                                    <span class="text-warning">
                            {{{ trans('msg.town_pref') . $entry->mo_town }}}
                        </span>
                                @endif
                            </p>
                        @endif

                        @if($entry->descr)
                            <p>
                                <span class="glyphicon glyphicon-phone-alt"></span>
                                <em>{{ \nl2br($entry->descr) }}</em>
                            </p>
                        @endif

                        @if(!empty($entry->details))
                            <p>
                                <span class="glyphicon glyphicon-info-sign"></span>
                                <em>{{{ $entry->details }}}</em>
                            </p>
                        @endif

                    </div>


                    @if($entry->is_deferred)
                        <div class="alert alert-{{ $entry->finish_status }} alert-block">
                            <dl class="dl-horizontal details">
                                <!-- deferred -->
                                <dt>
                                    {{ trans('entry.defer_before') }}
                                </dt>
                                <dd>
                                    {{ human_date($entry->defer_before, true) }}
                                    @if($entry->is_overdue_deferred)
                                        <br/>
                                        <b>
                                            {{ trans('msg.days_deferred') }}: {{ $entry->overdue_deferred_days_count }}
                                        </b>
                                    @endif
                                </dd>
                                <!-- defer_reason -->
                                <dt>
                                    {{{ trans('entry.defer_reason') }}}
                                </dt>
                                <dd>
                                    {{ $entry->defer_reason }}
                                </dd>
                            </dl>
                        </div>

                    @elseif($entry->is_refused)
                    <!-- refused -->
                        <div class="alert alert-{{ $entry->finish_status }} alert-block">
                            <dl class="dl-horizontal details">
                                <dt>
                                    {{{ trans('entry.do_refuse') }}}
                                </dt>
                                <dd>
                                    {{ human_date_time($entry->refuse_time, true) }}
                                </dd>
                                <!-- details -->
                                <dt>
                                    {{{ trans('entry.refuse_reason') }}}
                                </dt>
                                <dd>
                                    @if(!empty($entry->refuse_reason))
                                        {{ \nl2br($entry->refuse_reason) }}
                                    @else
                                        <i> - </i>
                                    @endif
                                </dd>
                            </dl>
                        </div>

                    @elseif($entry->is_overdue)
                    <!--  -->
                        <div class="alert alert-{{ $entry->finish_status }} alert-block">
                            <dl class="dl-horizontal details">
                                <dt>
                                    {{{ trans('entry.visit_time') }}}
                                </dt>
                                <dd>
                                    {{ human_date($entry->visit_time, true) }}
                                    <br/>
                                    <b>
                                        {{ trans('msg.days_deferred') }}: {{ $entry->overdue_days_count }}
                                    </b>
                                </dd>
                            </dl>
                        </div>
                    @endif

                    <div class="well">

                        <h4 style="margin:0 0 10px 2px;">
                            {{{ $entry->client_name }}}
                        </h4>

                        <a class="btn btn-success" href="tel:{{{ $entry->client_phone }}}">
                            <span class="glyphicon glyphicon-earphone"> </span>
                            <span class="bfh-phone" data-format="<?= get_phone_format() ?>"
                                  data-number="{{$entry->client_phone}}"></span>
                        </a>

                    </div>

                    <dl class="dl-horizontal details" style="margin-left:8px;">

                    @if($entry->visit_time_isset)
                        <!-- visit_time -->
                            <dt>
                                <span class="glyphicon glyphicon-road col-lg-offset-0"></span>
                                {{{ trans('entry.visit_time') }}}
                            </dt>
                            <dd>
                        <span class="text-info">
                            <b>{{ human_date_time($entry->visit_time) }}</b>
                        </span>
                            </dd>
                        @endif

                        @if($entry->master_isset)

                            <dt>
                                <span class="glyphicon glyphicon-eye-open"></span>
                                {{{ trans('entry.master_view_entry') }}}
                            </dt>
                            <dd>
                                @if(!$entry->is_new)
                                    {{ human_date_time($entry->view_time) }}
                                @else
                                    {{ trans('msg.no') }}
                                @endif
                            </dd>

                            @if($entry->visit_time_isset)
                                <dt>
                                    <span class="glyphicon glyphicon-phone-alt"></span>
                                    Звонил
                                </dt>
                                <dd>
                                    @if($entry->is_called)
                                        {{ human_date_time($entry->call_time) }}
                                    @else
                                        Нет
                                    @endif
                                </dd>
                            @endif

                        @endif

                        @if(route_allowed('refer.index'))
                            <dt>
                                <span class="glyphicon glyphicon-user"></span>
                                Мастер
                            </dt>
                            <dd>
                                @if(!$entry->master_isset)
                                    не назначен
                                @elseif($entry->master->id != Auth::id())
                                    {{--<a class="btn btn-success" href="tel:{{{ $entry->master->phone }}}">
                                        <span class="glyphicon glyphicon-earphone"> </span>
                                        <span>{{ $entry->master->name }}</span>
                                    </a>--}}
                                    <div class="btn btn-success" href="tel:{{{ $entry->master->phone }}}">
                                        {{--<span class="glyphicon glyphicon-earphone"> </span>--}}
                                        <span>{{ $entry->master->name }}</span>
                                    </div>
                                @else
                                    <span class="text-primary">
                            {{ $entry->master->name }}
                        </span>
                                @endif
                            </dd>

                            <dt>
                                <span class="glyphicon glyphicon-user"></span>
                                Оператор
                            </dt>
                            <dd>
                                @if($entry->operator->id != Auth::id())
                                    <a class="btn btn-success" href="tel:{{{ $entry->operator->phone }}}">
                                        <span class="glyphicon glyphicon-earphone"> </span>
                                        <span>
                                {{ $entry->operator->name }}
                            </span>
                                    </a>
                                @else
                                    <span class="text-primary">
                            {{ $entry->operator->name }}
                        </span>
                                @endif
                            </dd>

                            <dt>
                                <span class="glyphicon glyphicon-user"></span>
                                Снабженец
                            </dt>
                            <dd>
                                @if($entry->supplier->id != Auth::id())
                                    <a class="btn btn-success" href="tel:{{{ $entry->supplier->phone }}}">
                                        <span class="glyphicon glyphicon-earphone"> </span>
                                        <span>
                                {{ $entry->supplier->name }}
                            </span>
                                    </a>
                                @else
                                    <span class="text-primary">
                            {{ $entry->supplier->name }}
                        </span>
                                @endif
                            </dd>

                            <dt>
                                <span class="glyphicon glyphicon-bullhorn"></span>
                                Источник
                            </dt>
                            <dd>
                                {{{ $entry->refer->name }}}
                            </dd>
                        @endif

                    </dl>

                    @if(isset($routes) && !empty($routes))
                        @include('entry.inc.visit_time_form')
                    @endif

                    <div class="col-sm-9 col-md-7">

                        <!-- visit_time-button -->
                        @if(isset($routes['visit']))
                            <button id="visit-button" type="button"
                                    class="btn btn-primary btn-block">{{{ $routes['visit']['name'] }}}</button>
                        @endif

                    <!-- Order create button -->
                        @if(isset($routes['create']))
                            <a href="{{ $routes['create']['route'] }}" type="button"
                               class="btn btn-info btn-block">{{ $routes['create']['name'] }}</a>
                        @endif

                    <!-- defer-button -->
                        @if(isset($routes['defer']))
                            <button id="defer-button" type="button"
                                    class="btn btn-primary btn-block">{{{ $routes['defer']['name'] }}}</button>
                        @endif

                    <!-- refuse-button -->
                        @if(isset($routes['refuse']))
                            <button id="refuse-button" type="button"
                                    class="btn btn-primary btn-block">{{{ $routes['refuse']['name'] }}}</button>
                        @endif

                    <!-- reanimate-button -->
                        @if(isset($routes['reanimate']))
                            {{ Form::postButton($routes['reanimate'],
                        "Восстановить ".(isset($reanimate_msg)?$reanimate_msg:$entry->order_key)."?") }}
                        @endif

                    <!-- archiving-button -->
                        @if(isset($routes['archiving']))
                            {{ Form::postButton($routes['archiving'],
                        trans('entry.do_archived').' '.(isset($reanimate_msg)?$reanimate_msg:$entry->order_key)."?") }}
                        @endif

                    <!-- edit-button -->
                        @if(isset($routes['edit']))
                            <a class="btn btn-primary btn-block" style="margin:4px 0 0 0;"
                               href="{{$routes['edit']['route']}}">{{$routes['edit']['name']}}</a>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            @if(isset($routes['showlogs']))
                @include('includes.showlogs',['route' => $routes['showlogs']])
            @endif
        </div>


    </div>

    <a href="/entry" type="button" class="btn btn-default col-lg-3 col-md-4 col-sm-12 col-xs-12">
        {{ trans('entry.all') }}
    </a>
    <br/>
    <br/>

    {{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}


@stop
