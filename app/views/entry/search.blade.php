@extends('includes.extends_master')

{{-- Content --}}
@section('content')

    {{ HTML::style('/css/datepicker3.css') }}
    {{ HTML::script('/js/bootstrap-datepicker.js') }}
    {{ HTML::script('/js/bootstrap-datepicker.ru.js') }}
    {{ HTML::style('/css/typeahead.css') }}
    {{ HTML::script('/js/typeahead.bundle.min.js') }}
    {{ HTML::script('/js/jquery.inputmask.js') }}

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <div class="row">
                {{Form::open()}}
                <div class="col-md-2">

                    <select name="search_by" id="search_by" class="form-control" onChange="">
                        @foreach($options as $key => $val)
                            <option value="{{$key}}"
                                    <?=
                                    isset($input["search_by"]) && $input["search_by"] == $key ? ' selected="selected"'
                                        : '';

                                    ?>
                                    data-type="{{$val['type']}}">{{$val['name']}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-4">
                    {{ Form::text('search_val', $input["search_val"],
                        array('class' => 'form-control','id'=>'search_val')) }}
                </div>

                <div class="col-md-3">
                    {{Form::button("Искать", array(
                        'type' => 'submit',
                        'name' => 'doSearch',
                        'class' => 'btn btn-info btn col-xs-12'
                        )) }}

                </div>
                {{ Form::close() }}
            </div>
        </div>

        <?php /* @var $entries Illuminate\Database\Eloquent\Collection */ ?>

        @if(!$entries->count())
            <div class="panel-body">
                <p>Ничего не найдено по данному запросу.</p>
            </div>
        @else
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Заголовок</th>
                    <th>Мастер</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($entries as $entry)
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
                            <a href="{{ $entry->url }}">{{{$entry->order_key}}}</a>
                        </td>
                        <td>{{{$entry->title}}}</td>
                        <td>@if($entry->master_isset){{{$entry->master->name}}}@else - не назначен - @endif</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>

    <div class="well">
        @foreach($icons as $key => $val)
            <span class="glyphicon glyphicon-{{ $val }}"></span> - {{ trans('status.' . strtolower($key)) }}<br/>
        @endforeach
    </div>

    <script>
        $(document).ready(function () {

            metro = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                limit: 10,
                prefetch: '/assets/metro.obj.json'
            });
            metro.initialize();

            set_search_value_class();

            $("#search_by").change(function () {
                $('#search_val').val('');
                $('#search_val').datepicker('remove');
                $('#search_val').typeahead('destroy');
                $('#search_val').inputmask("remove");
                set_search_value_class();
            });
        });

        function set_search_value_class() {
            var type = $("#search_by").find(':selected').data('type');
            if (type === 'date') {
                $('#search_val').datepicker({
                    format: "dd.mm.yyyy",
                    todayBtn: "linked",
                    language: "ru",
                    autoclose: true
                });
            } else if (type === 'metro') {
                $('#search_val').typeahead(null, {
                    name: 'metro',
                    displayKey: 'name',
                    source: metro.ttAdapter()
                });
            } else if (type === 'phone') {
                $('#search_val').inputmask('<?= get_phone_format('9') ?>');
            } else if (type === 'order_key') {
                $('#search_val').inputmask({mask: "M-999999-9[9][9]", greedy: false});
            } else {
                //$('#search_val').val('');
            }

            //alert($("#search_val").attr('class'));
        }
    </script>

@stop
