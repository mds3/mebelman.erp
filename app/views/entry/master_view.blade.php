@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
$view_time = strtotime($entry->view_time);
$call_time = strtotime($entry->call_time);
$visit_time = strtotime($entry->visit_time);
$owner = \Auth::id() == $entry->master->id;

?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            {{{ $page_title}}}
            @if($view_time <= 0)
            <span class="label label-default pull-right">Новая</span>
            @endif
        </h3>
    </div>
    <div class="panel-body">

        {{ Form::model($entry, array(
                    'route' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'method'=>(isset($method)?$method:'POST'))) }}

        <div class="form-group">
            <label class="col-sm-4 control-label">Дата</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{{date('j.m.Y в H:i',strtotime($entry->created_at))}}}
                </p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Заголовок</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->title}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Метро</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->metro}}}</p>
            </div>
        </div>

        @if(!empty($entry->mo_town))
        <div class="form-group">
            <label class="col-sm-4 control-label">М.О.</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->mo_town}}}</p>
            </div>
        </div>
        @endif

        <div class="form-group">
            <label class="col-sm-4 control-label">Телефон</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->client_phone}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Имя</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->client_name}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Детали</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->descr}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Мастер</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->master->name}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Просмотрена</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    @if($view_time > 0)
                    {{date('j.m.Y в H:i',$view_time);}}
                    @else
                    Нет
                    @endif
                </p>
            </div>
        </div>

        @if($view_time > 0)
        @include('entry.inc.visit_time_form')
        @endif

        {{ Form::close() }}

        @include('includes.control_unit',[
        'edit_route' => 'entry.edit',
        'delete_route' => 'entry.destroy',
        'delete_msg' => 'заявку №'.$entry->order_key,
        'model' => $entry,
        ])

    </div>
</div>




@stop