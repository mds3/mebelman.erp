{{ HTML::style('/css/jquery.datetimepicker.css') }}
{{ HTML::script('/js/jquery.datetimepicker.js') }}

<!-- visit_time -->
@if($visit_time > 0)
<div class="form-group" id="visit-time-info">
    <label class="col-sm-4 control-label">Первичный выезд</label>
    <div class="col-sm-8">
        <p class="form-control-static">
            <span>{{date('j.m.Y в H:i',$visit_time);}}</span>
            @if($owner)
            <br/><a href="#js" id="change-visit-time">Изменить</a>
            @endif
        </p>
    </div>
</div>


<!-- details -->
<div class="form-group" id="details-info">
    <label class="col-sm-4 control-label">Детали</label>
    <div class="col-sm-8">
        <p class="form-control-static">
            @if(!empty($entry->details))
            <span>{{{$entry->details}}}</span>
            @else
            <small><i>Не указаны</i></small>
            @endif
            @if($owner)
            <br/><a href="#js" id="change-details">Изменить</a>
            @endif
        </p>
    </div>
</div>

@endif


@if($owner)

{{ Form::hidden('_status', Input::old('_status','visit'), ['id'=>'status']) }}

<div id="visit-form">

    <!-- visit_time form -->
    <div class="form-group{{{ $errors->has('visit_time') ? ' has-error' : '' }}}" id="visit-time-form">
        {{ Form::label('visit_time', 'Дата первичного выезда', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::text('visit_time', Input::old('visit_time'), array('class' => 'form-control')) }}
            <span class="text-danger">{{ $errors->first('visit_time') }}</span>
        </div>
    </div>

    <!-- details form -->
    <div class="form-group{{{ $errors->has('details') ? ' has-error' : '' }}}" id="details-form">
        {{ Form::label('details', 'Детали', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control', 'size' => '20x4')) }}
            <span class="text-danger">{{ $errors->first('details') }}</span>
        </div>
    </div>
</div>

<!-- refuse_reason form -->
<div class="form-group{{{ $errors->has('refuse_reason') ? ' has-error' : '' }}}" id="refuse-reason-form">
    {{ Form::label('refuse-refuse_reason', 'Причина отказа', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-md-3">
        {{ Form::textarea('refuse_reason', Input::old('refuse_reason'), array('class' => 'form-control', 'size' => '20x4','id'=>'refuse-reason-input')) }}
        <span class="text-danger">{{ $errors->first('refuse_reason') }}</span>
    </div>
</div>

<!-- defer-form -->
<div class="form-group" id="defer-form">
    <div class="form-group{{{ $errors->has('defer_before') ? ' has-error' : '' }}}" id="defer_before-form">
        {{ Form::label('defer_before', 'Отложить до', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::text('defer_before', Input::old('defer_before'), array('class' => 'form-control')) }}
            <span class="text-danger">{{ $errors->first('defer_before') }}</span>
        </div>
    </div>
    <div class="form-group{{{ $errors->has('defer_reason') ? ' has-error' : '' }}}" id="defer-reason-form">
        {{ Form::label('defer_reason', 'Причина', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::textarea('defer_reason', Input::old('defer_reason'), array('class' => 'form-control', 'size' => '20x4','id'=>'refuse-reason-input')) }}
            <span class="text-danger">{{ $errors->first('defer_reason') }}</span>
        </div>
    </div>
</div>



<div class="form-group" id="buttons">
    <div class="col-sm-offset-4 col-sm-8">
        <!-- Submit button -->
        <button id="submit-button" name="save" type="submit" class="btn btn-large btn-primary">Сохранить заявку</button>
        <!-- visit_time-button -->
        <button id="visit-button" type="button" class="btn btn-large btn-primary">Время визита</button>
        <!-- refuse-button -->
        <button id="refuse-button" type="button" class="btn btn-large btn-primary">Отказ</button>
        <!-- defer-button -->
        <button id="defer-button" type="button" class="btn btn-large btn-primary">Отложить</button>
    </div>
</div>


<!-- Order create button -->
@if($entry->is_called)
<div class="form-group" id="order-button">
    <div class="col-sm-offset-4 col-sm-4">
        <a href="<?= route('order.create', ['id' => $entry->id], false) ?>" type="button" class="btn btn-large btn-primary">Сформировать заказ</a>
    </div>
</div>
@endif

<script>
    $(document).ready(function () {
        $("#order-button").hide();
        $("#refuse-reason-form").hide();
        $("#defer-form").hide();
        $("#visit-button").hide();
        //alert($("#visit-time-info span").text().length);
        if ($("#visit-time-info span").text().length > 0) {
            $("#visit-time-form").hide();
            $("#details-form").hide();
            $("#submit-button").hide();
            $("#order-button").show();
        }
        $("#change-visit-time").click(function () {
            $("#visit-time-info").hide();
            $("#visit-time-form").show();
            $("#order-button").hide();
            $("#submit-button").show();
            $("#status").val('visit');
        });
        $("#change-details").click(function () {
            $("#details-info").hide();
            $("#details-form").show();
            $("#order-button").hide();
            $("#submit-button").show();
        });
        $("#visit-button").click(function () {
            $("#order-button").hide();
            $("#refuse-reason-form").hide();
            $("#visit-button").hide();
            $("#defer-form").hide();
            $("#defer-button").show();
            $("#visit-form").show();
            $("#submit-button").show();
            $("#refuse-button").show();
            $("#status").val('visit');
        });
        $("#refuse-button").click(function () {
            $("#order-button").hide();
            $("#visit-form").hide();
            $("#refuse-button").hide();
            $("#defer-form").hide();
            $("#defer-button").show();
            $("#refuse-reason-form").show();
            $("#refuse-reason-input").focus();
            $("#submit-button").show();
            $("#visit-button").show();
            $("#status").val('refuse');
        });

        $("#defer-button").click(function () {
            $("#order-button").hide();
            $("#visit-form").hide();
            $("#defer-button").hide();
            $("#refuse-reason-form").hide();
            $("#defer-form").show();
            $("#submit-button").show();
            $("#visit-button").show();
            $("#status").val('defer');
        });

        $('#visit_time').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d H:i',
            minDate: 0
        });
        $('#defer_before').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d',
            timepicker: false,
            closeOnDateSelect: true,
            minDate: 0
        });


        if ($("#status").val() !== 'visit') {
            $("#" + $("#status").val() + "-button").click();
        }
    });
</script>

@endif

