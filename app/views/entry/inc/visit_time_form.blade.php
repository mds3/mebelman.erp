{{ HTML::style('/css/jquery.datetimepicker.css') }}
{{ HTML::script('/js/jquery.datetimepicker.js') }}


@if(isset($routes['visit']))

{{ Form::model($entry, array(
                    'url' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'id'=>'visit-form',
                    'method'=>(isset($method)?$method:'POST'))) }}

{{ Form::hidden('_status', Input::old('_status','visit'), ['id'=>'status']) }}

<!-- visit_time form -->
<div id="visit-time-form" class="form-group{{{ $errors->has('visit_time') ? ' has-error' : '' }}}">
    {{ Form::label('visit_time', 'Дата выезда', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-md-6">
        {{ Form::text('visit_time', Input::old('visit_time'), array('class' => 'form-control')) }}
        <span class="text-danger">{{ $errors->first('visit_time') }}</span>
    </div>
</div>
<!-- details form -->
<div id="details-form" class="form-group{{{ $errors->has('details') ? ' has-error' : '' }}}">
    {{ Form::label('details', 'Детали', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-md-6">
        {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control', 'size' => '20x4')) }}
        <span class="text-danger">{{ $errors->first('details') }}</span>
    </div>
</div>
<!-- Submit button -->
<div id="submit-button" class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить заявку', array('class' => 'btn btn-primary',  'name' => 'save')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">Отмена</button>
    </div>
</div>
{{ Form::close() }}
@endif


@if(isset($routes['refuse']))
<!-- refuse_reason form -->
{{ Form::model($entry, array(
                    'url' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'id'=>'refuse-form',
                    'method'=>(isset($method)?$method:'POST'))) }}

{{ Form::hidden('_status', Input::old('_status','refuse'), ['id'=>'status']) }}
<div id="refuse-reason-form" class="form-group{{{ $errors->has('refuse_reason') ? ' has-error' : '' }}}">
    {{ Form::label('refuse-refuse_reason', 'Причина отказа', array('class' => 'col-sm-4 control-label')) }}
    <div class="col-md-3">
        {{ Form::textarea('refuse_reason', Input::old('refuse_reason'), array('class' => 'form-control', 'size' => '20x4','id'=>'refuse-reason-input')) }}
        <span class="text-danger">{{ $errors->first('refuse_reason') }}</span>
    </div>
</div>
<!-- Submit button -->
<div id="submit-button" class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить заявку', array('class' => 'btn btn-large btn-primary',  'name' => 'save')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">
            Отмена
        </button>
    </div>
</div>
{{ Form::close() }}
@endif

@if(isset($routes['defer']))
<!-- defer-form -->
<?php
$entry->defer_before = null;
$entry->defer_reason = '';

?>
{{ Form::model($entry, array(
                    'url' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'id'=>'defer-form',
                    'method'=>(isset($method)?$method:'POST'))) }}

{{ Form::hidden('_status', Input::old('_status','defer'), ['id'=>'status']) }}
<div class="form-group">
    <div id="defer_before-form" class="form-group{{{ $errors->has('defer_before') ? ' has-error' : '' }}}">
        {{ Form::label('defer_before', 'Отложить до', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::text('defer_before', Input::old('defer_before'), array('class' => 'form-control')) }}
            <span class="text-danger">{{ $errors->first('defer_before') }}</span>
        </div>
    </div>
    <div id="defer-reason-form" class="form-group{{{ $errors->has('defer_reason') ? ' has-error' : '' }}}">
        {{ Form::label('defer_reason', 'Причина', array('class' => 'col-sm-4 control-label')) }}
        <div class="col-md-3">
            {{ Form::textarea('defer_reason', Input::old('defer_reason'), array('class' => 'form-control', 'size' => '20x4','id'=>'refuse-reason-input')) }}
            <span class="text-danger">{{ $errors->first('defer_reason') }}</span>
        </div>
    </div>
</div>
<!-- Submit button -->
<div id="submit-button" class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        {{ Form::submit('Сохранить заявку', array('class' => 'btn btn-large btn-primary',  'name' => 'save')) }}
        <button id="reset-button" type="button" class="reset btn btn-primary">Отмена</button>
    </div>
</div>
{{ Form::close() }}
@endif

<script>
    $(document).ready(function () {

        reset_all();

        if ($("#visit-time-info span").text().length > 0) {
            $("#visit-time-form").hide();
            $("#details-form").hide();
            //$("#submit-button").hide();
            $("#order-button").show();
        }

        $(".reset").click(function () {
            reset_all();
        });

        $("#visit-button").click(function () {
            $("#refuse-form").hide();
            $("#visit-button").hide();
            $("#defer-form").hide();
            //$("#submit-button").show();
            $("#order-button").show();
            $("#defer-button").show();
            $("#visit-form").show();
            $("#refuse-button").show();
            $("#status").val('visit');
        });
        $("#refuse-button").click(function () {
            $("#order-button").hide();
            $("#visit-form").hide();
            $("#refuse-button").hide();
            $("#defer-form").hide();
            $("#defer-button").show();
            $("#refuse-form").show();
            $("#refuse-reason-input").focus();
            //$("#submit-button").show();
            $("#visit-button").show();
            $("#status").val('refuse');
        });

        $("#defer-button").click(function () {
            $("#order-button").hide();
            $("#visit-form").hide();
            $("#defer-button").hide();
            $("#refuse-button").show();
            $("#refuse-form").hide();
            $("#defer-form").show();
            //$("#submit-button").show();
            $("#visit-button").show();
            $("#status").val('defer');
        });

        $('#visit_time').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d H:i',
            minDate: 0
        });

        $('#defer_before').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'ru',
            format: 'Y-m-d',
            timepicker: false,
            closeOnDateSelect: true,
            minDate: 0
        });


        /*if ($("#status").val() !== 'visit') {
         $("#" + $("#status").val() + "-button").click();
         }*/
    });
    function reset_all() {
        $("#refuse-form").hide();
        $("#defer-form").hide();
        $("#visit-form").hide();
        $("#visit-button").show();
        $("#defer-button").show();
        $("#refuse-button").show();
    }
</script>

