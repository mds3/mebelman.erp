@extends('includes.extends_master')

{{-- Content --}}
@section('content')

<?php
//echo '<pre>', print_r($entry->toArray()), '</pre>';
//die();

$view_time = \strtotime($entry->view_time);
$call_time = \strtotime($entry->call_time);
$visit_time = \strtotime($entry->visit_time);
$refuse_time = \strtotime($entry->refuse_time);
$owner = (!is_null($entry->master) && \Auth::id() == $entry->master->id);


/* @var $errors Illuminate\Support\ViewErrorBag */
//echo '<pre>', print_r($errors), '</pre>';

?>

@if($errors->count())
<div class="alert alert-danger alert-block">
    На странице имеются ошибки: {{{ $errors->first() }}}
</div>
@endif

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-{{ $entry->getIcon() }}"></span>
            {{{ $entry->order_key }}} /
            {{{ is_null($entry->master)? 'без мастера' : $entry->master->name }}}
        </h3>
    </div>
    <div class="panel-body">

        {{ Form::model($entry, array(
                    'url' => $route,
                    'class' => 'form-horizontal',
                    'role'=>'form',
                    'method'=>(isset($method)?$method:'POST'))) }}

        <div class="form-group">
            <label class="col-sm-4 control-label">Дата</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{{date('j.m.Y в H:i',strtotime($entry->created_at))}}}
                </p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Заголовок</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->title}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Имя</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->client_name}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Телефон</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    <a class="btn btn-success" href="tel:{{{ $entry->client_phone }}}">
                        <span class="glyphicon glyphicon-earphone"> </span>
                        <span class="bfh-phone" data-format="<?= get_phone_format() ?>" data-number="{{$entry->client_phone}}"></span>
                    </a>
                </p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Метро</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->metro}}}</p>
            </div>
        </div>

        @if(!empty($entry->mo_town))
        <div class="form-group">
            <label class="col-sm-4 control-label">М.О.</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->mo_town}}}</p>
            </div>
        </div>
        @endif

        <div class="form-group">
            <label class="col-sm-4 control-label">Описание</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{\nl2br($entry->descr)}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Источник</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{{$entry->refer->name}}}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Мастер</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    @if(is_null($entry->master))
                    не назначен
                    @else
                    <a class="btn btn-success" href="tel:{{{ $entry->master->phone }}}">
                        <span class="glyphicon glyphicon-earphone"> </span>
                        <span>{{ $entry->master->name }}</span>
                    </a>
                    @endif
                </p>
            </div>
        </div>

        @if(!is_null($entry->master))
        <div class="form-group">
            <label class="col-sm-4 control-label">Просмотрена</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    @if($view_time > 0)
                    {{\date('j.m.Y в H:i',$view_time);}}
                    @else
                    Нет
                    @endif
                </p>
            </div>
        </div>
        @endif

        @if($call_time > 0)
        <div class="form-group">
            <label class="col-sm-4 control-label">Звонок</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{\date('j.m.Y в H:i',$call_time);}}
                </p>
            </div>
        </div>
        @endif

        @if($entry->is_deferred)
        <div class="form-group">
            <label class="col-sm-4 control-label">Отложена до</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{\date('j.m.Y',\strtotime($entry->defer_before));}}
                </p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Причина</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{ $entry->defer_reason }}
                </p>
            </div>
        </div>
        @endif

        @if($refuse_time > 0)
        <div class="form-group">
            <label class="col-sm-4 control-label">Отказ</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    {{\date('j.m.Y в H:i',$refuse_time);}}
                </p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Причина отказа</label>
            <div class="col-sm-8">
                <p class="form-control-static">
                    @if(!empty($entry->refuse_reason))
                    {{\nl2br($entry->refuse_reason)}}
                    @else
                    <i>Не указана</i>
                    @endif
                </p>
            </div>
        </div>

        @endif

        @include('entry.inc.visit_time_form')

        {{ Form::close() }}

        <div class="btn-group">

            @if(isset($routes['reanimate']))
            {{ Form::postButton($routes['reanimate'],
                "Восстановить ".(isset($reanimate_msg)?$reanimate_msg:$entry->order_key)."?") }}
            @endif

            @if(isset($routes['archiving']))
            {{ Form::postButton($routes['archiving'],
                "Переместить в архив ".(isset($reanimate_msg)?$reanimate_msg:$entry->order_key)."?") }}
            @endif

            @if(isset($routes['edit']))
            <a class="btn btn-primary btn-sm" href="{{$routes['edit']['route']}}">{{$routes['edit']['name']}}</a>
            @endif

            @if(isset($routes['delete']))
            {{ Form::btnDelete($routes['delete']['route'],"Удалить заявку №".$entry->order_key." безвозвратно?") }}
            @endif

            @if(isset($routes['showlogs']))
            @include('includes.showlogs',['route' => $routes['showlogs']])
            @endif

        </div>

    </div>
</div>

{{ HTML::script('/js/bootstrap-formhelpers-phone.js') }}


@stop