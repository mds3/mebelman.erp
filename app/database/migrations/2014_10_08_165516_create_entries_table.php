<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries',
            function(Blueprint $table) {
            $table->increments('id');
            $table->string('order_key', 10)->unique();
            $table->string('title', 100);
            $table->string('client_name', 50);
            $table->string('client_phone', 20);
            $table->string('metro', 50);
            $table->string('mo_town', 50);
            $table->text('descr');
            $table->integer('user_id', false, true)->unsigned();
            $table->timestamp('call_time');
            $table->timestamp('view_time');
            $table->timestamp('visit_time');
            $table->text('details');
            $table->timestamps();
            $table->integer('created_by', false, true)->unsigned();
            $table->integer('updated_by', false, true)->unsigned();
            $table->index('call_time');
            $table->index('view_time');
            $table->index('visit_time');
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('created_by')->references('id')->on('user');
            $table->foreign('updated_by')->references('id')->on('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }

}
