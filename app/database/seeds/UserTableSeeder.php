<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
          'name' => 'admin',
          'pass' => Hash::make('adminpass'),
          'role_id' => 1
        ));

        User::create(array(
          'name' => 'user',
          'pass' => Hash::make('userpass'),
          'role_id' => 2
        ));
    }

}
