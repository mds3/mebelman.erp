<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::filter('ACL', 'Mebius\Filter\AclPermittedFilter');

Route::when('*', 'csrf', ['post', 'put', 'delete']);

Route::group(['before' => 'auth'],
    function () {
    Route::get('/',
        ['as' => 'main',
      'uses' => 'Mebius\Controller\MainController@indexAction'
    ]);
    Route::get('logout',
        ['as' => 'logout',
      'uses' => 'Mebius\Controller\AuthController@getLogout']);
    Route::put('auth/settings',
        ['as' => 'auth.settings.put',
      'uses' => 'Mebius\Controller\AuthController@putSettings']);
    Route::get('auth/settings',
        ['as' => 'auth.settings.get',
      'uses' => 'Mebius\Controller\AuthController@getSettings']);
    #####################
    ## ACL
    Route::group(['before' => 'ACL'],
        function () {
        #####################
        ## Entry
        Route::match(['GET', 'POST'], 'entry/search',
            ['as' => 'entry.search',
          'uses' => 'Mebius\Controller\EntryController@search']);
        Route::put('entry/{entry}/status',
            ['as' => 'entry.status',
          'uses' => 'Mebius\Controller\EntryController@status']);
        Route::get('entry/export',
            ['as' => 'entry.export',
          'uses' => 'Mebius\Controller\EntryController@export']);
        Route::get('entry/{entry}/showlogs',
            ['as' => 'entry.showlogs',
          'uses' => 'Mebius\Controller\EntryController@showlogs']);
        Route::resource('entry', 'Mebius\Controller\EntryController');
        ###############
        ## Order
        Route::match(['GET', 'POST'], 'order/search',
            ['as' => 'order.search',
          'uses' => 'Mebius\Controller\OrderController@search']);
        Route::get('order/export',
            ['as' => 'order.export',
          'uses' => 'Mebius\Controller\OrderController@export']);
        Route::post('order/{order}/finish',
            ['as' => 'order.finish',
          'uses' => 'Mebius\Controller\OrderController@finish']);
        Route::get('order/{order}/status/{status}',
            ['as' => 'order.status.get',
          'uses' => 'Mebius\Controller\OrderController@status']);
        Route::put('order/{order}/status/{status}',
            ['as' => 'order.status.put',
          'uses' => 'Mebius\Controller\OrderController@status']);
        Route::get('order/{order}/showlogs',
            ['as' => 'order.showlogs',
          'uses' => 'Mebius\Controller\OrderController@showlogs']);
        Route::post('order/{order}/cancel',
            ['as' => 'order.cancel',
          'uses' => 'Mebius\Controller\OrderController@cancel']);
        Route::post('order/{order}/archivate',
            ['as' => 'order.archivate',
          'uses' => 'Mebius\Controller\OrderController@archivate']);
        Route::post('order/{order}/stuff',
            ['as' => 'order.stuff',
          'uses' => 'Mebius\Controller\OrderController@stuff']);
        // Добавление денег в кассу
        Route::post('order/{order}/cashbox',
            ['as' => 'order.cashbox',
          'uses' => 'Mebius\Controller\OrderController@cashbox']);
        Route::resource('order', 'Mebius\Controller\OrderController');
        #####################
        ## User
        Route::resource('user', 'Mebius\Controller\UserController');
        #####################
        ## Role
        Route::resource('role', 'Mebius\Controller\RoleController');
        #####################
        ## Refer
        Route::resource('refer', 'Mebius\Controller\ReferController');
        #####################
        ## Cashbox
        Route::get('cashbox',
            ['as' => 'cashbox.index',
          'uses' => 'Mebius\Controller\CashboxController@getIndex']);
        Route::get('cashbox/export',
            ['as' => 'cashbox.export',
          'uses' => 'Mebius\Controller\CashboxController@export']);
        Route::get('cashbox/{order}',
            ['as' => 'cashbox.show',
          'uses' => 'Mebius\Controller\CashboxController@show']);
        #####################
        ## Report
        Route::get('report',
            ['as' => 'report.index',
          'uses' => 'Mebius\Controller\ReportController@getIndex']);
        Route::get('report/export',
            ['as' => 'report.export',
          'uses' => 'Mebius\Controller\ReportController@export']);
        #####################
        ## Help
        Route::get('help',
            ['as' => 'help.index',
          'uses' => 'Mebius\Controller\HelpController@getIndex']);
    });
});
Route::post('login', 'Mebius\Controller\AuthController@postLogin');
Route::get('login',
    ['as' => 'login', 'uses' => 'Mebius\Controller\AuthController@showLogin']);



