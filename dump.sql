/* Дамп приложен на всякий случай, т.к. нет уверенности, что миграции покрывают всё... */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных erp
CREATE DATABASE IF NOT EXISTS `erp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `erp`;


-- Дамп структуры для таблица erp.entries
CREATE TABLE IF NOT EXISTS `entries`
(
    `id`            int(10) unsigned                                                        NOT NULL AUTO_INCREMENT,
    `order_key`     varchar(10) COLLATE utf8_unicode_ci                                     NOT NULL,
    `title`         varchar(100) COLLATE utf8_unicode_ci                                    NOT NULL,
    `client_name`   varchar(50) COLLATE utf8_unicode_ci                                     NOT NULL,
    `client_phone`  varchar(20) COLLATE utf8_unicode_ci                                     NOT NULL,
    `metro`         varchar(50) COLLATE utf8_unicode_ci                                     NOT NULL,
    `mo_town`       varchar(50) COLLATE utf8_unicode_ci                                     NOT NULL,
    `descr`         text COLLATE utf8_unicode_ci                                            NOT NULL,
    `user_id`       int(10) unsigned                                                        NOT NULL,
    `supplier_id`   int(10) unsigned                                                        NOT NULL DEFAULT '2',
    `refer_id`      int(10) unsigned                                                        NOT NULL DEFAULT '1',
    `view_time`     datetime                                                                NOT NULL,
    `call_time`     datetime                                                                NOT NULL,
    `visit_time`    datetime                                                                NOT NULL,
    `refuse_time`   datetime                                                                NOT NULL,
    `defer_time`    datetime                                                                NOT NULL,
    `defer_before`  date                                                                             DEFAULT NULL,
    `details`       text COLLATE utf8_unicode_ci                                            NOT NULL,
    `refuse_reason` text COLLATE utf8_unicode_ci                                            NOT NULL,
    `defer_reason`  text COLLATE utf8_unicode_ci                                            NOT NULL,
    `status`        enum ('ACTIVE','REFUSED','DEFERRED','ARCHIVED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
    `archived_at`   datetime                                                                         DEFAULT NULL,
    `order_created` tinyint(1) unsigned                                                     NOT NULL DEFAULT '0',
    `created_at`    timestamp                                                               NOT NULL DEFAULT '0000-00-00 00:00:00',
    `updated_at`    timestamp                                                               NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by`    int(10) unsigned                                                        NOT NULL,
    `updated_by`    int(10) unsigned                                                        NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `entrys_order_key_unique` (`order_key`),
    KEY `view_time` (`view_time`),
    KEY `call_time` (`call_time`),
    KEY `visit_time` (`visit_time`),
    KEY `FK_entries_refers` (`refer_id`),
    KEY `FK_entries_users_creater` (`created_by`),
    KEY `FK_entries_users_updater` (`updated_by`),
    KEY `FK_entries_users_master` (`user_id`),
    KEY `refuse_time` (`refuse_time`),
    KEY `status` (`status`),
    KEY `defer_before` (`defer_before`),
    CONSTRAINT `FK_entries_refers` FOREIGN KEY (`refer_id`) REFERENCES `refers` (`id`),
    CONSTRAINT `FK_entries_users_creater` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_entries_users_updater` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 196
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


-- Дамп структуры для таблица erp.events_log
CREATE TABLE IF NOT EXISTS `events_log`
(
    `id`         int(10) unsigned                     NOT NULL AUTO_INCREMENT,
    `event`      enum ('created','updated','deleted') NOT NULL DEFAULT 'updated',
    `model`      enum ('Entry','Order')               NOT NULL,
    `model_id`   int(10) unsigned                     NOT NULL,
    `created_by` int(10) unsigned                     NOT NULL,
    `items`      text                                 NOT NULL,
    `created_at` timestamp                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `FK_events_log_users` (`created_by`),
    KEY `model` (`model`, `model_id`),
    KEY `event` (`event`),
    CONSTRAINT `FK_events_log_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 688
  DEFAULT CHARSET = utf8;


-- Дамп структуры для таблица erp.orders
CREATE TABLE IF NOT EXISTS `orders`
(
    `id`            int(10) unsigned                                               NOT NULL AUTO_INCREMENT,
    `order_key`     varchar(10) COLLATE utf8_unicode_ci                            NOT NULL,
    `title`         varchar(100) COLLATE utf8_unicode_ci                           NOT NULL,
    `client_name`   varchar(50) COLLATE utf8_unicode_ci                            NOT NULL,
    `client_phone`  varchar(20) COLLATE utf8_unicode_ci                            NOT NULL,
    `metro`         varchar(50) COLLATE utf8_unicode_ci                            NOT NULL,
    `mo_town`       varchar(50) COLLATE utf8_unicode_ci                            NOT NULL,
    `descr`         text COLLATE utf8_unicode_ci                                   NOT NULL,
    `supplier_id`   int(10) unsigned                                               NOT NULL DEFAULT '2',
    `refer_id`      int(10) unsigned                                               NOT NULL,
    `details`       text COLLATE utf8_unicode_ci                                   NOT NULL,
    `cashless`      tinyint(1) unsigned                                            NOT NULL DEFAULT '0',
    `inplace`       tinyint(1) unsigned                                            NOT NULL DEFAULT '0',
    `docs_signed`   tinyint(1) unsigned                                            NOT NULL DEFAULT '0',
    `cost`          decimal(10, 0) unsigned                                        NOT NULL DEFAULT '0',
    `prepay`        decimal(10, 0) unsigned                                        NOT NULL DEFAULT '0',
    `result_amount` decimal(10, 0) unsigned                                        NOT NULL DEFAULT '0',
    `cashbox`       decimal(10, 0) unsigned                                        NOT NULL DEFAULT '0',
    `comment`       text COLLATE utf8_unicode_ci                                   NOT NULL,
    `finish_date`   date                                                           NOT NULL,
    `finished_time` datetime                                                       NOT NULL DEFAULT '0000-00-00 00:00:00',
    `canceled_at`   datetime                                                                DEFAULT NULL,
    `status`        enum ('CREATED','FINISHED','ARCHIVED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CREATED',
    `archived_at`   datetime                                                                DEFAULT NULL,
    `created_at`    timestamp                                                      NOT NULL DEFAULT '0000-00-00 00:00:00',
    `updated_at`    timestamp                                                      NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by`    int(10) unsigned                                               NOT NULL,
    `updated_by`    int(10) unsigned                                               NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `orders_order_key_unique` (`order_key`),
    KEY `FK_orders_users` (`created_by`),
    KEY `FK_orders_users_2` (`updated_by`),
    KEY `FK_orders_refers` (`refer_id`),
    KEY `status` (`status`),
    KEY `cashbox` (`cashbox`),
    KEY `finish_date` (`finish_date`),
    CONSTRAINT `FK_orders_refers` FOREIGN KEY (`refer_id`) REFERENCES `refers` (`id`),
    CONSTRAINT `FK_orders_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_orders_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 91
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


-- Дамп структуры для таблица erp.order_stuff
CREATE TABLE IF NOT EXISTS `order_stuff`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `order_id`   int(10) unsigned NOT NULL,
    `name`       varchar(255)     NOT NULL DEFAULT '0',
    `quantity`   varchar(50)      NOT NULL,
    `cost`       decimal(10, 0)   NOT NULL,
    `created_by` int(10) unsigned NOT NULL,
    `updated_by` int(10) unsigned NOT NULL,
    `created_at` timestamp        NOT NULL DEFAULT '0000-00-00 00:00:00',
    `updated_at` timestamp        NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`id`),
    KEY `FK_order_stuff_users` (`created_by`),
    KEY `FK_order_stuff_users_2` (`updated_by`),
    KEY `FK_order_stuff_orders` (`order_id`),
    CONSTRAINT `FK_order_stuff_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_order_stuff_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_order_stuff_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 101
  DEFAULT CHARSET = utf8;


-- Дамп структуры для таблица erp.order_user
CREATE TABLE IF NOT EXISTS `order_user`
(
    `order_id` int(10) unsigned NOT NULL,
    `user_id`  int(10) unsigned NOT NULL,
    UNIQUE KEY `order_user` (`order_id`, `user_id`),
    KEY `FK__users` (`user_id`),
    CONSTRAINT `FK__orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
    CONSTRAINT `FK__users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- Дамп данных таблицы erp.order_user: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `order_user`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `order_user`
    ENABLE KEYS */;


-- Дамп структуры для таблица erp.payments
CREATE TABLE IF NOT EXISTS `payments`
(
    `id`         int(10) unsigned        NOT NULL AUTO_INCREMENT,
    `order_id`   int(10) unsigned        NOT NULL,
    `summ`       decimal(10, 0) unsigned NOT NULL,
    `created_at` timestamp               NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp               NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` int(10) unsigned        NOT NULL,
    `updated_by` int(10) unsigned        NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_payments_orders` (`order_id`),
    KEY `FK_payments_updated_by_users` (`updated_by`),
    KEY `FK_payments_created_by_users` (`created_by`),
    CONSTRAINT `FK_payments_created_by_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_payments_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
    CONSTRAINT `FK_payments_updated_by_users` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 51
  DEFAULT CHARSET = utf8;


-- Дамп структуры для таблица erp.permissions
CREATE TABLE IF NOT EXISTS `permissions`
(
    `id`    int(10) unsigned                     NOT NULL AUTO_INCREMENT,
    `route` varchar(50) COLLATE utf8_unicode_ci  NOT NULL,
    `name`  varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `descr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `permissions_route_unique` (`route`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 82
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

-- Дамп данных таблицы erp.permissions: ~80 rows (приблизительно)
/*!40000 ALTER TABLE `permissions`
    DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `route`, `name`, `descr`)
VALUES (1, 'user.index', 'Список пользователей', 'Просматривать cписок пользователей'),
       (2, 'user.create', '', 'Создать новых пользователей'),
       (3, 'user.show', '', 'Просматривать данные пользователя'),
       (4, 'user.edit', '', 'Редактировать пользователя'),
       (5, 'user.destroy', '', 'Удалять пользователя'),
       (6, 'role.index', 'Список ролей', 'Просматривать cписок ролей'),
       (7, 'role.create', '', 'Создать новые роли'),
       (8, 'role.show', '', 'Просматривать данные ролей'),
       (9, 'role.edit', '', 'Редактировать роли'),
       (10, 'role.destroy', '', 'Удалять роли'),
       (11, 'refer.index', 'Список источников', 'Просматривать cписок источников'),
       (12, 'refer.create', '', 'Создать новые источники'),
       (13, 'refer.show', '', 'Просматривать данные источников'),
       (14, 'refer.edit', '', 'Редактировать источники'),
       (15, 'refer.destroy', '', 'Удалять источники'),
       (16, 'entry.index', '', 'Просматривать список заявок - все'),
       (17, 'entry.index:user_id', '', 'Просматривать список заявок - назначенные'),
       (18, 'entry.index:created_by', '', 'Просматривать список заявок - свои'),
       (19, 'entry.create', '', 'Создать новые заявки'),
       (20, 'entry.show', '', 'Просматривать данные заявок - все'),
       (21, 'entry.show:user_id', '', 'Просматривать данные заявок - назначенные'),
       (22, 'entry.show:created_by', '', 'Просматривать данные заявок - свои'),
       (23, 'entry.edit', '', 'Редактировать заявки - все'),
       (24, 'entry.edit:user_id', '', 'Редактировать заявки - назначенные'),
       (25, 'entry.edit:created_by', '', 'Редактировать заявки - свои'),
       (26, 'entry.destroy', '', 'Удалять заявки - все'),
       (27, 'entry.destroy:user_id', '', 'Удалять заявки - назначенные'),
       (28, 'entry.destroy:created_by', '', 'Удалять заявки - свои'),
       (29, 'entry.status', '', 'Изменять статус заявки - все'),
       (30, 'entry.status:user_id', '', 'Изменять статус заявки - назначенные'),
       (31, 'entry.status:created_by', '', 'Изменять статус заявки - свои'),
       (32, 'entry.export', '', 'Экспорт списка заявок'),
       (33, 'order.index', '', 'Просматривать список заказов - все'),
       (34, 'order.index:user_id', '', 'Просматривать список заказов - назначенные'),
       (35, 'order.index:created_by', '', 'Просматривать список заказов - свои'),
       (36, 'order.create', '', 'Формировать новые заказы - из всех заявок'),
       (37, 'order.create:user_id', '', 'Формировать новые заказы - из назначенных заявок'),
       (38, 'order.create:created_by', '', 'Формировать новые заказы - из своих заявок'),
       (39, 'order.show', '', 'Просматривать данные заказов - все'),
       (40, 'order.show:user_id', '', 'Просматривать данные заказов - назначенные'),
       (41, 'order.show:created_by', '', 'Просматривать данные заказов - свои'),
       (42, 'order.edit', '', 'Редактировать заказы - все'),
       (43, 'order.edit:user_id', '', 'Редактировать заказы - назначенные'),
       (44, 'order.edit:created_by', '', 'Редактировать заказы - свои'),
       (45, 'order.destroy', '', 'Удалять заказы - все'),
       (46, 'order.destroy:user_id', '', 'Удалять заказы - назначенные'),
       (47, 'order.destroy:created_by', '', 'Удалять заказы - свои'),
       (48, 'order.status', '', 'Изменять статус заказа - все'),
       (49, 'order.status:user_id', '', 'Изменять статус заказа - назначенные'),
       (50, 'order.status:created_by', '', 'Изменять статус заказа - свои'),
       (51, 'order.export', '', 'Экспорт списка заказов'),
       (52, 'order.finish', '', 'Завершать заказы - все'),
       (53, 'order.finish:user_id', '', 'Завершать заказы - назначенные'),
       (54, 'order.finish:created_by', '', 'Завершать заказы - свои'),
       (55, 'entry.showlogs', '', 'Просматривать историю изменений - все'),
       (56, 'entry.showlogs:user_id', '', 'Просматривать историю изменений - назначенные'),
       (57, 'entry.showlogs:created_by', '', 'Просматривать историю изменений - свои'),
       (58, 'order.showlogs', '', 'Просматривать историю изменений - все'),
       (59, 'order.showlogs:user_id', '', 'Просматривать историю изменений - назначенные'),
       (60, 'order.showlogs:created_by', '', 'Просматривать историю изменений - свои'),
       (61, 'order.search', '', 'Поиск заказов - все'),
       (62, 'order.search:user_id', '', 'Поиск заказов - назначенные'),
       (63, 'order.search:created_by', '', 'Поиск заказов - свои'),
       (64, 'order.cashbox', '', 'Вносить деньги в кассу заказов'),
       (65, 'entry.search', '', 'Поиск заявок - все'),
       (66, 'entry.search:user_id', '', 'Поиск заявок - назначенные'),
       (67, 'entry.search:created_by', '', 'Поиск заявок - свои'),
       (68, 'order.close', '', 'Закрывать заказы'),
       (69, 'cashbox.index', '', 'Просматривать кассу'),
       (70, 'entry.archiving', '', 'Перемещать заявки в архив'),
       (71, 'report.index', '', 'Просматривать отчеты'),
       (72, 'cashbox.export', '', 'Экспорт списка платежей'),
       (73, 'report.export', '', 'Экспорт отчетов'),
       (74, 'help.index', '', 'Просматривать справку'),
       (75, 'order.stuff', '', 'Материалы - все'),
       (76, 'order.stuff:user_id', '', 'Материалы - назначенные'),
       (77, 'order.stuff:created_by', '', 'Материалы - свои'),
       (78, 'order.cancel', '', 'Аннулировать заказы'),
       (79, 'cashbox.show', '', 'Просматривать платежи заказа в кассе'),
       (80, 'order.archivate', '', 'Архивировать заказы');
/*!40000 ALTER TABLE `permissions`
    ENABLE KEYS */;


-- Дамп структуры для таблица erp.refers
CREATE TABLE IF NOT EXISTS `refers`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name`       varchar(255)     NOT NULL,
    `descr`      text             NOT NULL,
    `created_at` timestamp        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `updated_at` timestamp        NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` int(10) unsigned NOT NULL,
    `updated_by` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_refers_creater` (`created_by`),
    KEY `FK_refers_updater` (`updated_by`),
    CONSTRAINT `FK_refers_creater` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
    CONSTRAINT `FK_refers_updater` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;

-- Дамп данных таблицы erp.refers: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `refers`
    DISABLE KEYS */;
INSERT INTO `refers` (`id`, `name`, `descr`, `created_at`, `updated_at`, `created_by`, `updated_by`)
VALUES (1, 'Не установлен', 'Неизвестно, откуда клиент получил информацию', '2014-11-12 02:51:27',
        '2014-11-06 03:53:51', 1, 1);
/*!40000 ALTER TABLE `refers`
    ENABLE KEYS */;


-- Дамп структуры для таблица erp.roles
CREATE TABLE IF NOT EXISTS `roles`
(
    `id`         tinyint(3) unsigned                  NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `created_at` timestamp                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp                            NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`id`),
    UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

-- Дамп данных таблицы erp.roles: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`)
VALUES (1, 'Владелец', '2014-10-20 21:07:00', '2014-10-20 21:08:15'),
       (2, 'Оператор', '2014-10-20 21:07:00', '2014-10-20 21:07:25'),
       (3, 'Мастер', '2014-10-20 22:53:18', '2015-02-25 02:18:28'),
       (4, 'Снабженец', '2014-10-20 22:56:40', '2015-01-24 18:15:29');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;


-- Дамп структуры для таблица erp.role_permission
CREATE TABLE IF NOT EXISTS `role_permission`
(
    `role_id`       tinyint(3) unsigned NOT NULL,
    `permission_id` int(10) unsigned    NOT NULL,
    UNIQUE KEY `role_permission` (`permission_id`, `role_id`),
    KEY `FK_role_permission_roles` (`role_id`),
    CONSTRAINT `FK_role_permission_permissions` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
    CONSTRAINT `FK_role_permission_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

-- Дамп данных таблицы erp.role_permission: ~59 rows (приблизительно)
/*!40000 ALTER TABLE `role_permission`
    DISABLE KEYS */;
INSERT INTO `role_permission` (`role_id`, `permission_id`)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 8),
       (1, 11),
       (1, 12),
       (1, 13),
       (1, 14),
       (1, 15),
       (1, 16),
       (1, 19),
       (1, 20),
       (1, 23),
       (1, 26),
       (1, 29),
       (1, 32),
       (1, 33),
       (1, 36),
       (1, 39),
       (1, 42),
       (1, 45),
       (1, 48),
       (1, 51),
       (1, 52),
       (1, 55),
       (1, 58),
       (1, 61),
       (1, 64),
       (1, 65),
       (1, 68),
       (1, 69),
       (1, 70),
       (1, 71),
       (1, 72),
       (1, 73),
       (1, 74),
       (1, 75),
       (1, 78),
       (1, 79),
       (1, 80),
       (2, 18),
       (2, 19),
       (2, 22),
       (2, 25),
       (2, 74),
       (3, 17),
       (3, 21),
       (3, 30),
       (3, 35),
       (3, 37),
       (3, 41),
       (3, 50),
       (3, 54),
       (3, 74),
       (3, 77),
       (4, 74);
/*!40000 ALTER TABLE `role_permission`
    ENABLE KEYS */;


-- Дамп структуры для таблица erp.role_user
CREATE TABLE IF NOT EXISTS `role_user`
(
    `role_id` tinyint(3) unsigned NOT NULL,
    `user_id` int(11) unsigned    NOT NULL,
    UNIQUE KEY `user_role_unique` (`user_id`, `role_id`),
    KEY `FK_user_role_roles` (`role_id`),
    CONSTRAINT `FK_user_role_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
    CONSTRAINT `FK_user_role_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


-- Дамп структуры для таблица erp.users
CREATE TABLE IF NOT EXISTS `users`
(
    `id`         int(10) unsigned                     NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `password`   varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    `api_key`    varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `phone`      varchar(16) COLLATE utf8_unicode_ci  NOT NULL,
    `email`      varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `created_at` timestamp                            NOT NULL DEFAULT '0000-00-00 00:00:00',
    `updated_at` timestamp                            NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by` int(10) unsigned                     NOT NULL,
    `updated_by` int(10) unsigned                     NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 13
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
