<!DOCTYPE html>
<html>
    <head>
        <title>DrMebius</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="icon" href="/favicon.production.ico" type="image/x-icon"/>
        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
        <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link media="all" type="text/css" rel="stylesheet" href="/css/style.css">
    </head>
    <body class="bg-info">
        <div class="container-fluid">
            <div style="text-align: center" class="alert alert-success alert-block">
                <h4 class="text-capitalize">Мы переехали!</h4>
                Сервис находится по новому адресу
                <br/>
                <br/>
                <a style="padding:20px;" href="http://erp.drmebius.ru" class="btn btn-default btn-large">
                    <b class="text-info">http://erp.drmebius.ru</b>
                </a>
                <br/>
                <br/>
                Необходимо будет заново ввести свои данные
                <br/>
                для входа в систему на новом месте.
                <br/>
                <br/>
                <br/>
            </div>
        </div>
    </body>
</html>